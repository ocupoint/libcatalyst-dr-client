/// @file   ocpcatalystdrclient.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#ifndef _LIBCATLYST_DR_CLIENT_OCP_CATALYST_DR_CLIENT_PUB_H_
#define _LIBCATLYST_DR_CLIENT_OCP_CATALYST_DR_CLIENT_PUB_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Digital Receiver Client Top level
 *
 * Access all resources and components through the client calling interface
 *
 * @struct
 * @retval None
 */
typedef struct OCPCatalystDRClient OCPCatalystDRClient;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create the Digital Receiver Client
 * @memberof  OCPCatalystDRClient
 * @param     client:
 * @retval    None
 */
OCPCatalystDRClient* OCPCatalystDRClient_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Free the Client
 * @memberof  OCPCatalystDRClient
 * @param     client:
 * @retval    None
 */
void OCPCatalystDRClient_free(OCPCatalystDRClient* client);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif