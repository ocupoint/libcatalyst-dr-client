/// @file   errorcodes.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-25-2021

#ifndef _OCPCDR_EVENT_CODES_PUB_H_
#define _OCPCDR_EVENT_CODES_PUB_H_

#ifdef __cplusplus
extern "C" {
#endif


enum OCPCDR_ErrorCodes
{
  NO_ERROR            =0x0,
  MEMORY_ERROR,
  ASSERTION_ERROR,
  ATTRIBUTE_ERROR
};


#ifdef __cplusplus
}
#endif

#endif