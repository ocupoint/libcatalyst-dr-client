/// @file   helpers.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#ifndef _OCPCDR_HELPERS_PRIV_H_
#define _OCPCDR_HELPERS_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <stddef.h>
#include <stdint.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// String
/// @struct
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Concatenates two strings
 * @memberof  String
 * @param     a: string A
 * @param     b: string B
 * @retval
 */
char* String_concat(const char* a, const char* b);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Returns a formatted string
 * @memberof  String
 * @param     format:
 * @retval    char*
 */
char* String_format(const char* format, ...);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     String free
 * @memberof  String
 * @param     str:
 * @retval    uint8_t: status code
 */
void String_destroy(char* str);


//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Heap
/// @struct
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Standard Allocator to the Heap
 * @param  ptr: Pointer to allocate memory to
 * @param  size_bytes: Size of memory pointer points to
 * @retval void *
 */
void* Heap_allocate(size_t size_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Standard Clear Memory Allocator to the Heap
 * @param  size_bytes: Size of memory pointer points to
 * @retval void *
 */
void* Heap_clear_allocate(size_t size_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Prints out to Heap data as Base16 Hex Data
 * @param  ptr: pointer to the heap
 * @param  size_bytes: size of the heap int bytes
 * @retval None
 */
void Heap_to_hex_string(void* ptr, size_t size_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Frees Memory from the Heap
 * @param  ptr: Pointer to free
 * @retval None
 */
void Heap_free(void* ptr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Bits32
/// @struct
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Read a value from a 32 bit word given a mask and offset
 * @memberof  Bits32
 * @param     word: 32 bit word
 * @param     mask: 32 bit mask
 * @param     offset: offset within 32 bit word
 * @retval    uint32_t
 */
uint32_t Bits32_read(uint32_t word, uint32_t mask, uint8_t offset);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Writes a value into a given word provided a mask and offset
 * @memberof  Bits32
 * @param     word: 32 bit word
 * @param     mask: 32 bit mask
 * @param     offset: offset within 32 bit word
 * @param     value: 32 bit value
 * @retval    uint32_t
 */
uint32_t Bits32_write(uint32_t word, uint32_t mask, uint8_t offset, uint32_t value);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Get a time stamp in with microsecond resolution
 * @retval uint64_t
 */
uint64_t Timestamp_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Initialize a timestamp to be used as a reference point
 * @memberof  Timestamp
 * @retval    None
 */
void Timestamp_initialize();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Get the timedelta from the initialization time
 * @memberof  Timestamp
 * @retval    uint64_t
 */
uint64_t Timestamp_timedelta();

//////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifdef __cplusplus
}
#endif

#endif