/// @file   debug.h
/// @brief  Ocupint Debug information
///
/// @author Nicholas Molinski
/// @date   01-20-2021


#ifndef _LIBCATLYST_DR_CLIENT_DEBUG_PRIV_H_
#define _LIBCATLYST_DR_CLIENT_DEBUG_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "private/helpers.h"
#include "private/debug.h"

// Print all INFO and up by default
#ifndef ENV_OCPCATALYST_LOG_LEVEL_TYPE
#define ENV_OCPCATALYST_LOG_LEVEL_TYPE OCPCATALYST_LOG_TYPE_INFO
#endif

// Debug by default
#ifndef ENV_OCPCATALYST_DEBUG
#define ENV_OCPCATALYST_DEBUG 1
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Log type enumeration
 */
typedef enum LOG_TYPE
{
  /// Internal Log
  OCPCATALYST_LOG_TYPE_INTR=0x0,
  /// Debug Log
  OCPCATALYST_LOG_TYPE_DBUG=0x1,
  /// Info Log
  OCPCATALYST_LOG_TYPE_INFO=0x2,
  /// Warning Log
  OCPCATALYST_LOG_TYPE_WARN=0x3,
  /// Error Log
  OCPCATALYST_LOG_TYPE_ERRO=0x4,

} LOG_TYPE;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Debug Logger Function
 * @note
 * @param  *format:s
 * @retval None
 */
void OCPCatalystDebugLogger_log(enum LOG_TYPE log_type, const char *format, ...);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#if ENV_OCPCATALYST_DEBUG

/**
 * @brief  Log Data
 * @todo   Add file format to write
 */
#define OCPCATALYST_LOG(LOG_TYPE, FMT, ...) do {                       \
  if (ENV_OCPCATALYST_LOG_LEVEL_TYPE <= LOG_TYPE)                             \
  {                                                                    \
    char* info = String_format("- %30s [%3d] - ", __FILE__, __LINE__); \
    char* next = String_concat(info, FMT);                             \
    OCPCatalystDebugLogger_log(LOG_TYPE, next, ##__VA_ARGS__);         \
    String_destroy(info);                                              \
    String_destroy(next);                                              \
  }                                                                    \
} while (0);

#else
#define OCPCATALYST_LOG(LOG_TYPE, FMT, ...)
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// Logger Macros
// Check .env to see what is defined

#if (ENV_OCPCATALYST_DEBUG == 1) && (ENV_OCPCATALYST_LOG_LEVEL_TYPE >= OCPCATALYST_LOG_TYPE_ERRO)
#define OCPCATALYST_LOG_ERRO(FMT, ...)  do {                       \
  OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_ERRO, FMT, ##__VA_ARGS__)   \
} while (0);
#else
#define OCPCATALYST_LOG_ERRO(FMT, ...)
#endif

#if (ENV_OCPCATALYST_DEBUG == 1) && (ENV_OCPCATALYST_LOG_LEVEL_TYPE >= OCPCATALYST_LOG_TYPE_WARN)
#define OCPCATALYST_LOG_WARN(FMT, ...)  do {                       \
  OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_WARN, FMT, ##__VA_ARGS__)   \
} while (0);
#else
#define OCPCATALYST_LOG_WARN(FMT, ...)
#endif

#if (ENV_OCPCATALYST_DEBUG == 1) && (ENV_OCPCATALYST_LOG_LEVEL_TYPE >= OCPCATALYST_LOG_TYPE_INFO)
#define OCPCATALYST_LOG_INFO(FMT, ...)  do {                       \
  OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_INFO, FMT, ##__VA_ARGS__)   \
} while (0);
#else
#define OCPCATALYST_LOG_INFO(FMT, ...)
#endif

#if (ENV_OCPCATALYST_DEBUG == 1) && (ENV_OCPCATALYST_LOG_LEVEL_TYPE >= OCPCATALYST_LOG_TYPE_DBUG)
#define OCPCATALYST_LOG_DBUG(FMT, ...)  do {                       \
  OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_DBUG, FMT, ##__VA_ARGS__)   \
} while (0);
#else
#define OCPCATALYST_LOG_DBUG(FMT, ...)
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#if ENV_OCPCATALYST_DEBUG

/**
 * @brief  Assert success of a query
 */
#define OCPCATALYST_ASSERT(QUERY, FMT, ...) do {   \
  if ((QUERY))                           \
  {                                       \
    OCPCATALYST_LOG_ERRO(FMT, ##__VA_ARGS__);     \
    exit(EXIT_FAILURE);                   \
  }                                       \
} while(0);

#else

#define OCPCATALYST_ASSERT(QUERY, FMT, ...) do { QUERY; } while(0);

#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif