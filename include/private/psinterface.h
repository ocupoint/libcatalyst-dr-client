/// @file   psinterface.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   03-12-2021

#ifndef _OCPCDR_PS_INTERFACE_PRIV_H_
#define _OCPCDR_PS_INTERFACE_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define PSINTERFACE_SPI_FD_LENGTH 20
#define PSINTERFACE_I2C_FD_LENGTH 20

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  SPI Interface provided by the PS
 */
typedef struct PSInterfaceSPI
{
  /** File Descriptor to this interface */
  uint32_t fd;
  /** SCLK speed */
  uint32_t sclk_speed_hz;
  /** SPI Mode */
  uint8_t transaction_mode;
  /** LSB First */
  uint8_t is_lsb_first;
  /** Flags */
  uint32_t flags;

} PSInterfaceSPI;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  I2C Interface provided by the PS
 */
typedef struct PSInterfaceI2C
{
  /** File Descriptor to this interface */
  uint32_t fd;
  /** I2C Device Address */
  uint8_t addr;

} PSInterfaceI2C;


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Creates a SPI Interface struct
 * @retval PSInterfaceSPI *
 */
PSInterfaceSPI* PSInterfaceSPI_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Opens a SPI interface that exists on the PS
 * @memberof  PSInterfaceSPI
 * @param     psspi: PSInterfaceSPI *
 * @param     filename
 * @retval    None
 */
void PSInterfaceSPI_open(PSInterfaceSPI* psspi, char filename[PSINTERFACE_SPI_FD_LENGTH]);


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Perform a SPI full-duplex transfer
 * @memberof  PSInterfaceSPI
 * @param     psspi: PSInterfaceSPI *
 * @param     buffer: uint8_t *
 * @param     bytes_per_word: uint8_t
 * @param     number_of_words: uint8_t
 * @param     verify_buffer: uint8_t *
 * @retval    None
 */
void PSInterfaceSPI_transfer(PSInterfaceSPI* psspi, uint8_t* buffer, uint8_t bytes_per_word, uint8_t number_of_words, uint8_t* verify_buffer);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Frees a SPI Interface
 * @param  psspi:
 * @retval None
 */
void PSInterfaceSPI_free(PSInterfaceSPI* psspi);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a PS Interface to I2C
 * @memberof  PSInterfaceI2C
 * @retval    0=EXIT_SUCCESS; 1=EXIT_FAILURE
 */
PSInterfaceI2C* PSInterfaceI2C_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Opens a I2C Device
 * @memberof  PSInterfaceI2C
 * @param     psi2c:
 * @param     i2c_bus:
 * @param     dev_addr:
 * @retval    uint8_t
 */
uint8_t PSInterfaceI2C_open(PSInterfaceI2C* psi2c, uint8_t i2c_bus, uint8_t dev_addr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  I2C Write
 * @param  fd: I2C file descriptor
 * @param  command: i2c command
 * @param  buffer: Buffer to write
 * @param  num_bytes: transaction length
 * @retval uint8_t
 */
uint8_t PSInterfaceI2C_write(PSInterfaceI2C* psi2c, uint8_t* buffer, uint8_t num_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Free an I2C Interface
 * @memberof  PSInterfaceI2C
 * @param     psi2c:
 * @retval    None
 */
void PSInterfaceI2C_free(PSInterfaceI2C* psi2c);



#ifdef __cplusplus
}
#endif

#endif