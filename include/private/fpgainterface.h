/// @file   ocpcatalystdrcomponents.h
/// @brief  Automatically Generated File
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#ifndef _LIBCATLYST_DR_CLIENT_FPGA_INTERFACE_PRIV_H_
#define _LIBCATLYST_DR_CLIENT_FPGA_INTERFACE_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "private/memorymap.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// FsPGAInterface
/// @struct
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     FPGA Interface for hardware reading
 * @param     mem: MemoryMap*
 * @param     addr: Address in the component to access
 * @retval    uint32_t
 */
uint32_t FPGAInterface_read(MemoryMap* mem, void* addr);

//////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @brief     FPGA Interface for hardware writing
 * @param     mem: MemoryMap*
 * @param     addr: Address in the component to access
 * @retval    None
 */
void FPGAInterface_write(MemoryMap* mem, void* addr, uint32_t value);

//////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifdef __cplusplus
}
#endif

#endif