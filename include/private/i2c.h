/// @file   i2c.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   03-11-2021


#ifndef _OCPCDR_I2C_PRIV_H_
#define _OCPCDR_I2C_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief  I2C Write
 * @param  fd: I2C file descriptor
 * @param  command: i2c command
 * @param  length: transaction length
 * @param  buffer: Buffer to write
 * @todo   Rename this to PSInterface_i2c_write
 * @retval None
 */
void I2C_write(uint32_t fd, uint8_t command, uint8_t length, uint8_t* buffer);

#ifdef __cplusplus
}
#endif

#endif