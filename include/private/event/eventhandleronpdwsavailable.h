// /// @file   event.h
// /// @brief  File Description
// ///
// /// @author Nicholas Molinski
// /// @date   02-05-2021

// #ifndef _OCPCDR_EVENT_ON_PDWS_AVAILABLE_PRIV_H_
// #define _OCPCDR_EVENT_ON_PDWS_AVAILABLE_PRIV_H_

// #ifdef __cplusplus
// extern "C" {
// #endif

// #include <stddef.h>
// #include <stdint.h>
// #include <stdarg.h>
// #include "private/event/event.h"


// #define __NUM_ARGS_EVENT_ON_PDWS_AVAILABLE 2

// //////////////////////////////////////////////////////////////////////////////////////////////////////////
// ///
// /// Event Handler On PDWs Available
// ///
// ///
// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// typedef void (*on_pdws_available_listener)(enum EventHandlerType event_type, uint16_t num_pdws, PDWBufferPulseReport* pdw_buffer);

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Event Handler creation
//  * @memberof  EventHandlerOnPDWsAvailable
//  * @param     event_type: Event subscribing to
//  * @param     subscriber_handler: callback to run when event is occuring
//  * @retval    0=SUCCESS, 1=FAILURE
//  */
// uint8_t EventHandlerOnPDWsAvailable_create();

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Subscribe to an OnPDWsAvailable event
//  * @memberof  EventHandlerOnPDWsAvailable
//  * @param     handler: EventHandler
//  * @param     subscriber_handler:
//  * @todo      Add more error codes
//  * @retval    0=SUCCESS, 1=FAILURE
//  */
// uint8_t EventHandlerOnPDWsAvailable_subscribe(on_pdws_available_listener callback);

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Emits an event to all event subscribers created for this type
//  * @memberof  EventHandlerOnPDWsAvailable
//  * @param     num_pdws: Number of pdws to be emited
//  * @param     pdw_buffer: Buffer to pass to all subscribers
//  * @retval    None
//  */
// void EventHandlerOnPDWsAvailable_emit(uint16_t num_pdws, PDWBufferPulseReport* pdw_buffer);

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// #ifdef __cplusplus
// }
// #endif

// #endif