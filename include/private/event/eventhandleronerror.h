/// @file   eventhandleronerror.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-05-2021

#ifndef _OCPCDR_EVENT_ON_ERROR_PRIV_H_
#define _OCPCDR_EVENT_ON_ERROR_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include "public/errorcodes.h"
#include "private/event/event.h"

#define __NUM_ARGS_EVENT_ON_ERROR 2

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Event Handler On Error
///
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef void (*on_error_listener)(enum EventHandlerType event_type, enum OCPCDR_ErrorCodes erro_code, const char erro_msg[16]);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Event Handler creation
 * @memberof  EventHandlerOnError
 * @retval    0=SUCCESS, 1=FAILURE
 */
uint8_t EventHandlerOnError_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Subscribe to an OnError event
 * @memberof  EventHandlerOnError
 * @param     handler: EventHandler
 * @param     callback: The callback when this event is emitted
 * @retval    0=SUCCESS, 1=FAILURE
 */
uint8_t EventHandlerOnError_subscribe(on_error_listener callback);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Emits an event to all event subscribers created for this type
 * @memberof  EventHandlerOnError
 * @param     erro_code: The error code emitted
 * @param     erro_msg: The error message
 * @retval    None
 */
void EventHandlerOnError_emit(enum OCPCDR_ErrorCodes erro_code, const char erro_msg[16]);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif