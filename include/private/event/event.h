/// @file   event.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-05-2021

#ifndef _OCPCDR_EVENT_PRIV_H_
#define _OCPCDR_EVENT_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Event Handler Type
///
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

// Number of args each event has


/**
 * @brief  Available events to subscribe to
 */
typedef enum EventHandlerType
{
  /** On PDWs available event */
  NO_EVENT=0x0,

  /** On Error event */
  EVENT_ON_ERROR,

  /** On PDWs available event */
  EVENT_ON_PDWS_AVAILABLE

} EventHandlerType;


//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Event Handler
///
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __MAX_EVENT_HANDLERS
#define __MAX_EVENT_HANDLERS 10
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __MAX_EVENT_SUBSCRIBERS
#define __MAX_EVENT_SUBSCRIBERS 10
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Event Handler structures
 * @struct EventHandler
 */
typedef struct EventHandler
{
  /** Subscriber event callbacks */
  void (*subscriber_handler[__MAX_EVENT_SUBSCRIBERS])(enum EventHandlerType event_type, ...);
  /** Number of subscribers */
  size_t num_subscribers;
  /** Event type this handler emits */
  enum EventHandlerType event_type;
  /** Used to hold number of callback arguments for va_lists */
  size_t num_callback_args;

} EventHandler;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Event Handler creation
 * @memberof  EventHandler
 * @param     event_type: Event subscribing to
 * @param     subscriber_handler: callback to run when event is occuring
 * @retval    0=SUCCESS, 1=FAILURE
 */
uint8_t EventHander_create(enum EventHandlerType event_type);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Subscribe to an event
 * @memberof  EventHandler
 * @param     handler: EventHandler
 * @param     subscriber_handler:
 * @todo      Add more error codes
 * @retval    0=SUCCESS, 1=FAILURE
 */
uint8_t EventHander_subscribe(enum EventHandlerType event_type,  void (*subscriber_handler)(enum EventHandlerType event_type, ...));

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Emits an event to all event handlers created for that type
 * @memberof  EventHandler
 * @param     event_type:
 * @retval    None
 */
void EventHander_emit( uint8_t num_args, ...);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// Events

#include "private/event/eventhandleronerror.h"

#ifdef __cplusplus
}
#endif

#endif