/// @file   main.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-26-2021

#ifndef _OCPCDR_MAIN_PRIV_H_
#define _OCPCDR_MAIN_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>
#include <time.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Main Function to Initialize Digital Receiver
 * @note   This should be called only once
 * @retval 0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
int libcatalyst_dr_driver_main();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

void helloworld_libcatalyst();

#ifdef __cplusplus
}
#endif

#endif