/// @file   yosemitepdwgenerator.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   05-23-2021

#ifndef _OCPCDR_YOSEMITE_PDW_GENERATOR_PRIV_H_
#define _OCPCDR_YOSEMITE_PDW_GENERATOR_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "private/memorymap.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>

#define DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR                                            "Yosemite PDW Generator"
#define DRCOMPONENT_BASE_ADDR_YOSEMITE_PDW_GENERATOR                                       (uint32_t*)0xA0000000
#define DRCOMPONENT_RESERVED_ADDR_SPACE_YOSEMITE_PDW_GENERATOR                             0x0000FFFF

// Configuration

#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_RESET                                 ((DRComponentValueInfo){ .addr=0x000, .mask=0xFFFFFFFF, .offset=0, .description="IP Reset. Does not need to be cleared" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_ENABLE                                ((DRComponentValueInfo){ .addr=0x004, .mask=0xFFFFFFFF, .offset=0, .description="POR IP Core is enabled" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_PACKETSIZE_AXI4_STREAM_DMA_MASTER     ((DRComponentValueInfo){ .addr=0x008, .mask=0xFFFFFFFF, .offset=0, .description="Packetsize must be set to same value as capture length register" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_IPCORE_TIMESTAMP                      ((DRComponentValueInfo){ .addr=0x00C, .mask=0xFFFFFFFF, .offset=0, .description="IP Core build timestamp" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_SRCSELREG_DATA                        ((DRComponentValueInfo){ .addr=0x100, .mask=0xFFFFFFFF, .offset=0, .description="PDW gen source (0 = ADC0, 1 = internal Tx loopback, 2 = ADC1)" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_ADCSTREAMREG_DATA                     ((DRComponentValueInfo){ .addr=0x104, .mask=0xFFFFFFFF, .offset=0, .description="Data capture source (0 = PDW, 1 = raw ADC samples)" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_FFT1_THREG_DATA                       ((DRComponentValueInfo){ .addr=0x108, .mask=0xFFFFFFFF, .offset=0, .description="FFT_THRESH = 0.00001" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURELENGTH_REG_DATA                ((DRComponentValueInfo){ .addr=0x10C, .mask=0xFFFFFFFF, .offset=0, .description="Number of DMA transfers to perform. Should be the same as register 0x8" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURETRIGGER_REG_DATA               ((DRComponentValueInfo){ .addr=0x110, .mask=0xFFFFFFFF, .offset=0, .description="Toggle this to trigger the PDW Capture" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_S2MMDROPCOUNT_DATA                    ((DRComponentValueInfo){ .addr=0x114, .mask=0xFFFFFFFF, .offset=0, .description="Count how many PDWs have been dropped" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CWREPORTCYCLESREG_DATA                ((DRComponentValueInfo){ .addr=0x118, .mask=0xFFFFFFFF, .offset=0, .description="CW Declation time. Usually: round(CWReportTime*FPGAClkRate))" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CONTINUOS_STREAM                      ((DRComponentValueInfo){ .addr=0x11C, .mask=0xFFFFFFFF, .offset=0, .description="Toggle this to trigger the PDW Capture" })
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_SCRATCH                               ((DRComponentValueInfo){ .addr=0x120, .mask=0xFFFFFFFF, .offset=0, .description="SCRATCH" })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// Current Scale factor limitations
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_SCALE_MAX                             -21.1
#define DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_SCALE_MIN                             -69.2369

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates the YosemitePDWGenerator IP Core DrCompoent
 * @memberof  YosemitePDWGenerator
 * @retval    DRComponent *
 */
DRComponent* YosemitePDWGenerator_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Setups routine for the Yosemite PDW Generator
 * @memberof  YosemitePDWGenerator
 * @param     drcomponent:
 * @param     num_pdws:
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t YosemitePDWGenerator_setup(DRComponent* drc, size_t num_pdws);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Enables the PDW capture trigger
 * @memberof  YosemitePDWGenerator
 * @param     drc:
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t YosemitePDWGenerator_capture(DRComponent* drc);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     YosemitePDWGenerator
 * @memberof  YosemitePDWGenerator
 * @param     drc:
 * @param     amplitude_dbm:
 * @todo      provide arbitrary scale value
 * @retval    0=EXIT_SUCCESS; 1=EXIT_FAILURE
 */
uint8_t YosemitePDWGenerator_set_fft_threshold(DRComponent* drc, int16_t amplitude_decidbm);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif