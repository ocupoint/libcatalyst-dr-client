/// @file   dmaaxixilinx.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   05-24-2021

#ifndef _OCPCDR_DMA_AXI_XILINX_PRIV_H_
#define _OCPCDR_DMA_AXI_XILINX_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "private/memorymap.h"
#include "private/drcomponent/drcomponent.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>

#define DRCOMPONENT_NAME_DMA_AXI_XILINX                                            "DMA AXI Xilinx"
#define DRCOMPONENT_BASE_ADDR_DMA_AXI_XILINX                                       (uint32_t*)0xA0010000
#define DRCOMPONENT_RESERVED_ADDR_SPACE_DMA_AXI_XILINX                             0x0001FFFF

// Configuration

#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_MM2S_DMACR                         ((DRComponentValueInfo){ .addr=0x000, .mask=0xFFFFFFFF, .offset=0 , .description="MM2S DMA Control register" })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_MM2S_DMASR                         ((DRComponentValueInfo){ .addr=0x004, .mask=0xFFFFFFFF, .offset=0 , .description="MM2S DMA Status register" })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_MM2S_SA                            ((DRComponentValueInfo){ .addr=0x018, .mask=0xFFFFFFFF, .offset=0 , .description="MM2S Source Address. Lower 32 bits of address." })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_MM2S_SA_MSB                        ((DRComponentValueInfo){ .addr=0x01C, .mask=0xFFFFFFFF, .offset=0 , .description="MM2S Source Address. Upper 32 bits of address" })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_MM2S_LENGTH                        ((DRComponentValueInfo){ .addr=0x028, .mask=0xFFFFFFFF, .offset=0 , .description="MM2S Transfer Length (Bytes)" })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR                         ((DRComponentValueInfo){ .addr=0x030, .mask=0xFFFFFFFF, .offset=0 , .description="S2MM DMA Control register" })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMASR                         ((DRComponentValueInfo){ .addr=0x034, .mask=0xFFFFFFFF, .offset=0 , .description="S2MM DMA Status register" })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DA                            ((DRComponentValueInfo){ .addr=0x048, .mask=0xFFFFFFFF, .offset=0 , .description="S2MM Destination Address. Lower 32 bit address." })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DA_MSB                        ((DRComponentValueInfo){ .addr=0x04C, .mask=0xFFFFFFFF, .offset=0 , .description="S2MM Destination Address. Upper 32 bit address." })
#define DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_LENGTH                        ((DRComponentValueInfo){ .addr=0x058, .mask=0xFFFFFFFF, .offset=0 , .description="S2MM Buffer Length (Bytes)" })

typedef struct DMAAXIXilinxStatusRegister
{
  uint32_t halted           : 1;
  uint32_t idle             : 1;
  uint32_t reserved_2       : 1;
  uint32_t SGIncld          : 1;
  uint32_t DMAIntErr        : 1;
  uint32_t DMASlvErr        : 1;
  uint32_t DMADecErr        : 1;
  uint32_t reserved_7       : 1;
  uint32_t SGIntErr         : 1;
  uint32_t SGSlvErr         : 1;
  uint32_t SGDeErr          : 1;
  uint32_t reserved_11      : 1;
  uint32_t IOC_Irq          : 1;
  uint32_t Dly_Irq          : 1;
  uint32_t Err_Irq          : 1;
  uint32_t reserved_15      : 1;
  uint32_t IRQThresholdSts  : 8;
  uint32_t IRQDelaySts      : 8;

} DMAAXIXilinxStatusRegister;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define DRCOMPONENT_DMA_AXI_XILINX_CR_MODE 0x1

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates the DMA AXI Xilinx component
 * @memberof  DMAAXIXilinx
 * @retval    DRComponent *
 */
DRComponent* DMAAXIXilinx_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Setup routine for the DMA AXI Xilinx component
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @param     dma_address:
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t DMAAXIXilinx_setup(DRComponent* drc, uintptr_t dma_address);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Perform a manual DMA transfer
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @param     num_bytes:
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t DMAAXIXilinx_s2mm_transfer(DRComponent* drc, size_t num_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Get the status from the DMA Engine
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @retval    uint32_t
 */
uint32_t DMAAXIXilinx_get_s2mm_status(DRComponent* drc);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Handle DMA interrupts
//  * @memberof  DMAAXIXilinx
//  * @param     drc:
//  * @param     dma_address:
//  * @retval    None
//  */
// void DMAAXIXilinx_handle_interrupt(DRComponent* drc, void* dma_address);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Manually poll the DMA for transfers
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @param     dma_address:
 * @retval    None
 */
void DMAAXIXilinx_poll(DRComponent* drc, void* dma_address, void (*intc_callback)(), size_t num_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Clears the DMA Interrupt
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @param     dma_alloc:
 * @retval    uint8_t
 */
uint8_t DMAAXIXilinx_clear_interrupt(DRComponent* drc, uintptr_t dma_alloc);

//////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifdef __cplusplus
}
#endif

#endif