/// @file   pdwreportcontroller.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-26-2021


#ifndef _OCPCDR_PDWREPORTS_PRIV_H_
#define _OCPCDR_PDWREPORTS_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "private/message/message.h"

#define DRCOMPONENT_NAME_PDW_REPORTS                                   "PDW Reports"
#define DRCOMPONENT_BASE_ADDR_PDW_REPORTS                               (uint32_t*)0x40000000
#define DRCOMPONENT_RESERVED_ADDR_SPACE_PDW_REPORTS                     0x00000FFF

typedef struct ALIGN_STRUCT_32BIT PDWReport
{
  uint64_t toa_msb             : 32; // Word 0
  uint64_t toa_lsb             : 32; // Word 1
  uint32_t avg_freq_khz        : 32; // Word 2
  uint32_t min_freq_khz        : 32; // Word 3
  uint32_t max_freq_khz        : 32; // Word 4
  uint32_t passband_center_khz : 32; // Word 5
  uint32_t passband_width_khz  : 32; // Word 6
  uint32_t pulse_width_khz     : 32; // Word 7
  uint16_t snr_decidb          : 16; // Word 8
  uint16_t avg_amp_decidb      : 16; // Word 8
  uint16_t min_amp_decidb      : 16; // Word 9
  uint16_t max_amp_decidb      : 16; // Word 9
  uint16_t aoa_decideg         : 16; // Word 10
  uint16_t heading_decideg     : 16; // Word 10
  uint16_t pitch_decideg       : 16; // Word 11
  uint16_t roll_decideg        : 16; // Word 11
  uint16_t offset_decideg      : 16; // Word 12
  uint16_t parameters          : 16; // Word 12
  uint16_t flag_bits           : 16; // Word 13
  uint8_t  angle_quality       : 8;  // Word 13
  uint8_t  pdw_source          : 8;  // Word 13
} PDWReport;


#ifdef __cplusplus
}
#endif

#endif