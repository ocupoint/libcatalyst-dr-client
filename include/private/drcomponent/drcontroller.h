/// @file   component.h
/// @brief  Automatically Generated File
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#ifndef _OCPCDR_DR_CONTROLLER_PRIV_H_
#define _OCPCDR_DR_CONTROLLER_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "private/memorymap.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>

#define DRCOMPONENT_NAME_DR_CONTROLLER                                            "DR Controller"
#define DRCOMPONENT_BASE_ADDR_DR_CONTROLLER                                       (uint32_t*)0xA0000000
#define DRCOMPONENT_RESERVED_ADDR_SPACE_DR_CONTROLLER                             0x00000FFF

// Configuration

#define DRCOMPONENT_VALUE_INFO_DR_CONTROLLER_INTERRUPT                            ((DRComponentValueInfo){ .addr=0x100, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_DR_CONTROLLER_BIT                                  ((DRComponentValueInfo){ .addr=0x104, .mask=0xFFFFFFFF, .offset=0  })

#ifdef __cplusplus
}
#endif

#endif