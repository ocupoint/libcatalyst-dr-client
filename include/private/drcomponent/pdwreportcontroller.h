/// @file   pdwreportcontroller.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-26-2021


#ifndef _OCPCDR_PDWREPORTCONTROLLER_PRIV_H_
#define _OCPCDR_PDWREPORTCONTROLLER_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define DRCOMPONENT_NAME_PDW_REPORT_CONTROLLER                                    "PDW Report Controller"
#define DRCOMPONENT_BASE_ADDR_PDW_REPORT_CONTROLLER                               (uint32_t*)0xA0001000
#define DRCOMPONENT_RESERVED_ADDR_SPACE_PDW_REPORT_CONTROLLER                     0x00000FFF

// Configuration

#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_PDWS_PER_MESSAGE             ((DRComponentValueInfo){ .addr=0x100, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_MAXIMUM_DETECTION_DURATION   ((DRComponentValueInfo){ .addr=0x104, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_PHYSICAL_BUFFER_ADDR         ((DRComponentValueInfo){ .addr=0x108, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_AMPLITUDE_THRESHOLD          ((DRComponentValueInfo){ .addr=0x10C, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_GENERATION_SOURCE            ((DRComponentValueInfo){ .addr=0x110, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_VIRTUAL_BUFFER_ADDR_MSB      ((DRComponentValueInfo){ .addr=0x114, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_VIRTUAL_BUFFER_ADDR_LSB      ((DRComponentValueInfo){ .addr=0x118, .mask=0xFFFFFFFF, .offset=0  })

// F1 Artificial Configuration

#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_PRI_NS                    ((DRComponentValueInfo){ .addr=0x140, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_AVG_FREQ_KHZ              ((DRComponentValueInfo){ .addr=0x144, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_MIN_FREQ_KHZ              ((DRComponentValueInfo){ .addr=0x148, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_MAX_FREQ_KHZ              ((DRComponentValueInfo){ .addr=0x14C, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_PASSBAND_CENTER_KHZ       ((DRComponentValueInfo){ .addr=0x150, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_PASSBAND_WIDTH_KHZ        ((DRComponentValueInfo){ .addr=0x154, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_PULSE_WIDTH_NS            ((DRComponentValueInfo){ .addr=0x158, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_SNR_DECIDB                ((DRComponentValueInfo){ .addr=0x15C, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_AVG_AMP_DECIDB            ((DRComponentValueInfo){ .addr=0x160, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_MIN_AMP_DECIDB            ((DRComponentValueInfo){ .addr=0x164, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_MAX_AMP_DECIDB            ((DRComponentValueInfo){ .addr=0x168, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_AOA_DECIDEG               ((DRComponentValueInfo){ .addr=0x16C, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_HEADING_DECIDEG           ((DRComponentValueInfo){ .addr=0x170, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_PITCH_DECIDEG             ((DRComponentValueInfo){ .addr=0x174, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_ROLL_DECIDEG              ((DRComponentValueInfo){ .addr=0x178, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_OFFSET_DECIDEG            ((DRComponentValueInfo){ .addr=0x17C, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_PARAMETERS                ((DRComponentValueInfo){ .addr=0x180, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_FLAG_BITS                 ((DRComponentValueInfo){ .addr=0x184, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_ANGLE_QUALITY             ((DRComponentValueInfo){ .addr=0x188, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_PDW_REPORT_CONTROLLER_F1_PDW_SOURCE                ((DRComponentValueInfo){ .addr=0x18C, .mask=0xFFFFFFFF, .offset=0  })

// Generation Sources

#define DRCOMPONENT_PDW_REPORT_CONTROLLER_GENERATION_SOURCE_LIVE                  0
#define DRCOMPONENT_PDW_REPORT_CONTROLLER_GENERATION_SOURCE_FW                    1
#define DRCOMPONENT_PDW_REPORT_CONTROLLER_GENERATION_SOURCE_CAL                   2
#define DRCOMPONENT_PDW_REPORT_CONTROLLER_GENERATION_SOURCE_SW                    3

struct PDWReportControllerSoftwareDefinedPDWConfiguration
{
  uint64_t start_toa;
  uint64_t virtual_addr;
  uint32_t max_pdws;
  uint32_t max_duration_ns;
  uint32_t f1_pri;
  uint32_t f1_avg_freq_khz;
  uint32_t f1_min_freq_khz;
  uint32_t f1_max_freq_khz;
  uint32_t f1_passband_center_khz;
  uint32_t f1_passband_width_khz;
  uint32_t f1_pulse_width_khz;
  uint32_t f1_snr_decidb;
  uint32_t f1_avg_amp_decidb;
  uint32_t f1_min_amp_decidb;
  uint32_t f1_max_amp_decidb;
  uint32_t f1_aoa_decideg;
  uint32_t f1_heading_decideg;
  uint32_t f1_pitch_decideg;
  uint32_t f1_roll_decideg;
  uint32_t f1_offset_decideg;
  uint32_t f1_parameters;
  uint32_t f1_flag_bits;
  uint32_t f1_angle_quality;
  uint32_t f1_pdw_source;
};

struct PDWReportControllerFirmwareDefinedPDWConfiguration
{
  uint64_t virtual_addr;
  uint32_t max_pdws;
  uint32_t max_duration_ns;
  uint32_t f1_pri;
  DRComponent* drc_pdwreports;

};


#ifdef __cplusplus
}
#endif

#endif