/// @file   component.h
/// @brief  Automatically Generated File
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#ifndef _OCPCDR_DR_COMPONENT_PRIV_H_
#define _OCPCDR_DR_COMPONENT_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "private/memorymap.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define DRCOMPONENT_NAME_LENGTH             32
#define DRCOMPONENT_MAX_NUM_OF_COMPONENTS   16

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// DRComponent
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Digital Receiver Component
 */
typedef struct DRComponent
{
  /** The Component Name */
  char name[DRCOMPONENT_NAME_LENGTH];
  /** The Component MemoryMap */
  MemoryMap* mmap;
  /** Drawing Number of the Component */
  uint32_t drawing_number;
  /** Component Version */
  uint32_t version;
  /** Component Revision */
  uint32_t revision;
  /** Last Build Date */
  uint32_t date;
  /** Optional shadow memory used to hold */
  void* shadow_memory;

} DRComponent;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// DRComponentValueInfo
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @brief  Digital Receiver Component Data Information
 */
typedef struct DRComponentValueInfo
{
  /** The register address the value is located at */
  const uint64_t addr;
  /** The mask of which the value exists in */
  const uint64_t mask;
  /** The offset at which the value starts at in the register */
  const uint8_t offset;
  /** Optional Description to give register */
  const char* description;

} DRComponentValueInfo;

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Header Value Information to Digital Receiver Components
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define DRCOMPONENT_VALUE_INFO_HEADER_DRAWING_NUMBER ((DRComponentValueInfo){ .addr=0x000, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_HEADER_VERSION        ((DRComponentValueInfo){ .addr=0x004, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_HEADER_REVISION       ((DRComponentValueInfo){ .addr=0x004, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_HEADER_DATE           ((DRComponentValueInfo){ .addr=0x004, .mask=0xFFFF0000, .offset=16 })
#define DRCOMPONENT_VALUE_INFO_HEADER_ECHO           ((DRComponentValueInfo){ .addr=0x008, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a Digital Receiver Component
 * @memberof  DRComponent *
 * @param     name[16]:
 * @param     base_address: Base address to the component
 * @param     reserved_address_space: Reserved Address Space component takes
 * @retval    DRComponent *
 */
DRComponent* DRComponent_create(const char name[DRCOMPONENT_NAME_LENGTH], uint32_t* base_address, uint32_t reserved_address_space);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a Digital Receiver Component that does conform to the Catalyst Component Framework
 * @memberof  DRComponent
 * @param     name[DRCOMPONENT_NAME_LENGTH]:
 * @param     base_address:
 * @param     reserved_address_space:
 * @retval    DRComponent *
 */
DRComponent* DRComponent_create_noncatalyst_component(const char name[DRCOMPONENT_NAME_LENGTH], uint32_t* base_address, uint32_t reserved_address_space);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Gets a DRComponent by name
 * @memberof  DRComponent
 * @param     name[DRCOMPONENT_NAME_LENGTH]: Component Name
 * @retval    DRComponent *
 */
DRComponent* DRComponent_get_by_name(const char name[DRCOMPONENT_NAME_LENGTH]);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Get value of Digital Receiver Component Register
 * @memberof  DRComponent
 * @param     drc: DRComponent *
 * @param     vinfo: DRComponentValueInfo
 * @retval    uint32_t
 */
uint32_t DRComponent_get_value(DRComponent* drc, DRComponentValueInfo vinfo);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Set value of Digital Receiver Component Register
 * @memberof  DRComponent
 * @param     drc: DRComponent *
 * @param     vinfo: DRComponentValueInfo
 * @param     value: uint32_t
 * @retval    None
 */
void DRComponent_set_value(DRComponent* drc, DRComponentValueInfo vinfo, uint32_t value);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Free a Digital Receiver Component from memory
 * @memberof  DRComponent
 * @param     drc:
 * @retval    None
 */
void DRComponent_free(DRComponent* drc);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif