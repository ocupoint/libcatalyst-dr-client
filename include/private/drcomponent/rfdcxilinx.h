/// @file   rfdc_XILINXxilinx.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   05-26-2021

#ifndef _OCPCDR_RFDC_XILINX_XILINX_PRIV_H_
#define _OCPCDR_RFDC_XILINX_XILINX_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "private/memorymap.h"
#include "private/drcomponent/drcomponent.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define DRCOMPONENT_NAME_RFDC_XILINX                                                  "RF Data Converter Xilinx"
#define DRCOMPONENT_BASE_ADDR_RFDC_XILINX                                             (uint32_t*)0xA0040000 // Yosemite
// #define DRCOMPONENT_BASE_ADDR_RFDC_XILINX                                             (uint32_t*)0x80140000 // Pentek
#define DRCOMPONENT_RESERVED_ADDR_SPACE_RFDC_XILINX                                   0x0003FFFF

#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA                               0x04000
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA                               0x08000
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA                               0x0C000
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA                               0x10000
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA                               0x14000
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA                               0x18000
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA                               0x1C000
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA                               0x20000
// Configuration

#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_IP_VERSION_MAJOR                           ((DRComponentValueInfo){ .addr=0x000, .mask=0xFF000000, .offset=24 })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_IP_VERSION_MINOR                           ((DRComponentValueInfo){ .addr=0x000, .mask=0x00FF0000, .offset=16 })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_IP_VERSION_REVISION                        ((DRComponentValueInfo){ .addr=0x000, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_MASTER_RESET                               ((DRComponentValueInfo){ .addr=0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_INTC_STATUS                                ((DRComponentValueInfo){ .addr=0x100, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_INTC_ENABLE                                ((DRComponentValueInfo){ .addr=0x104, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC 0
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC 1
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC 2
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC 3
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADC 0
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADC 1
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADC 2
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADC 3
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates the RFDC Xilinx component
 * @memberof  RFDCXilinx
 * @retval    DRComponent *
 */
DRComponent* RFDCXilinx_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Setup RFDC Xilinx component
 * @memberof  RFDCXilinx
 * @param     drc:
 * @retval    DRComponent *
 */
uint8_t RFDCXilinx_setup(DRComponent* drc);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Logs tile status in a Debug (Level=INFO) Message
 * @param  drc: DRComponent*
 * @retval None
 */
void RFDCXilinx_print_tile_status(DRComponent* drc);

#ifdef __cplusplus
}
#endif

#endif