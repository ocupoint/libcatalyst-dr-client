/// @file   rfdc.h
/// @brief  RFDC IP
/// Register Space
/// The address map, shown in the following table, is split on a per-tile basis. All banks are 8 KB.
/// The first bank contains the functions common to all tiles. Each tile has a bank for control and status.
/// | ADDR              | FUNCTION                                      |
/// | 0x00000 - 0x03FFF | IP Common Control and Status                  |
/// | 0x04000 - 0x07FFF | RF-DAC Tile 0 registers (Tile <n> Registers)  |
/// | 0x08000 - 0x0BFFF | RF-DAC Tile 1 registers (Tile <n> Registers)  |
/// | 0x0C000 - 0x0FFFF | RF-DAC Tile 2 registers (Tile <n> Registers)  |
/// | 0x10000 - 0x13FFF | RF-DAC Tile 3 registers (Tile <n> Registers)  |
/// | 0x14000 - 0x17FFF | RF-ADC Tile 0 registers (Tile <n> Registers)  |
/// | 0x18000 - 0x1BFFF | RF-ADC Tile 1 registers (Tile <n> Registers)  |
/// | 0x1C000 - 0x1FFFF | RF-ADC Tile 2 registers (Tile <n> Registers)  |
/// | 0x20000 - 0x23FFF | RF-ADC Tile 3 registers (Tile <n> Registers)  |
/// @author Nicholas Molinski
/// @date   03-11-2021


#ifndef _OCPCDR_RFDC_PRIV_H_
#define _OCPCDR_RFDC_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"
#include "private/memorymap.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdint.h>

#define DRCOMPONENT_NAME_RFDC                                                  "RF Data Converter"
#define DRCOMPONENT_BASE_ADDR_RFDC                                             (uint32_t*)0x80000000
#define DRCOMPONENT_RESERVED_ADDR_SPACE_RFDC                                   0x0003FFFF

#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA                               0x04000
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA                               0x08000
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA                               0x0C000
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA                               0x10000
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA                               0x14000
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA                               0x18000
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA                               0x1C000
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA                               0x20000
// Configuration

#define DRCOMPONENT_VALUE_INFO_RFDC_IP_VERSION_MAJOR                           ((DRComponentValueInfo){ .addr=0x000, .mask=0xFF000000, .offset=24 })
#define DRCOMPONENT_VALUE_INFO_RFDC_IP_VERSION_MINOR                           ((DRComponentValueInfo){ .addr=0x000, .mask=0x00FF0000, .offset=16 })
#define DRCOMPONENT_VALUE_INFO_RFDC_IP_VERSION_REVISION                        ((DRComponentValueInfo){ .addr=0x000, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_MASTER_RESET                               ((DRComponentValueInfo){ .addr=0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_INTC_STATUS                                ((DRComponentValueInfo){ .addr=0x100, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_INTC_ENABLE                                ((DRComponentValueInfo){ .addr=0x104, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC 0
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC 1
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC 2
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// DAC 3
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADC 0
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADC 1
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADC 2
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADC 3
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_RESTART_POWER_ON_FSM             ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA+0x004, .mask=0x00000001, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_RESTART_START_STATE              ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA+0x008, .mask=0x0000FF00, .offset=8  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_RESTART_END_STATE                ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA+0x008, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_CURRENT_STATE                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA+0x00C, .mask=0x000000FF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_INTC_STATUS                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA+0x200, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_INTC_ENABLE                      ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA+0x204, .mask=0xFFFFFFFF, .offset=0  })
// Missing some
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_COMMON_STATUS                    ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA+0x228, .mask=0xFFFFFFFF, .offset=0  })
#define DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_FIFO_DISABLE                     ((DRComponentValueInfo){ .addr=DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_BA+0x230, .mask=0xFFFFFFFF, .offset=0  })

#ifdef __cplusplus
}
#endif

#endif