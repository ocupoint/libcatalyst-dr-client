/// @file   xilinxrfdc.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   03-11-2021


#ifndef _OCPCDR_XILINX_RFDC_PRIV_H_
#define _OCPCDR_XILINX_RFDC_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  RF Tile to use for the RF Data Converter
 */
enum XILRFDC_TILE
{
  XILRFDC_TILE_NONE,
  XILRFDC_TILE_ADC0,
  XILRFDC_TILE_ADC1,
  XILRFDC_TILE_ADC2,
  XILRFDC_TILE_ADC3,
  XILRFDC_TILE_ADC4,
  XILRFDC_TILE_ADC5,
  XILRFDC_TILE_ADC6,
  XILRFDC_TILE_ADC7,
  XILRFDC_TILE_DAC0,
  XILRFDC_TILE_DAC1,
  XILRFDC_TILE_DAC2,
  XILRFDC_TILE_DAC3,
  XILRFDC_TILE_DAC4,
  XILRFDC_TILE_DAC5,
  XILRFDC_TILE_DAC6,
  XILRFDC_TILE_DAC7
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct XilinxRFDC
{
  /** PLL Reference Frequency MHz */
  uint32_t pll_reference_frequency_khz;
  /** RF Tile this data converter belongs to */
  enum XILRFDC_TILE tile;
  /** Mixer Frequency MHz */
  uint32_t mixer_frequency_khz;
  /** I2C bus descriptor */
  uint32_t i2c_bus;


} XilinxRFDC;


//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define XILINXRFDC_LMK04208_CONFIG_LENGTH 26

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Xilinx RFDC Create
 * @retval XilinxRFDC *
 */
XilinxRFDC* XilinxRFDC_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Xilinx Power up all RFDC Tiles
 * @retval 0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t XilinxRFDC_power_up_all(XilinxRFDC* xilrfdc);

//////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @brief  Logs tile status in a Debug (Level=DBUG) Message
 * @param  drc: DRComponent*
 * @retval None
 */
void XilinxRFDC_print_tile_status(DRComponent* drc);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Tunes the onboard PLL to the provided reference
 * @param  xilrfdc: XilinxRFDC
 * @param  pll_reference_freq_mhz: uint32_t
 * @retval 0=EXIT_SUCCESS; 1=EXIT_FAILURE
 */
uint8_t XilinxRFDC_tune_pll_reference_frequency(XilinxRFDC* xilrfdc, uint32_t pll_reference_frequency_mhz);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif