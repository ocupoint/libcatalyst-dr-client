/// @file   ocp.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-19-2021

#ifndef _LIBCATLYST_DR_CLIENT_OCP_PRIV_H_
#define _LIBCATLYST_DR_CLIENT_OCP_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <time.h>

// Environment variables
#define __DENV_STR(x) #x
#define __DENV(x) __DENV_STR(x)

#define OCPCDR_VERSION __DENV(ENV_VERSION_REVISION)
#define OCPCDR_BUILD_TIME __DENV(ENV_BUILD_TIMESTAMP)


#if defined _WIN32 || defined __CYGWIN__
  #ifdef BUILDING_DLL
    #ifdef __GNUC__
      #define __export__ __attribute__ ((dllexport))
    #else
      #define __export__ __declspec(dllexport)
    #endif
  #else
    #ifdef __GNUC__
      #define __export__ __attribute__ ((dllimport))
    #else
      #define __export__ __declspec(dllimport)
    #endif
  #endif
#else
  #if __GNUC__ >= 4
    #define __export__ __attribute__ ((visibility ("default")))
  #else
    #define __export__
  #endif
#endif


#ifdef __cplusplus
}
#endif

#endif