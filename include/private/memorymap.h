/// @file   MemoryMap.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-20-2021


#ifndef _LIBCATLYST_DR_CLIENT_MEMORY_MAP_PRIV_H_
#define _LIBCATLYST_DR_CLIENT_MEMORY_MAP_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

/// Define maximum number of clients that can exist on one server
#ifndef __OCPCATALYST_MAX_MEMORY_MAP_COUNT
#define __OCPCATALYST_MAX_MEMORY_MAP_COUNT 15
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// MemoryMap
///
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define MEMORY_MAP_STATUS_FREE      0x00
#define MEMORY_MAP_STATUS_ALLOCATED 0x01

/**
 * @brief  Memory Map manages resource allocation and consumption
 * @todo   Support split allocations
 * @struct
 * @retval None
 */
typedef struct MemoryMap
{
 /**
  * Virtual Memory Address
  */
  uintptr_t resource_virtual_memory_address;
  /**
  * Physical Memory Address
  */
  uintptr_t resource_physical_memory_addr;
  /**
  * Physical Memory Address
  */
  uint32_t resource_memory_size_bytes;
  /**
   * Memory map status
   * @todo turn to bit-field
   */
  uint8_t status;

} MemoryMap;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a Memory Map Instance
 * @memberof  MemoryMap
 * @param     memory_offset: Offset in the Physical Memory Space
 * @param     size_bytes: Size of the Physical Memory
 * @retval    MemoryMap*
 */
MemoryMap* MemoryMap_create(uint32_t* memory_offset, uint32_t size_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Frees MemoryMap, unmapping virtual and physical memory
 * @todo      Add warnings to prevent double free
 * @memberof  MemoryMap
 * @param     map:
 * @retval    None
 */
void MemoryMap_free(MemoryMap* map);

//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// MemoryMapManager
///
///
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     MemoryMapManager is an alias for a contiguous memory of MemoryMaps
 * @struct
 * @retval    None
 */
typedef MemoryMap* MemoryMapManager;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a new Memory Map Manager
 * @memberof  MemoryMapManager
 * @retval    MemoryMapManager*
 */
void MemoryMapManager_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Allocate a new memory map
 * @memberof  MemoryMapManager
 * @retval    MemoryMapManager*
 */
MemoryMap* MemoryMapManager_allocate(uint32_t* memory_offset, uint32_t size_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @brief     Returns a MemoryMap in string form
 * @memberof  MemoryMap
 * @param     map: MemoryMap *
 * @retval    char*
 */
char* MemoryMap_to_string(MemoryMap* map);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Validates a MemoryMap
 * @memberof  MemoryMap
 * @param     map: MemoryMap *
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t MemoryMap_validate(MemoryMap* map);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Find an existing memory map
 * @memberof  MemoryMapManager
 * @retval    MemoryMapManager*
 */
MemoryMap* MemoryMapManager_find(uint32_t* memory_offset, uint32_t size_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif