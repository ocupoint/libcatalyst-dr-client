/// @file   pool.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-28-2021

#ifndef _OCPCDR_POOL_PRIV_H_
#define _OCPCDR_POOL_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Pool Object Header
 * @note
 * @retval None
 */
typedef struct PoolObjectHeader
{
  /** Whether of not the Pool object is used */
  bool is_used;

} PoolObjectHeader;


//////////////////////////////////////////////////////////////////////////////////////////////////////////
///
/// Pool
///

/**
 * @brief  Pool object
 * @struct Pool
 */
typedef struct Pool
{
  /** Object Size */
  size_t            object_size;
  /** Number of Objects */
  size_t            num_objects;
  /** Number of Objects Allocated to this pool */
  size_t            num_objects_allocated;
  /** Number of Volatile objects in pool */
  size_t            num_volatile_objects;
  /** Pool Header */
  PoolObjectHeader* pool_header;
  /** Pool memory */
  void*             pool_memory;
  /** Volatile memory */
  void**            pool_volatile_memory;

} Pool;

//////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifdef __cplusplus
}
#endif

#endif