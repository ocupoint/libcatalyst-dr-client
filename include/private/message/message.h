/// @file   message.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-25-2021

#ifndef _OCPCDR_MESSAGE_PRIV_H_
#define _OCPCDR_MESSAGE_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stddef.h"
#include "stdint.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef ALIGN_STRUCT_32BIT
/** A 4 byte aligned struct (i.e 32 bits) */
#define ALIGN_STRUCT_32BIT  __attribute__((packed, aligned(4)))
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Actions contained within message
 */
enum MessageHeaderActions
{
  /** Performs a write to the memory address */
  WRITE = 0x0,
  /** Performs a read to the memory address */
  READ  = 0x1,
  /** Performs a read of the current address value, buffers the result, then writes the payload to the memory address */
  READ_WRITE = 0x2,
  /** Performs a write of the current address value, buffers the result, then reads the payload to the memory address */
  WRITE_READ = 0x3,
  /** Performs a write to the memory address immediately, giving the message high priority */
  WRITE_IMMEDIATELY = 0x4,
  /** Performs a read to the memory address, giving the message high priority */
  READ_IMMEDIATELY = 0x5,
  /** Performs a read of the current address value, buffers the result, then writes the payload to the memory address, giving the message high priority */
  READ_WRITE_IMMEDIATELY = 0x6,
  /** Performs a write of the current address value, buffers the result, then reads the payload to the memory address, giving the message high priority */
  WRITE_READ_IMMEDIATELY = 0x7,

};


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Privileges requested on message
 */
enum MessageHeaderPrivilege
{
  /** Normal user access */
  NON_PRIVILEGED = 0x0,
  /** Superuser access */
  PRIVILEGED = 0x1,

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Catalyst Digital Receiver Message Header
 *
 * Defines a message to be transmitted between a Client and a Server
 *
 * Typical with a messaging scheme, this data structure contains identifying information about the message
 * payload contents
 *
 * Binary Header Decode
 *
 * | Bits               | Field              | Type     |
 * |--------------------|--------------------|----------|
 * | `128` downto `096` | to_address         | `uint32` |
 * | `095` downto `064` | from_address       | `uint32` |
 * | `063` downto `032` | fwd_address        | `uint32` |
 * | `031` downto `024` | action             | `uint8`  |
 * | `023` downto `016` | privilege          | `uint8`  |
 * | `015` downto `000` | payload_num_bytes  | `uint16` |

 * @struct MessageHeader
 */
typedef struct ALIGN_STRUCT_32BIT MessageHeader
{
  /** Address to route message to */
  uint32_t to_address;
  /** Identifier address from client */
  uint32_t from_address;
  /** Identifier to forward information to other address */
  uint32_t fwd_address;
  /** Action to perform on the message */
  enum MessageHeaderActions action;
  /** Privilege of this message */
  enum MessageHeaderPrivilege privilege;
  /** Size of the Payload in bytes */
  uint16_t payload_num_bytes;

} MessageHeader;


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     A message is buffer containing a payload to data
 * Messages are passed back and forth between different systems. They contain
 * @struct    Message
 */
typedef struct ALIGN_STRUCT_32BIT Message
{
  /** Size of the Message in bytes */
  size_t size_bytes;
  /** Payload of the message */
  void* payload;

} Message;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates a Message
 * @memberof  Message
 * @retval    Message *
 */
Message* Message_create();

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates a Message Payload
 * @memberof  Message
 * @param     payload_size_bytes: The size of the message payload in bytes
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t Message_create_payload(Message* msg, size_t payload_size_bytes);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Frees a Message
 * @param  msg: Message
 * @retval None
 */
void Message_free(Message* msg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif