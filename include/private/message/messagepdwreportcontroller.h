/// @file   messagepdwreportcontroller.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   03-01-2021

#ifndef _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_
#define _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the PDW Amplitude Threshold
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MesagePDWReportController_set_pdw_amplitude_threshold(void* msg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the Amplitude Offset
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MesagePDWReportController_set_amplutude_offset(void* msg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the PDW Rejects
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MessagePDWReportController_set_pdw_rejects(void* msg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the PDW Generation Source
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MesagePDWReportController_set_pdw_generation_source(void* msg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the PDW Report Configuration
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MessagePDWReportController_set_configuration(void* msg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the Platform Heading
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MesagePDWReportController_set_platform_heading(void* msg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the Band Configuration
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MessagePDWReportController_set_band_configuration(void* msg);

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif