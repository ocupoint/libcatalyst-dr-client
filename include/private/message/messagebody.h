/// @file   messagebody.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   03-01-2021

#ifndef _OCPCDR_MESSAGE_BODY_PRIV_H_
#define _OCPCDR_MESSAGE_BODY_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  PDW Amplitude Threshold Word
 */
struct MessagePDWReportControllerPDWAmplitudeThreshold
{
  /** Message Word 1 */
  long unsigned int pdw_amplitude_threshold: 32;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  PDW Amplitude Threshold Word
 */
struct MessagePDWReportControllerPDWFormatSelection
{
  /** Message Word 1 */
  long unsigned int pdw_format_selection: 8;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  PDW Amplitude Threshold Word
 */
struct MessagePDWReportControllerConfiguration
{
  /** Message Word 1 */
  unsigned int maximum_pdws_per_message: 32;
  /** Message Word 2 */
  unsigned int maximum_detection_duration_ms: 32;
  /** Message Word 3 */
  long unsigned int virtual_buffer_address: 64;

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  PDW Amplitude Threshold Word
 */
struct MessagePDWReportsBody
{
  /** Message Word 1 */
  unsigned int num_pdws : 32;
  /** Message Word 2 */
  long unsigned int virtual_buffer_address : 64;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  PDW Rejects Word
 */
struct MessagePDWRejectsBody
{
  /** Message Word 1 */
  uint16_t frequency_low_mhz;
  /** Message Word 2 */
  uint16_t frequency_high_mhz;
    /** Message Word 3 */
  uint16_t bearing_low_decideg;
  /** Message Word 4 */
  uint16_t bearing_high_decideg;
  /** Message Word 5 */
  uint32_t pulse_width_low_ns;
  /** Message Word 6 */
  uint32_t pulse_width_high_ns;
  /** Message Word 7 */
  uint16_t amplitude_low_decidb;
  /** Message Word 8 */
  uint16_t amplitude_high_decidb;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Platform Amplitude Offset Body
 * @retval None
 */
struct MessageAmplitudeOffsetBody
{
  /** Message Word 1 */
  uint32_t amplitude_offset_decidb;
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Platform Heading Body
 * @retval None
 */
struct MessagePlatformHeadingBody
{
  /** Message Word 1 */
  uint16_t platform_heading_decideg;
};


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Band Configuration
 * @retval None
 */
struct MessageBandConfigurationBody
{
  /** Message Word 1 */
  uint32_t band_center_khz;
  /** Message Word 2*/
  uint32_t band_ibw_khz;
  /** Message Word 3 */
  uint16_t band_number;
  /** Message Word 4 */
  uint16_t band_attenuation_decidb;

};


#ifdef __cplusplus
}
#endif

#endif