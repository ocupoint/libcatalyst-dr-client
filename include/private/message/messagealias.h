/// @file   messagealias.h
/// @brief  Aliases for customer provided function names
///
/// @author Nicholas Molinski
/// @date   02-26-2021

#ifndef _OCPCDR_MESSAGE_ALIAS_PRIV_H_
#define _OCPCDR_MESSAGE_ALIAS_PRIV_H_

#ifdef __cplusplus
extern "C" {
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_MESSAGE_DIGITAL_RECEIVER_H_
__export__ void DigitalReceiverInitialize(void)                       __attribute__ ((alias ("MessageDigitalReceiver_setup")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_
__export__ void SetAmplitudeOffset(void *msgBody)                     __attribute__ ((alias ("MesagePDWReportController_set_amplutude_offset")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_LIBCATALYS_DR_DRIVER_PUB_H_
__export__ void SetDetectionSNR(void *msgBody)                        __attribute__ ((alias ("HelloWorld_SNR")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_
__export__ void SetPDWThreshold(void *msgBody)                        __attribute__ ((alias ("MesagePDWReportController_set_pdw_amplitude_threshold")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_LIBCATALYS_DR_DRIVER_PUB_H_
__export__ void SetBandStatus(void *msgBody)                          __attribute__ ((alias ("HelloWorld_Set_Band_Status")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_
__export__ void SetPDWRejects(void *msgBody)                          __attribute__ ((alias ("MessagePDWReportController_set_pdw_rejects")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_
__export__ void SetBandConfiguration(void *msgBody)                   __attribute__ ((alias ("MessagePDWReportController_set_band_configuration")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_
__export__ void SetPDWProducerToConsumerConfiguration(void *msgBody)  __attribute__ ((alias ("MessagePDWReportController_set_configuration")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_LIBCATALYS_DR_DRIVER_PUB_H_
__export__ void SetNarrowbandTune(void *msgBody)                      __attribute__ ((alias ("HelloWorld_Set_Narrowband_Tune")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_LIBCATALYS_DR_DRIVER_PUB_H_
__export__ void SetNarrowbandStepAndDwellTable(void *msgBody)         __attribute__ ((alias ("HelloWorld_Set_Narrowband_Step_Dwell")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_
__export__ void SetPDWFormatSelection(void *msgBody)                  __attribute__ ((alias ("MesagePDWReportController_set_pdw_generation_source")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_MESSAGE_PDW_REPORT_CONTROLLER_H_
__export__ void SetPlatformHeading(void *msgBody)                     __attribute__ ((alias ("MesagePDWReportController_set_platform_heading")));
__export__ void SetBearingOffset(void *msgBody)                       __attribute__ ((alias ("MesagePDWReportController_set_platform_heading")));
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _OCPCDR_LIBCATALYS_DR_DRIVER_PUB_H_
__export__ void SetDebug(void *msgBody)         __attribute__ ((alias ("HelloWorld_libcatalyst")));
#endif


#ifdef __cplusplus
}
#endif

#endif