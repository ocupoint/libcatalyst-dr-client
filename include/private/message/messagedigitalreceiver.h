/// @file   messageyosemite.h
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-25-2021

#ifndef _OCPCDR_MESSAGE_DIGITAL_RECEIVER_H_
#define _OCPCDR_MESSAGE_DIGITAL_RECEIVER_H_

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief  Setup Digital Receiver
 * Initializes Memory and sets appropriate registers to put the Digital Receiver in the
 * start state.
 * @retval None
 */
void MessageDigitalReceiver_setup();

#ifdef __cplusplus
}
#endif

#endif