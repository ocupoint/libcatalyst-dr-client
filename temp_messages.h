// /// @file   temp.h
// /// @brief  File Description
// ///
// /// @author Nicholas Molinski
// /// @date   02-09-2021

// #include "private/memorymap.h"

// /**
//  * @brief  Sets the amplitude offset
//  * @param  amplitude_offset_decidb:
//  * @retval
//  */
// void DigitalReceiver_set_amplitide_offset(MemoryMap* mmap, uint32_t amplitude_offset_decidb);

// /**
//  * @brief  Sets the Detection SNR
//  * @param  detection_snr:
//  * @retval
//  */
// void DigitalReceiver_set_detection_snr(MemoryMap* mmap, uint32_t detection_snr);


// /**
//  * @brief  Band Select Typess
//  * @retval None
//  */
// typedef enum BandSelectType
// {
//   BAND_REJECT_ALL  =0x00,
//   BAND_02G_TO_04G  =0x01,
//   BAND_04G_TO_06G  =0x02,
//   BAND_06G_TO_08G  =0x04,
//   BAND_08G_TO_10G  =0x08,
//   BAND_10G_TO_12G  =0x10,
//   BAND_12G_TO_14G  =0x20,
//   BAND_14G_TO_16G  =0x40,
//   BAND_16G_TO_18G  =0x80,

// } BandSelectType;


// /**
//  * @brief  Sets the Detection SNR
//  * @param  detection_snr:
//  * @retval
//  */
// void DigitalReceiver_set_band_enable(MemoryMap* mmap, enum BandSelectType band_mask);

// /**
//  * @brief  Sets the PDW Report Amplitude thresholding
//  * @param  threshold:
//  * @retval
//  */
// void PDWReportController_set_amplitude_threshold(MemoryMap* mmap, uint32_t threshold);


// /**
//  * @brief  Sets the PDW Report Amplitude thresholding
//  * @param  threshold:
//  * @retval
//  */
// void PDWReportController_set_amplitude_threshhold(MemoryMap* mmap, uint32_t threshold_decidb);

// /**
//  * @brief  Band Select Typess
//  * @retval None
//  */
// typedef enum PDWSource
// {
//   PDW_SOURCE_AQUISITION  =0x00,
//   PDW_SOURCE_SIMULATED   =0x01,
//   PDW_SOURCE_CALIBRATION =0x02,

// } PDWSource;


// /**
//  * @brief  Sets the PDW Source
//  * Selects whether PDWs are originating from the PDW Generator or from the Acquisition system
//  * @param  source:
//  * @retval
//  */
// void PDWReportController_set_source(MemoryMap* mmap, enum PDWSource source);

// /**
//  * @brief  Sets the maximum wait time until the PDW Generator declares it is full
//  * @param  timeout_ns: Timeout in Nanoseconds
//  * @retval None
//  */
// void PDWGenerator_set_timeout(MemoryMap* mmap, uint32_t timeout_ns);

// /**
//  * @brief  Sets the maximum number of pdws that will be accumulated before the PDWBuffer is declared full
//  * @param  timeout_ns: Timeout in Nanoseconds
//  * @retval None
//  */
// void PDWGenerator_set_max_pdws(MemoryMap* mmap, uint32_t max_pdws);

// /**
//  * @brief  Sets DMA address to write the PDWs to
//  * @param  physical_address: Physcial Memory Address
//  * @retval None
//  */
// void PDWGenerator_set_pdw_buffer_address(MemoryMap* mmap, uint32_t physical_address);

// /**
//  * @brief  PDW Reject Filter Configuration
//  */
// typedef struct PDWRejectContollerPDWRejectConfiguration
// {
//   uint32_t freq_low_mhz;
//   uint32_t freq_high_mhz;

//   uint32_t bearing_high_decideg;
//   uint32_t bearing_low_decideg;

//   uint32_t pulse_width_high_ns;
//   uint32_t pulse_width_low_ns;

//   uint32_t amplitude_high_decidb;
//   uint32_t amplitude_low_decidb;

// } PDWRejectContollerPDWRejectConfiguration;


// /**
//  * @brief  Initialize the PDW Reject Controller to fixed values in configuration
//  * @note   Beta
//  * @param  mmap:
//  * @param  conf:
//  * @param  generator_id: TBA
//  * @retval None
//  */
// void PDWRejectController_write_pdw_reject_configuration(MemoryMap* mmap, PDWRejectContollerPDWRejectConfiguration* conf, uint8_t filter_id)
