/// @file   Test_cast.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-28-2021

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

// Static test success
static bool static_test_success = true;

void test_ptr_cast()
{
  void* void_p = (void*)0xA0000000;
  uintptr_t void_u = 0xA0000000;

  // printf("%lx", void_p);
  printf("%lx", (uintptr_t)void_p);
  printf("%lx", void_u);
}

int main(void)
{
  test_ptr_cast();

  return static_test_success;
}