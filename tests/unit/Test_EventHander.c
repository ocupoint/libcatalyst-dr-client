/// @file   Test_EventHander.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-05-2021

#include <stdio.h>
#include <stdlib.h>
#include "public/event.h"
#include "private/event.h"
#include "public/components.h"

static int _static_test_1_status = EXIT_FAILURE;

void on_pdws_available_callback(enum EventHandlerType event_type, uint16_t num_pdws, PDWBufferPulseReport* buffer)
{
  printf("Event callback td %d\r\n", num_pdws);

  _static_test_1_status = EXIT_SUCCESS;
}


void test_1_crud_test()
{
  printf("Test 1\r\n");
  // Create an event
  EventHander_create(OCPCDR_EVENT_ON_PDWS_AVAILABLE);

  // Subscribe callback to emitter
  EventHander_subscribe(
    EVENT_ON_PDWS_AVAILABLE,
    ((void (*)(EventHandlerType, ...))*on_pdws_available_callback)
  );

  // Will call callback
  EventHander_emit(__OCPCDR_NUM_ARGS_OCPCDR_EVENT_ON_PDWS_AVAILABLE, EVENT_ON_PDWS_AVAILABLE, 6, 0);
}

void test_2_detail_test()
{
  EventHandlerOnPDWsAvailable_create();
  // Will cause two printouts
  EventHandlerOnPDWsAvailable_subscribe(*on_pdws_available_callback);
  EventHandlerOnPDWsAvailable_emit(7, 0);
}

int main(void)
{
  printf("Running Test_EventHandler\r\n");
  // Test simple crud test
  test_1_crud_test();
  test_2_detail_test();

  return _static_test_1_status;
}
