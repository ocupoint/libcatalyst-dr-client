/// @file   Test_EventHander.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-05-2021

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

static int _static_test_1_status = EXIT_FAILURE;

#define THRSH_POW 536870

void test_1_set_amp_thresh()
{
  // Variables
  uint16_t scale_factor;
  double  numerator;
  double  denominator;
  int16_t amplitude_decidbm;


  printf("Test 1 Set PDW Threshhold\r\n");

  amplitude_decidbm = 60;

  numerator     = (double)pow(10, (amplitude_decidbm/100));
  // denominator   = (double)pow(2, -23);

  scale_factor  = (uint16_t)round((double)((double)numerator*(double)THRSH_POW));

  printf("%x\n", scale_factor);

  _static_test_1_status = EXIT_SUCCESS;

}


int main(void)
{
  printf("Running Test_YosemitePDWGenerator\r\n");

  // Test simple crud test
  test_1_set_amp_thresh();

  return _static_test_1_status;
}
