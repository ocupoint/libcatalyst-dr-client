/// @file   main.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   03-15-2021

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <dirent.h>
#include <math.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>


/**
*
*   @file        nav5950_lmx2594.c
*
*   @brief       This file contains a collection of mid-level routines
*                for the 5950 board family.  These routines are used
*                by the Navigator library to perform common functions
*                whose implementation is specific to this board family.
*
*   $Revision:  $   $Date: $
*
*   @internal
*   $History: $
*
*   @endinternal
*/

// #include "5950_include/nav5950.h"

/**
 * Select verbosity of debug printouts
 * 0 - Disable all prints
 * 1 - Print errors
 * 2 - Print actions being performed
 * 3 - Print register settings
 */
#define LMX_DEBUG            3

static uint32_t mode = 0;
static uint8_t bits = 8;
static uint16_t delay = 1000;
static uint32_t speed = 50000;

static void transfer(int fd, uint8_t const *tx, uint8_t const *rx, size_t len);
static uint32_t lmx2594DefaultTable[] = {
    112,    0x700000,
    111,    0x6F0000,
    110,    0x6E0000,
    109,    0x6D0000,
    108,    0x6C0000,
    107,    0x6B0000,
    106,    0x6A0000,
    105,    0x690021,
    104,    0x680000,
    103,    0x670000,
    102,    0x663F80,
    101,    0x650011,
    100,    0x640000,
    99,     0x630000,
    98,     0x620200,
    97,     0x610888,
    96,     0x600000,
    95,     0x5F0000,
    94,     0x5E0000,
    93,     0x5D0000,
    92,     0x5C0000,
    91,     0x5B0000,
    90,     0x5A0000,
    89,     0x590000,
    88,     0x580000,
    87,     0x570000,
    86,     0x560000,
    85,     0x55D300,
    84,     0x540001,
    83,     0x530000,
    82,     0x521E00,
    81,     0x510000,
    80,     0x506666,
    79,     0x4F0026,
    78,     0x4E0003,
    77,     0x4D0000,
    76,     0x4C000C,
    75,     0x4B0800, // 0840
    74,     0x4A0000,
    73,     0x4906E4,
    72,     0x48001E,
    71,     0x470049, //0x47008D,
    70,     0x46C350,
    69,     0x450000,
    68,     0x4403E8,
    67,     0x430000,
    66,     0x4201F4,
    65,     0x410000,
    64,     0x401388,
    63,     0x3F0000,
    62,     0x3E0322,
    61,     0x3D00A8,
    60,     0x3C0000,
    59,     0x3B0001,
    58,     0x3A0001, //0x3A8001,
    57,     0x390020,
    56,     0x380000,
    55,     0x370000,
    54,     0x360000,
    53,     0x350000,
    52,     0x340820,
    51,     0x330080,
    50,     0x320000,
    49,     0x314180,
    48,     0x300300,
    47,     0x2F0300,
    46,     0x2E07FE, //*
    45,     0x2DC0DF,
    44,     0x2C1F23, //*
    43,     0x2B0000,
    42,     0x2A0000,
    41,     0x290000,
    40,     0x280000,
    39,     0x2703E8,
    38,     0x260000,
    37,     0x250304,
    36,     0x240064,
    35,     0x230004,
    34,     0x220000,
    33,     0x211E21,
    32,     0x200393,
    31,     0x1F43EC, // chdiv_div2 = 1
    30,     0x1E318C,
    29,     0x1D318C,
    28,     0x1C0488,
    27,     0x1B0002,
    26,     0x1A0DB0,
    25,     0x190624,
    24,     0x18071A,
    23,     0x17007C,
    22,     0x160001,
    21,     0x150401,
    20,     0x14B048, // VCO sel = 6
    19,     0x1327B7,
    18,     0x120064,
    17,     0x11012C,
    16,     0x100080,
    15,     0x0F064F,
    14,     0x0E1E70,
    13,     0x0D4000,
    12,     0x0C5001, // PLL pre = 1
    11,     0x0B0018, // R = 1
    10,     0x0A10D8,//MULT=10 -//0x0A10D8, // mult = 1
    9,      0x091604, // osc = 2x
    8,      0x082000,
    7,      0x0740B2,
    6,      0x06C802,
    5,      0x0500C8,
    4,      0x040A43,
    3,      0x030642,
    2,      0x020500,
    1,      0x010808,
    0,      0x00641C,
    0xffff, 0xffff // 0xffff end of table
};


static void pabort(const char *s)
{
        perror(s);
        abort();
}


/* Function: NAV5950_LMX2594Init() -------------------------------------- */

/**
 @brief       This function sets the LMX2594 SPI device.

 @param       boardResrc - Pointer to NAV_BOARD_RESRC

 @statusurn      status, zero for success or non-zero for failures

 @b           Example:
 @code
              NAV_BOARD_RESRC *boardResrc;
              int32_t status;

              status = NAV5950_LMX2594Init(boardResrc);
 @endcode
*/
int32_t main()
{
    uint32_t i;
    uint32_t regAddr;
    uint32_t regData;

    uint8_t writeBuf[3];
    uint8_t readBuf[3];
    uint32_t *table = lmx2594DefaultTable;
    uint8_t RecvBuffer[0x13];
    uint8_t SendBuffer;
    uint8_t outBuffer[2];
    int32_t Index;
    int32_t status;
    double FoutA_FREQx;
    uint32_t FRAC_ORDER = 0;
    double Fvco_FREQ = 0;
    double Fvco = 0;
    uint32_t chandivTable[] = { 2, 4, 6, 8, 12, 16, 24, 32, 48, 64, 72, 96,
                                128, 192, 256, 384, 512, 768 };
    uint32_t includeddivide[] = { 4, 4, 6, 4, 6, 4, 4, 4, 6, 4, 6, 6, 4, 4,
                                  4, 6, 4, 6 };
    uint32_t IncludedDiv = 0;
    uint32_t CHDIV = 0;
    uint32_t x, rval, found = 0;
    double fout;
    double fpd = 20.0;
    double Ntotal = 0; //N+NUM/DEM
    double Ntotal_fractional,Ntotal_integer;
    double calcNUM = 0;
    uint32_t PLL_N = 0;
    uint32_t PLL_NUM = 0;
    uint32_t PLL_DEN = 0;
    uint32_t PFD_DLY_SEL = 0;
    uint32_t VCO_SEL = 0;
    double VOC_CAPCTRL_STRT = 0;
    double VCO_DACISET_STRT = 0;
    double SysRef_Interp = 0;
    uint32_t SYSREF_DIV_PRE = 0;
    uint32_t SYSREF_DIV = 0;
    double SYSREF_freq= 0;
    uint32_t CHDIV_DIV2 = 0;
    double Fdesired = 10.0;
    double adc_clkout = 0.0;
    uint32_t MMCM_CLKFBOUT_MULT = 8;
    uint32_t MMCM_DIVCLK_DIVIDE = 4;
    uint32_t locked = 0;
    double calc  = 0.0;
    double freq  = 0.0;
    uint32_t CLKOUT0_DIVIDE = 0;
    uint32_t refsrc = 0;

    /* Select Reference Clock Source (0 = On-board 10MHz, 1 = 10MHz Front Panel
     * Connector, 2 = GPS Not yet Supported
     */
    refsrc = 0;

    /* Desired Sample Clock Frequency (MHz)  */
    Fdesired = 4000;
    FoutA_FREQx = Fdesired;

///////////////////////////////////////////////////////////////////////////////
//// Transmit Clock Source Settings to the Lattice
/////////////////////////////////////////////////////////////////////////////
//// Lattice
////0x00 Clock Control Register
////[1:0] <96> Sample Clock Select
////      00 = LMX2594 Output Clock
////      01 = Front Panel MMCX Coax Connector
////      10 = P2 Differential Sample Clock
////      11 = VPX SYSCLK
////[3:2] <96> SYSREF Select
////      00 =LMX2594 Output SYSREF
////      01 = P2 Differential SYSREF
////      10 = VPX AUXCLK
////      11 = PLD SYSREF (REFSYNC from front panel)
////[5:4] REFCLK Select
////      00 = On-Board 10MHz
////      01 = External Ref Clock MMCX Coax Connector
////      10 = GPS 10MHz
////      11 = Off (Also Disables REFCLK receiver)
////[6] External REFCLK Receiver FILTA Control
////[7] External REFCLK Receiver FILTB Control
//
//// For our purposes we will select the LMX2594 as sample clock source.
//// We will set the Reference clock source based on user selection.
//// GPS 10MHz is not yet supported.
//// We will set the SYSREF to be from the LMX2594
//
//
//// Select LMX Clock As Clock Source and set 10MHz Reference Source
  // Variables
  int fd_i2c;
  char fd_i2c_name[20];
  unsigned char tx_array[4];

   // Generate the path
  sprintf(fd_i2c_name, "/dev/i2c-%d\0", 1);

  fd_i2c = open(fd_i2c_name, O_RDWR);

    outBuffer[0] = 0x0;
    outBuffer[1] = (0x3 & refsrc)<<4;

    status = write(fd_i2c, &outBuffer[0], 2);
    if (status != 2)
    printf("Error writing to device %d\n", status);

    // // Detect 10MHz Reference
    // SendBuffer = 0xE; // Clock Detect Register

    // status = write(fd_i2c, &SendBuffer, 1);
    // if (status != 1)
    //     printf("Error writing to device %d\n", status);
    // status = read(fd_i2c, &RecvBuffer[0], 1);


    // if (status != 1)
    //     printf("Error reading from device %d\n", status);
    // if  ((RecvBuffer[0] & 0x1) == 0)
    // {
    //     printf("10 MHz Reference not detected. Exiting. %x\n", RecvBuffer[0]);
    //     // return 1;
    // }

//////////////////////////////////////////////////////////////////////////////
/// Based on FAout, find a VCO frequency and CHDIV within valid range
//////////////////////////////////////////////////////////////////////////////
//
#if LMX_DEBUG
    printf("\nSetting LMX2594 Output Frequency - %14.9lf MHz\n", FoutA_FREQx);
#endif
    uint32_t y = 0;
    if (FoutA_FREQx > 6400.0)
    {
        printf("6400 MHz is Maximum Output supported on the 6001!\n");
        return 12;
    }
    fout = FoutA_FREQx;
    Fvco = fout;
    for (x = 0; x < 18; x++)
    {
        rval = chandivTable[x];
        Fvco = ((double) rval) * fout;
        if ((Fvco >= 7500.0) && (fout <= 15000.0001))
        {
            //printf("Fvco = %f MHz\n",Fvco);
            for (y = 0; y < 3; y++)
            {
                IncludedDiv = includeddivide[x];
		//printf("IncludedDiv = %d \n",IncludedDiv);
                SysRef_Interp = (Fvco/ (IncludedDiv*(pow(2.0,(double)y))));
                if ((SysRef_Interp >= 750.0) && (SysRef_Interp <= 1550.0))
                {
                    found = 1;
                    break;
                }
            }
            if (found ==1)
                break;
        }
    }
    if (found)
    {
        Fvco_FREQ = Fvco;
        CHDIV = x;
        IncludedDiv = includeddivide[x];
#if LMX_DEBUG
        printf("Fvco                       = %14.9lf MHz\n",Fvco_FREQ);
        printf("CHDIV                      = %d\n",CHDIV);
        printf("Channel Division           = %d\n",rval);
        printf("Included Divide            = %d\n",IncludedDiv);
#endif
    }
    else
    {
        printf("Did not find a VCO FREQ solution.\n");
        return 123;
    }

///////////////////////////////////////////////////////////////////////////////
// Calculate PLL_N and PLL_NUM and PLL_DEN
///////////////////////////////////////////////////////////////////////////////

 // Ntotal = Fvco/(Includeddivide* fpd)
     Ntotal =  Fvco_FREQ / (((double)IncludedDiv) * fpd);
#if LMX_DEBUG
     printf("Calculated Required Ntotal = %14.9lf\n", Ntotal);
#endif
     // If it is not an integer we need to create PLL_NUM and PLL_DEN
     Ntotal_fractional = modf( Ntotal, &Ntotal_integer);
#if LMX_DEBUG
     printf("Calculated N               = %d\n", ((uint32_t)Ntotal_integer));
     printf("Calculated Fractional N    = %14.9lf\n", Ntotal_fractional);
     printf("\n");
#endif
     if (Ntotal_fractional == 0.0)
     {
         FRAC_ORDER = 0;
         // Required N is an integer. We don't need a PLL_NUM and PLL_DEN
         PLL_N = (uint32_t)Ntotal_integer;
         PLL_NUM = 0;
         PLL_DEN = 4294967295;
         // Select proper PFD_DLY_SEL
         if  (Fvco_FREQ <= 12500.0)
            PFD_DLY_SEL = 1;
         else
            PFD_DLY_SEL = 2;

#if LMX_DEBUG
         printf("Calculated PLL_N   = %d\n", PLL_N);
#endif
         // Check Minimum N
         if  (Fvco_FREQ <= 12500.0)
         {
              if (PLL_N < 28)
              {
                  printf("Error- Calculated PLL_N is below minimum 28\n");
                  return 1234;
              }
         }
         else
         {
             if (PLL_N < 32)
             {
                 printf("Error- Calculated PLL_N is below minimum 32\n");
                 return 12345;
             }
         }
     }
     else
     {
//         printf("\nSelect Fractional Division Sigma-Delta Modulator Order (1,2,3,or 4): ");
//         scanf("%d", &FRAC_ORDER);
//         printf("\n");
         FRAC_ORDER = 4;
         if ((FRAC_ORDER < 1 ) || (FRAC_ORDER > 4 ))
              FRAC_ORDER = 1;
         PLL_N = (uint32_t)Ntotal_integer;
         calcNUM = (+4294967295.0 * Ntotal_fractional);
         PLL_DEN = (uint32_t)+4294967295;
         PLL_NUM = (uint32_t)calcNUM;
#if LMX_DEBUG
         printf("Calculated PLL_N   = %d\n", PLL_N);
         printf("Calculated PLL_NUM = %u\n", PLL_NUM);
         printf("Calculated PLL_DEN = %u\n", PLL_DEN);
#endif

         // Check Minimum N
         switch(FRAC_ORDER)
         {
             case 1:
             {
                  if  (Fvco_FREQ <= 10000.0)
                  {
                       PFD_DLY_SEL = 1;
                       if (PLL_N < 28)
                       {
                           printf("Error- Calculated PLL_N is below minimum 28\n");
                           return 123456;
                       }
                  }
                  else if (Fvco_FREQ <= 12500.0)
                  {
                      PFD_DLY_SEL = 2;
                      if (PLL_N < 32)
                      {
                           printf("Error Calculated PLL_N is below minimum 32\n");
                           return 1234567;
                      }
                  }
                  else
                  {
                      PFD_DLY_SEL = 3;
                      if (PLL_N < 36)
                      {
                          printf("Error Calculated PLL_N is below minimum 36\n");
                          return 12345678;
                      }
                  }
                  break;
              }
              case 2:
              {
                   if (Fvco_FREQ <= 10000.0)
                   {
                       PFD_DLY_SEL = 2;
                       if (PLL_N < 32)
                       {
                           printf("Error- Calculated PLL_N is below minimum 32\n");
                           return 123456789;
                       }
                   }
                   else
                   {
                       PFD_DLY_SEL = 3;
                       if (PLL_N < 36)
                       {
                           printf("Error Calculated PLL_N is below minimum 36\n");
                           return 987654321;
                       }
                   }
                   break;
               }
               case 3:
               {
                   if (Fvco_FREQ <= 10000.0)
                   {
                       PFD_DLY_SEL = 3;
                       if (PLL_N < 36)
                       {
                           printf("Error- Calculated PLL_N is below minimum 36\n");
                           return 87654321;
                       }
                   }
                   else
                   {
                         PFD_DLY_SEL = 4;
                         if (PLL_N < 40)
                         {
                             printf("Error Calculated PLL_N is below minimum 40\n");
                             return 7654321;
                         }
                   }
                   break;
              }
              case 4:
              {
                   if  (Fvco_FREQ <= 10000.0)
                   {
                        PFD_DLY_SEL = 5;
                        if (PLL_N < 44)
                        {
                            printf("Error- Calculated PLL_N is below minimum 44\n");
                            return 654321;
                        }
                   }
                   else
                   {
                        PFD_DLY_SEL = 6;
                        if (PLL_N < 48)
                        {
                             printf("Error Calculated PLL_N is below minimum 48\n");
                             return 54321;
                        }
                   }
                   break;
              }
       }
   }

   CHDIV_DIV2 = 1;
#if LMX_DEBUG
   printf("PFD_DLY_SEL        = %d\n", PFD_DLY_SEL);
   printf("FRAC_ORDER         = %d\n", FRAC_ORDER);
   printf("CHDIV_DIV2         = %d\n", CHDIV_DIV2);
#endif
//////////////////////////////////////////////////////////
// Select VCO
//////////////////////////////////////////////////////////
   if (Fvco_FREQ < 8600)
   {
      VCO_SEL = 1;
      VOC_CAPCTRL_STRT = round(164-(164-12)*(Fvco_FREQ- 7500)/(8600-7500));
      VCO_DACISET_STRT = round(299-(299-240)*(Fvco_FREQ- 7500)/(8600-7500));
   }
   else if (Fvco_FREQ < 9800)
   {
      VCO_SEL = 2;
      VOC_CAPCTRL_STRT = round(165-(165-16)*(Fvco_FREQ- 8600)/(9800-8600));
      VCO_DACISET_STRT = round(356-(356-247)*(Fvco_FREQ- 8600)/(9800-8600));
   }
   else if (Fvco_FREQ < 10800)
   {
      VCO_SEL = 3;
      VOC_CAPCTRL_STRT = round(158-(158-19)*(Fvco_FREQ- 10800)/(10800-9800));
      VCO_DACISET_STRT = round(324-(324-224)*(Fvco_FREQ- 10800)/(10800-9800));
   }
   else if (Fvco_FREQ < 12000)
   {
      VCO_SEL = 4;
      VOC_CAPCTRL_STRT = round(140-(140-0)*(Fvco_FREQ- 12000)/(12000-10800));
      VCO_DACISET_STRT = round(383-(383-244)*(Fvco_FREQ- 12000)/(12000-10800));
   }
   else if (Fvco_FREQ < 12900)
   {
      VCO_SEL = 5;
      VOC_CAPCTRL_STRT = round(183-(183-36)*(Fvco_FREQ- 12900)/(12900-12000));
      VCO_DACISET_STRT = round(205-(205-146)*(Fvco_FREQ- 12900)/(12900-12000));
   }
   else if (Fvco_FREQ < 13900)
   {
      VCO_SEL = 6;
      VOC_CAPCTRL_STRT = round(155-(155-6)*(Fvco_FREQ- 13900)/(13900-12900));
      VCO_DACISET_STRT = round(242-(242-163)*(Fvco_FREQ- 13900)/(13900-12900));
   }
   else if (Fvco_FREQ < 15000)
   {
      VCO_SEL = 7;
      VOC_CAPCTRL_STRT = round(175-(175-19)*(Fvco_FREQ- 15000)/(15000-13900));
      VCO_DACISET_STRT = round(323-(323-244)*(Fvco_FREQ- 15000)/(15000-13900));
   }
#if LMX_DEBUG
   printf("VCO_SEL            = %d\n", VCO_SEL);
   printf("VOC_CAPCTRL_STRT   = %d\n", (uint32_t)(VOC_CAPCTRL_STRT));
   printf("VCO_DACISET_STRT   = %d\n", (uint32_t)(VCO_DACISET_STRT));
#endif
//////////////////////////////////////////////////////////
// Find SYSREF INTERPOLATOR Freq
//////////////////////////////////////////////////////////
   // 850MHz <= SYSREF INTERP >= 1500MHz
   //SysRef_Interp = (Fvco_FREQ/ (IncludedDiv*SYSREF_DIV_PRE)
   found = 0;
   for (x = 0; x < 3; x++)
   {
        SysRef_Interp = (Fvco_FREQ/ (IncludedDiv*(pow(2.0,(double)x))));
        SYSREF_DIV_PRE = (uint32_t)(pow(2.0,(double)x));
#if LMX_DEBUG
        printf("SYSREF_DIV_PRE     = %d\n",SYSREF_DIV_PRE);
#endif
        if ((SysRef_Interp >= 750.0) && (SysRef_Interp <= 1550.0))
        {
            found = 1;
            break;
        }
    }
    if (found == 0)
    {
        printf("Did not find valid SYSREF INTERPOLATOR Freq solution.\n");
        return 54321;
    }
#if LMX_DEBUG
    else
    {
        printf("SYSREF_DIV_PRE     = %d\n",SYSREF_DIV_PRE);
        printf("SYSREF_INTERP      = %14.9lf MHz\n",SysRef_Interp);
    }
#endif


//////////////////////////////////////////////////////////
// Find SYSREF DIV
//////////////////////////////////////////////////////////
// SYSREF_freq =  Fvco_FREQ/(SYSREF_DIV_PRE * IncludedDiv * SYSREF_DIV*2)
// We will try to make SYSREF DIV = 128
// If the result is > 10MHz, we will set it to 256
// Note: SYSREF_DIV Setting 0=4, 1=6, 2=8 ...
   SYSREF_freq = Fvco_FREQ/(SYSREF_DIV_PRE * IncludedDiv * 128 * 2);
   if (SYSREF_freq > 10.0)
   {
       SYSREF_DIV = (256-4)/2;
       SYSREF_freq = Fvco_FREQ/(SYSREF_DIV_PRE * IncludedDiv * 256 * 2);
   }
   else
   {
      SYSREF_DIV = (128-4)/2;
      SYSREF_freq = Fvco_FREQ/(SYSREF_DIV_PRE * IncludedDiv * 128 * 2);
   }
#if LMX_DEBUG
   printf("SYSREF_DIV         = %d\n", SYSREF_DIV);
   printf("SYSREF Frequency   = %14.9lf MHz\n", SYSREF_freq);
#endif

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
// End Calculations
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// Modify the Table with new values
////////////////////////////////////////////////////////////////////////


//    VCO_DACISET_STRT   R17[8:0]
//    VCO_SEL            R20[13:11]
//    CHDIV_DIV2         R31[14]
//    PLL_N[18:16]       R34[2:0]
//    PLL_N[15:0]        R36[15:0]
//    PFD_DLY_SEL        R37[13:8]
//    PLL_DEN[31:16]     R38[15:0]
//    PLL_DEN[15:0]      R39[15:0]
//    PLL_NUM[31:16]     R42[15:0]
//    PLL_NUM[15:0]      R43[15:0]
//    MASH_ORDER         R44[2:0]
//    SYSREF_DIV_PRE     R71[7:5]
//    SYSREF_DIV         R72[10:0]
//    CHDIV              R75[10:6]
//    VOC_CAPCTRL_STRT   R78[8:1]

#if LMX_DEBUG
printf("\nModified Parameters:\n");
printf(  "-------------------------------------------\n");
printf(  " VCO_DACISET_STRT  = %u\n", (uint32_t)VCO_DACISET_STRT);
printf(  " VCO_SEL           = %u\n", (uint32_t)VCO_SEL);
printf(  " CHDIV_DIV2        = %u\n", (uint32_t)CHDIV_DIV2);
printf(  " PLL_N             = %u\n", (uint32_t)PLL_N);
printf(  " PFD_DLY_SEL       = %u\n", (uint32_t)PFD_DLY_SEL);
printf(  " PLL_DEN           = %u\n", (uint32_t)PLL_DEN);
printf(  " PLL_NUM           = %u\n", (uint32_t)PLL_NUM);
printf(  " MASH_ORDER        = %u\n", (uint32_t)FRAC_ORDER);
printf(  " SYSREF_DIV_PRE    = %u\n", (uint32_t)SYSREF_DIV_PRE);
printf(  " SYSREF_DIV        = %u (Divide by %u)\n", (uint32_t)SYSREF_DIV,(((uint32_t)SYSREF_DIV)*2)+4);
printf(  " CHDIV             = %u\n", (uint32_t)CHDIV);
printf(  " VOC_CAPCTRL_STRT  = %u\n", (uint32_t)VOC_CAPCTRL_STRT);

printf("\nModifying Table...\n");
#endif

//    VCO_DACISET_STRT   R17[8:0]
table[224-(2*17)+1] = (0x00FFFE00 & table[224-(2*17)+1]) | ((0x000001FF & (uint32_t)(VCO_DACISET_STRT)));
//    VCO_SEL            R20[13:11]
table[224-(2*20)+1] = (0x00FFC7FF & table[224-(2*20)+1]) | ((0x00000007 & (uint32_t)(VCO_SEL))<<11);
//    CHDIV_DIV2         R31[14]
table[224-(2*31)+1] = (0x00FFBFFF & table[224-(2*31)+1]) | ((0x00000001 & (uint32_t)(CHDIV_DIV2))<<14);
//    PLL_N[18:16]       R34[2:0]
table[224-(2*34)+1] = (0x00FFFFF8 & table[224-(2*34)+1]) | ((0x00070000 & (uint32_t)(PLL_N))>>16);
//    PLL_N[15:0]        R36[15:0]
table[224-(2*36)+1] = (0x00FF0000 & table[224-(2*36)+1]) | ((0x0000FFFF & (uint32_t)(PLL_N)));
//    PFD_DLY_SEL        R37[13:8]
table[224-(2*37)+1] = (0x00FFC0FF & table[224-(2*37)+1]) | ((0x0000003F & (uint32_t)(PFD_DLY_SEL))<<8);
//    PLL_DEN[31:16]     R38[15:0]
table[224-(2*38)+1] = (0x00FFC0FF & table[224-(2*38)+1]) | ((0xFFFF0000 & (uint32_t)(PLL_DEN))>>16);
//    PLL_DEN[15:0]      R39[15:0]
table[224-(2*39)+1] = (0x00FF0000 & table[224-(2*39)+1]) | ((0x0000FFFF & (uint32_t)(PLL_DEN)));
//    PLL_NUM[31:16]     R42[15:0]
table[224-(2*42)+1] = (0x00FFC0FF & table[224-(2*42)+1]) | ((0xFFFF0000 & (uint32_t)(PLL_NUM))>>16);
//    PLL_NUM[15:0]      R43[15:0]
table[224-(2*43)+1] = (0x00FF0000 & table[224-(2*43)+1]) | ((0x0000FFFF & (uint32_t)(PLL_NUM)));
//    MASH_ORDER         R44[2:0]
table[224-(2*44)+1] = (0x00FFFFF8 & table[224-(2*44)+1]) | ((0x00000007 & (uint32_t)(FRAC_ORDER)));
//    SYSREF_DIV_PRE     R71[7:5]
table[224-(2*71)+1] = (0x00FFFF3F & table[224-(2*71)+1]) | ((0x00000007 & (uint32_t)(SYSREF_DIV_PRE))<<5);
//    SYSREF_DIV         R72[10:0]
table[224-(2*72)+1] = (0x00FFF800 & table[224-(2*72)+1]) | ((0x000007FF & (uint32_t)(SYSREF_DIV)));
//    CHDIV              R75[10:6]
table[224-(2*75)+1] = (0x00FFF83F & table[224-(2*75)+1]) | ((0x0000001F & (uint32_t)(CHDIV))<<6);
//    VOC_CAPCTRL_STRT   R78[8:1]
table[224-(2*78)+1] = (0x00FFFE01 & table[224-(2*78)+1]) | ((0x000000FF & (uint32_t)(CHDIV))<<1);


  char dev[20] = "/dev/spidev1.0";
  uint8_t buf[3];
  int fd;

  mode = 0;

  if ((fd = open(dev, O_RDWR)) < 0)
  {
        pabort("can't open device");
  }

      /*
     * spi mode
     */
    status = ioctl(fd, SPI_IOC_WR_MODE32, &mode);
    if (status == -1)
        pabort("can't set spi mode");

    status = ioctl(fd, SPI_IOC_RD_MODE32, &mode);
    if (status == -1)
        pabort("can't get spi mode");

    /*
     * bits per word
     */
    status = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (status == -1)
        pabort("can't set bits per word");

    status = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (status == -1)
        pabort("can't get bits per word");

    /*
     * max speed hz
     */
    status = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (status == -1)
        pabort("can't set max speed hz");

    status = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (status == -1)
        pabort("can't get max speed hz");

#if LMX_DEBUG
    printf("spi mode: 0x%x\n", mode);
    printf("bits per word: %d\n", bits);
    printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);
#endif

    // Set MUX Output to readback
    writeBuf[0] = 0x0;   // Addr
    //writeBuf[1] = 0x0;   //
    //writeBuf[2] = 0x0;
    writeBuf[1] = 0x10;   //
    writeBuf[2] = 0x64;
    transfer(fd, writeBuf, readBuf, sizeof(writeBuf));

    usleep(1000);
 //   printf("%x %x %x\n", readBuf[0], readBuf[1], readBuf[2]);
    //Reset Chip
    writeBuf[0] = 0x0;   // Addr
//    writeBuf[1] = 0x2;   // Reset chip (Bit1 on - NOT self clearing)
//    writeBuf[2] = 0x0;
    writeBuf[1] = 0x12;   // Reset chip (Bit1 on - NOT self clearing)
    writeBuf[2] = 0x64;
    transfer(fd, writeBuf, readBuf, sizeof(writeBuf));
    usleep(1000);
    writeBuf[0] = 0x0;   // Addr
//    writeBuf[1] = 0x0;   // Reset chip (Bit1 on - NOT self clearing)
//    writeBuf[2] = 0x0;
    writeBuf[1] = 0x10;   // Reset chip (Bit1 on - NOT self clearing)
    writeBuf[2] = 0x64;
    transfer(fd, writeBuf, readBuf, sizeof(writeBuf));
    usleep(1000);

    i = 0;
    while (1)
    {
        regAddr = table[i++];
        if (regAddr == 0xffff)
            break;
        else if (regAddr > 110) // Registers above 106 are read-only
            continue;
        regData = table[i++] & 0xffff;
        writeBuf[0] = regAddr & 0xff;
        writeBuf[1] = (regData >> 8) & 0xff;
        writeBuf[2] = regData & 0xff;
        transfer(fd, writeBuf, readBuf, sizeof(writeBuf));
        usleep(1000);
//        printf("%x %x %x\n", readBuf[0], readBuf[1], readBuf[2]);

#if LMX_DEBUG
      if (1)
      {
          printf("Wrote Reg %2d, 0x%x \r\n", regAddr, regData);
          writeBuf[0] |= 0x80;
          transfer(fd, writeBuf, readBuf, sizeof(writeBuf));
          printf("Readback %2d, 0x%x => %x %x %x\r\n", regAddr, regData, readBuf[0], readBuf[1], readBuf[2]);
      }
#endif
    }

//////////////////////////////////////////////////////////////////////////////
// Check for LMX2594 Lock
//////////////////////////////////////////////////////////////////////////////
    locked = 0;

    // Output of SPI is set to be lock condition, so reading all 1s means lock.
    // Wait up to 3 seconds for lock before failing.
    for (x = 0; x < 30; x++)
    {
        usleep(100000);
        writeBuf[0]  = 0x6E;
        writeBuf[0] |= 0x80; //Read
        transfer(fd, writeBuf, readBuf, sizeof(writeBuf));
        if ((readBuf[0] = 0xFF) && (readBuf[1] = 0xFF) && (readBuf[2] = 0xFF))
       {
           locked = 1;
           break;
       }
    }
    if (locked == 0)
    {
       printf("\n\nLMX2594 failed to lock! Exiting.\n");
       return 321;
    }
#if LMX_DEBUG
    else
    {
        printf("\n\nLMX2594 is Locked.\n");
    }
#endif

//////////////////////////////////////////////////////////////////////////////
// Set ADC MMCM based on sample rate
//////////////////////////////////////////////////////////////////////////////
// MMCM after ADC doubles the (Sample Rate/16) clock output from the ADC
// Its VCO must be kept in the range 800 to 1600.
// VCO Frequency = (Input Clock Frequency) * (CLKFBOUT_MULT)/DIVCLK_DIVIDE
    adc_clkout =  FoutA_FREQx/16.0;
    y = 2;
    found = 0;
    for (x = 0; x < 8; x++)
    {
       if (((adc_clkout * y) > 800.0) && ((adc_clkout * y) < 1600.0))
       {
           MMCM_CLKFBOUT_MULT = y;
           CLKOUT0_DIVIDE = y/2;
           found = 1;
           break;
       }
       else
          y = y * 2;
    }
    MMCM_DIVCLK_DIVIDE = 1;

#if LMX_DEBUG
    printf("\n\n");
    printf("Setting ADC MMCM Multiplier.         MMCM_CLKFBOUT_MULT = %d\n", MMCM_CLKFBOUT_MULT);
    printf("Setting ADC MMCM Divider.            MMCM_DIVCLK_DIVIDE = %d\n", MMCM_DIVCLK_DIVIDE);
    printf("Setting ADC MMCM CLKOUT0 Divider.    CLKOUT0_DIVIDE     = %d\n", CLKOUT0_DIVIDE);
    printf("MMCM VCO Frequency  (Valid Range = 800 to 1600)         = %14.9lf MHz\n", (adc_clkout*MMCM_CLKFBOUT_MULT/MMCM_DIVCLK_DIVIDE));
#endif

    // MMCM Clock Configuration Register 0 - 0x200
    // Bit[7:0] = DIVCLK_DIVIDE  Eight bit divide value applied to all output clocks.
    // Bit[15:8] = CLKFBOUT_MULT Integer part of multiplier value.
    // Bit[25:16] = CLKFBOUT_FRAC Multiply Fractional part of multiplier value.

uint32_t adcMMCMRegs[161];
    // MMCM Clock Configuration Register 2 - 0x208
    // Bit[7:0] = CLKOUT0_DIVIDE Integer part of clkout0 divide value
    // Bit[17:8] = CLKOUT0_FRAC Divide
    uint32_t z = 0;

    adcMMCMRegs[0] = 0xA; // reset
    usleep(10000);
    // Set MMCM values
    adcMMCMRegs[128] = 0x0FFFF & ((MMCM_CLKFBOUT_MULT<<8) | MMCM_DIVCLK_DIVIDE);
    usleep(1);
    adcMMCMRegs[129] = 0;
    usleep(1);
    adcMMCMRegs[130] = 0x0FF & CLKOUT0_DIVIDE;
    usleep(1);
    adcMMCMRegs[131] = 0;
    usleep(1);
    adcMMCMRegs[132] = 0xC350;
    usleep(1);
    adcMMCMRegs[133] = 0x0FF & CLKOUT0_DIVIDE;
    usleep(1);
    adcMMCMRegs[134] = 0;
    usleep(1);
    adcMMCMRegs[135] = 0xC350;
    usleep(1);
    adcMMCMRegs[136] = 0x0FF & CLKOUT0_DIVIDE;
    usleep(1);
    adcMMCMRegs[137] = 0;
    usleep(1);
    adcMMCMRegs[138] = 0xC350;
    usleep(1);
    adcMMCMRegs[139] = 0x0FF & CLKOUT0_DIVIDE;
    usleep(1);
    adcMMCMRegs[140] = 0;
    usleep(1);
    adcMMCMRegs[141] = 0xC350;
    usleep(1);
    adcMMCMRegs[142] = 0x0FF & CLKOUT0_DIVIDE;
    usleep(1);
    adcMMCMRegs[143] = 0;
    usleep(1);
    adcMMCMRegs[144] = 0xC350;
    usleep(1);
    adcMMCMRegs[145] = 0x0FF & CLKOUT0_DIVIDE;
    usleep(1);
    adcMMCMRegs[146] = 0;
    usleep(1);
    adcMMCMRegs[147] = 0xC350;
    usleep(1);
    adcMMCMRegs[148] = 0x0FF & CLKOUT0_DIVIDE;
    usleep(1);
    adcMMCMRegs[149] = 0;
    usleep(1);
    adcMMCMRegs[150] = 0xC350;
    usleep(1);

    // Load MMCM values
    adcMMCMRegs[151] = 0x3; // Load and Enable Config Reg values
 //   printf("25\n");
    usleep(1);

    locked = 0;

    for (y = 0; y < 20; y++)
    {
        if ((adcMMCMRegs[151] & 0x1) == 1)
           usleep(1000);
        else
       {
#if LMX_DEBUG
           printf("\nMMCM Reconfiguration completed.\n");
#endif
           break;
       }
    }
    if (y > 19)
    {
        printf("\nMMCM Reconfiguration not ready! Exiting.\n");
        return 21;
    }

    adcMMCMRegs[0] = 0xA; // reset the MMCM

    // Wait up to 1 seconds for lock before failing.
    for (x = 0; x < 10; x++)
    {
        usleep(100000);
        locked = adcMMCMRegs[1] & 0x1;
        if (locked)
           break;
    }
    if (locked == 0)
    {
        printf("\n\nADC MMCM failed to lock! Exiting.\n");
    }
#if LMX_DEBUG
    else
    {
        printf("\n\nADC MMCM is Locked.\n");
    }
#endif

#if LMX_DEBUG
    printf("\n\nReading Actual Detected Clock Rates...\n");
#endif

    /////////////////////////////////////////////////////////////////////
    // Toggle REFSYNC Register in Lattice
    /////////////////////////////////////////////////////////////////////
    usleep(20000);

    // Disable REFSYNC
    outBuffer[0] = 0x1;
    outBuffer[1] = 0x0;
    status = write(fd_i2c, &outBuffer[0], 2);
    if (status != 2)
        printf("Error writing to device %d\n", status);

    // Enable REFSYNC
    outBuffer[0] = 0x1;
    outBuffer[1] = 0x1;
    status = write(fd_i2c, &outBuffer[0], 2);
    if (status != 2)
        printf("Error writing to device %d\n", status);
    // Enable REFSYNC
 /*   outBuffer[0] = 0x1;
    outBuffer[1] = 0x0;
    status = write(XIicDevFile, &outBuffer[0], 2);
    if (status != 2)
        printf("Error writing to device %d\n", status);*/
    sleep(2); //Wait for frequency counters to settle (2 seconds is enough. It updates once per second.)

#if LMX_DEBUG
    // printf("\n\n");
    // calc = ((double)b5950Params->adcOutputClockFreqRegs[1]*16.0)/1000000000.0;
    // printf("Detected Sample Clock Rate  = % 14.9lf GHz\n", calc);
    // calc = ((double)b5950Params->adcOutputClockFreqRegs[1])/1000000.0;
    // printf("Detected ADC Output Clock   = % 14.9lf MHz\n", calc);
    // calc = ((double)b5950Params->adcMMCMClockFreqRegs[1])/1000000.0;
    // printf("Detected MMCM Clock         = % 14.9lf MHz\n", calc);
    // freq = calc;
    // calc = ((double)b5950Params->dacOutputClockFreqRegs[1])/1000000.0;
    // printf("Detected DAC Output Clock   = % 14.9lf MHz\n", calc);
    // calc = (freq/((double)b5950Params->sysrefPeriodRegs[1]));
    // printf("Detected SYSREF Period      = % d  \n",b5950Params->sysrefPeriodRegs[1] );
    // printf("Detected SYSREF Frequency   = % 14.9lf MHz\n\n\n", calc);
#endif

    return (121);
}
static void transfer(int fd, uint8_t const *tx, uint8_t const *rx, size_t len)
{
        int ret;
        int out_fd;
        struct spi_ioc_transfer tr = {};
                tr.tx_buf = (unsigned long)tx;
                tr.rx_buf = (unsigned long)rx;
                tr.len = len;
                tr.delay_usecs = delay;
                tr.speed_hz = speed;
                tr.bits_per_word = bits;

        if (mode & SPI_TX_QUAD)
                tr.tx_nbits = 4;
        else if (mode & SPI_TX_DUAL)
                tr.tx_nbits = 2;
        if (mode & SPI_RX_QUAD)
                tr.rx_nbits = 4;
        else if (mode & SPI_RX_DUAL)
                tr.rx_nbits = 2;
        if (!(mode & SPI_LOOP)) {
                if (mode & (SPI_TX_QUAD | SPI_TX_DUAL))
                        tr.rx_buf = 0;
                else if (mode & (SPI_RX_QUAD | SPI_RX_DUAL))
                        tr.tx_buf = 0;
        }

        ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
        if (ret < 1)
                pabort("can't send spi message");

}