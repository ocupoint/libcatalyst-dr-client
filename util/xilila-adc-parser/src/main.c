/// @file   main.c
/// @brief  Spi-test main
///
/// @author Nicholas Molinski
/// @date   05-30-2021


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <linux/spi/spidev.h>

#define MAX_LENGTH 1024

/**
 * @brief  Converts Character to Hexadecimal Character
 * @param  value: character
 * @retval uint8_t
 */
uint8_t char2hex(char value)
{
  uint8_t retval;

  if (value >= '0' && value <= '9')
  {
    retval = value - '0';
  }
  else if (value >= 'A' && value <= 'F')
  {
    retval = value - 'A' + 0xA;
  }
  else if (value >= 'a' && value <= 'f')
  {
    retval = value - 'a' + 0xA;
  }
  else
  {
    retval = 0xF;
  }

  return retval;
}

/**
 * @brief  Convert a string into a hex string
 * @param  *str:
 * @param  *hex:
 * @param  length:
 * @retval None
 */
void string2hex(char *str, uint8_t *hex, size_t length)
{
  uint8_t nibble;

  for (size_t ii = 0; ii < (length * 2); ii++)
  {
    if (ii < strlen(str))
    {
      nibble = char2hex(str[ii]);
    }
    else
    {
      nibble = 0xf;
    }
    if (ii % 2 == 0)
    {
      hex[ii / 2] = nibble;
    }
    else
    {
      hex[ii / 2] = (hex[ii / 2] << 4) | nibble;
    }
  }
}


void print_usage(void)
{
  printf("USAGE: xilila-adc-parser -f <filename> -d <num_data_lanes>\n"
  "                       -f,--filename filename: name of the ila file\n"
  "                       -d,--datalanes num_data_lanes: number of datalanes\n"
  "                       -c,--column column: column of data in csv file to parse\n"
  "EX: xilila-adc-parser -f ./capture.csv -d 7 -c 5");
}


int main(int argc, char *argv[])
{
  char* filename;
  char* filename_out[MAX_LENGTH];
  uint8_t num_datalanes;
  int column;
  int bufferLength = MAX_LENGTH;
  char buffer[bufferLength];
  char c;
  int opt_i = 0;
  FILE* fd;
  FILE* fp_out;
  // int ret;

  // Generate a output filename

  filename      = NULL;
  num_datalanes = 0;
  column        = 0;

  static struct option long_opts[] = {
    { "filename", required_argument, 0, 'f' },
    { "datalanes", required_argument, 0, 'd' },
    { "column", required_argument, 0, 'c' },
    { "help", no_argument, 0, '?' },
    { 0, 0, 0, 0 },
  };

  while ((c = getopt_long(argc, argv, "f:d:c:?",
        long_opts, &opt_i)) != -1) {
    switch (c) {
    case 'f':
      filename = optarg;
      break;
    case 'd':
      num_datalanes = MIN(atoi(optarg), MAX_LENGTH);
      break;
    case 'c':
      column = MIN(atoi(optarg), MAX_LENGTH);
      break;
    case '?':
      print_usage();
      return 0;
    }
  }

  if (filename == NULL) {
    fprintf(stderr, "Missing required filename argument.\n");
    print_usage();
    return -1;
  }

  if (num_datalanes == 0)
  {
    fprintf(stderr, "Missing datalanes argument.\n");
    print_usage();
    return -1;
  }

  // Generate output filename
  sprintf(filename_out, "%s_parsed.csv", filename);

  fd      = fopen(filename, "r");
  fp_out  = fopen(filename_out, "w");


  if (fd == NULL)
  {
    fprintf(
      stderr,
      "main: opening device file: %s: %s\n",
      filename, strerror(errno)
    );
    return -1;
  }
  if (fp_out == NULL)
  {
    fprintf(
      stderr,
      "main: opening device file: %s: %s\n",
      filename_out, strerror(errno)
    );
    return -1;
  }

  // Print arguments
  printf(
    "Arguments:\r\n\tFilename: %s\r\n\tOutput Filename: %s\r\n\tNumber of Datalanes: %d\r\n\tData Column: %d\r\n",
    filename,
    filename_out,
    num_datalanes,
    column
  );

  char* search_buffer = malloc(sizeof(char)*bufferLength);
    // printf("%s\n", buffer);
  uint16_t count = 0;
  uint8_t  str_len;
  uint8_t  data_len;


  while(fgets(buffer, bufferLength, fd))
  {
    printf("Row: %s\r\n", buffer);

    strcpy(search_buffer, buffer);
    char* ptr = strtok(search_buffer, ",");

    for (size_t ii = 0; ii < column; ii++)
    {
      ptr = strtok(NULL, ",");

      if (search_buffer == NULL)
      {
        fprintf(
          stderr,
          "Column does not exist: %d\n",
          column
        );
      }
    }

    // Title line
    if (count == 0)
    {
      printf("Column Found: %s\r\n", ptr);
      fwrite(ptr, strlen(ptr), 1, fp_out);
      fwrite("\n", 1, 1, fp_out);
    }
    // Radix line
    else if (count == 1)
    {
      printf("Radix: %s\r\n", ptr);
      // fwrite(ptr, strlen(ptr), 1, fp_out);
      // fwrite("\n", 1, 1, fp_out);
    }
    else
    {
      // ptr will be something like this
      // 0004fffc00340004ffe8001cffd8fffc
      // We will splice and output
      printf("Data: %s\r\n", ptr);

      str_len   = strlen(ptr);
      data_len  = str_len/num_datalanes;

      char payload[data_len + 1];

      for (size_t ii = 0; ii < num_datalanes; ii++)
      {
        for (size_t jj = 0; jj < data_len; jj++)
        {
          // printf("%c", ptr[ii*data_len + jj]);
          payload[jj] = ptr[ii*data_len + jj];
        }

        payload[data_len] = '\0';
        // Really 12 bits not 16
        int16_t as_int = (int16_t)strtol(payload, NULL, 16) >> 4;
        // printf("\r\n");
        printf("fwrite %d\n", as_int);
        fprintf(fp_out, "%d\n", as_int);
      }

    }

    count++;
  }

  fclose(fd);
  fclose(fp_out);

  return 0;
}
