% File: plot_parsed_data.m
% Author: Nicholas Molinski
% Date: 06/09/21

%% Name the file
%CSV_FILE = "./examples/rfdc_bd_wrapper_700mhz_p00dbm.csv_parsed.csv"

%CSV_FILE = "./examples/rfdc_bd_wrapper_1400mhz_p00dbm.csv_parsed.csv"
%CSV_FILE = "./examples/rfdc_bd_wrapper_1400mhz_p00dbm_wo_sysref.csv_parsed.csv"
%CSV_FILE = "./examples/rfdc_bd_wrapper_900mhz_p00dbm.csv_parsed.csv"
%CSV_FILE = "./examples/rfdc_bd_wrapper_900mhz_p00dbm_wo_sysref.csv_parsed.csv"
CSV_FILE = "adc_2_rfdc_bd_wrapper_0400mhz_m10dbm_resetup.csv_parsed.csv";
%% Configure FFT
FS = 4000;
L  = 4096;

%% Read into CSV
S = readtable(CSV_FILE);

% Convert the table to a array of 16 bit
signal = int16(table2array(S(:,1)));

ret = fft(signal(1:L));

%% Count Bins
bin_resolution = (FS/L); % MHz

disp("This capture has a bin resolution of " + bin_resolution + "MHz");
%% Plot FFT
mag0 = abs(ret/L);
% Get single sided spectrum
mag  = mag0(1:L/2+1);
mag(2:end-1) = 2*mag(2:end-1);
freq = FS*(0:(L/2))/L;

T      = 1/FS;                  % period
time   = (0:L-1)*T;             % time vector  

fig = figure(1);

subplot(1,2,1, "parent", fig); 
plot(time, signal(1:L));
title("Original Signal");
%set(gca,"XTick",0:pi/2:time(length(time)-1));

subplot(1,2,2, "parent", fig); 
plot(freq, mag);
title("Single Sided FFT");       
ylabel("Magnitude"); 
%set(gca,"XTick",0:FS/8:FS/2);
