# Xilinx ILA ADC Parser

Parse ILA Data to common csv format

## Usage

```
USAGE: xilila-adc-parser -f <filename> -d <num_data_lanes> -c <column>
                          -f,--filename filename: name of the ila file
                          -d,--datalanes num_data_lanes: number of datalanes
                          -c,--column column: column data is in
  EX: xilila-adc-parser -f ./capture.csv -d 7 -c 3
```