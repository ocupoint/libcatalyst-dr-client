/// @file   main.c
/// @brief  Spi-test main
///
/// @author Nicholas Molinski
/// @date   05-30-2021


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <linux/spi/spidev.h>

#define MAX_LENGTH 64

/**
 * @brief  Converts Character to Hexadecimal Character
 * @param  value: character
 * @retval uint8_t
 */
uint8_t char2hex(char value)
{
  uint8_t retval;

  if (value >= '0' && value <= '9')
  {
    retval = value - '0';
  }
  else if (value >= 'A' && value <= 'F')
  {
    retval = value - 'A' + 0xA;
  }
  else if (value >= 'a' && value <= 'f')
  {
    retval = value - 'a' + 0xA;
  }
  else
  {
    retval = 0xF;
  }

  return retval;
}

/**
 * @brief  Convert a string into a hex string
 * @param  *str:
 * @param  *hex:
 * @param  length:
 * @retval None
 */
void string2hex(char *str, uint8_t *hex, size_t length)
{
  uint8_t nibble;

  for (size_t ii = 0; ii < (length * 2); ii++)
  {
    if (ii < strlen(str))
    {
      nibble = char2hex(str[ii]);
    }
    else
    {
      nibble = 0xf;
    }
    if (ii % 2 == 0)
    {
      hex[ii / 2] = nibble;
    }
    else
    {
      hex[ii / 2] = (hex[ii / 2] << 4) | nibble;
    }
  }
}

void print_spi_transaction(uint8_t *miso, uint8_t *mosi, size_t length)
{
  printf("MOSI  MISO\n");
  for (size_t ii = 0; ii < length; ii++)
  {
    printf("%.2X  : %.2X\n", mosi[ii], miso[ii]);
  }
}

void print_usage(void)
{
  printf("USAGE: spi_test -d dev -l len -m mosi -s speed\n"
         "      -d,--device dev: name of the spi device node\n"
         "      -l,--length len: length of spi transaction(bytes)\n"
         "      -m,--mosi mosi: hex value to be transmitted\n"
         "      -s,--speed speed: speed of the transaction in Hz\n\n"
         "EX: spi_test -d /dev/spidev1.0 -l 4 -m 12AB\n\n"
         "Note: mosi will be padded or truncated\n"
         "      to the length speficied.\n"
         "      %d bytes maximum length.\n", MAX_LENGTH);
}


int main(int argc, char *argv[])
{
  uint8_t miso[MAX_LENGTH];
  uint8_t mosi[MAX_LENGTH];
  struct spi_ioc_transfer tr = {
    .tx_buf = (unsigned long)mosi,
    .rx_buf = (unsigned long)miso,
    .len = (__u32)1,
    .delay_usecs = 1,
  };
  char *device_name = NULL;
  char *mosi_str = NULL;
  int opt_i = 0;
  int c;
  int fd;
  int ret;

  static struct option long_opts[] = {
    { "device", required_argument, 0, 'd' },
    { "length", required_argument, 0, 'l' },
    { "mosi", required_argument, 0, 'm' },
    { "speed", required_argument, 0, 's' },
    { "help", no_argument, 0, '?' },
    { 0, 0, 0, 0 },
  };

  while ((c = getopt_long(argc, argv, "d:l:m:s:?",
        long_opts, &opt_i)) != -1) {
    switch (c) {
    case 'd':
      device_name = optarg;
      break;
    case 'l':
      tr.len = MIN(atoi(optarg), MAX_LENGTH);
      break;
    case 'm':
      mosi_str = optarg;
      break;
    case 's':
      tr.speed_hz = atoi(optarg);
      break;
    case '?':
      print_usage();
      return 0;
    }
  }

  if (!device_name) {
    fprintf(stderr, "Missing required device argument.\n");
    print_usage();
    return -1;
  }

  if (mosi_str == NULL)
  {
    fprintf(stderr, "Missing MOSI string arguement.\n");
    print_usage();
    return -1;
  }

  fd = open(device_name, O_RDWR);
  if (fd == -1) {
    fprintf(stderr, "main: opening device file: %s: %s\n",
           device_name, strerror(errno));
    return -1;
  }

  string2hex(mosi_str, mosi, tr.len);

  printf("Sending to %s at %u Hz\n", device_name, tr.speed_hz);

  ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
  if (ret == -1)
    fprintf(stderr, "main: ioctl SPI_IOC_MESSAGE: %s: %s\n",
      device_name, strerror(errno));
  else
    print_spi_transaction(miso, mosi, tr.len);

  close(fd);

  return ret;
}
