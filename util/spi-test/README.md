# SPI Test Utility

Access SPI interface through the commandline

## Usage

```
"USAGE: spi_test -d dev -l len -m mosi -s speed
                 -d,--device dev: name of the spi device node
                 -l,--length len: length of spi transaction (bytes)
                 -m,--mosi mosi: hex value to be transmitted
                 -s,--speed speed: speed of the transaction in Hz
          EX: spi_test -d /dev/spidev1.0 -l 4 -m 12AB
          Note: mosi will be padded or truncated
                to the length speficied.
                bytes maximum length
```