# @file   pentek5950_readback_pll.sh
# @brief  Readback registers from the pll
#
# @author Nicholas Molinski
# @date   06-01-2021

echo "=== REG : 112 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m F00000
echo "=== REG : 111 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m EF0000
echo "=== REG : 110 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m EE0000
echo "=== REG : 109 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m ED0000
echo "=== REG : 108 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m EC0000
echo "=== REG : 107 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m EB0000
echo "=== REG : 106 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m EA0000
echo "=== REG : 105 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E90000
echo "=== REG : 104 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E80000
echo "=== REG : 103 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E70000
echo "=== REG : 102 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E60000
echo "=== REG : 101 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E50000
echo "=== REG : 100 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E40000
echo "=== REG : 99 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E30000
echo "=== REG : 98 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E20000
echo "=== REG : 97 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E10000
echo "=== REG : 96 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m E00000
echo "=== REG : 95 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m DF0000
echo "=== REG : 94 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m DE0000
echo "=== REG : 93 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m DD0000
echo "=== REG : 92 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m DC0000
echo "=== REG : 91 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m DB0000
echo "=== REG : 90 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m DA0000
echo "=== REG : 89 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D90000
echo "=== REG : 88 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D80000
echo "=== REG : 87 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D70000
echo "=== REG : 86 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D60000
echo "=== REG : 85 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D50000
echo "=== REG : 84 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D40000
echo "=== REG : 83 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D30000
echo "=== REG : 82 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D20000
echo "=== REG : 81 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D10000
echo "=== REG : 80 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m D00000
echo "=== REG : 79 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m CF0000
echo "=== REG : 78 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m CE0000
echo "=== REG : 77 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m CD0000
echo "=== REG : 76 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m CC0000
echo "=== REG : 75 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m CB0000
echo "=== REG : 74 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m CA0000
echo "=== REG : 73 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C90000
echo "=== REG : 72 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C80000
echo "=== REG : 71 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C70000
echo "=== REG : 70 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C60000
echo "=== REG : 69 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C50000
echo "=== REG : 68 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C40000
echo "=== REG : 67 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C30000
echo "=== REG : 66 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C20000
echo "=== REG : 65 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C10000
echo "=== REG : 64 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m C00000
echo "=== REG : 63 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m BF0000
echo "=== REG : 62 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m BE0000
echo "=== REG : 61 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m BD0000
echo "=== REG : 60 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m BC0000
echo "=== REG : 59 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m BB0000
echo "=== REG : 58 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m BA0000
echo "=== REG : 57 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B90000
echo "=== REG : 56 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B80000
echo "=== REG : 55 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B70000
echo "=== REG : 54 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B60000
echo "=== REG : 53 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B50000
echo "=== REG : 52 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B40000
echo "=== REG : 51 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B30000
echo "=== REG : 50 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B20000
echo "=== REG : 49 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B10000
echo "=== REG : 48 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m B00000
echo "=== REG : 47 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m AF0000
echo "=== REG : 46 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m AE0000
echo "=== REG : 45 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m AD0000
echo "=== REG : 44 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m AC0000
echo "=== REG : 43 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m AB0000
echo "=== REG : 42 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m AA0000
echo "=== REG : 41 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A90000
echo "=== REG : 40 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A80000
echo "=== REG : 39 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A70000
echo "=== REG : 38 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A60000
echo "=== REG : 37 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A50000
echo "=== REG : 36 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A40000
echo "=== REG : 35 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A30000
echo "=== REG : 34 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A20000
echo "=== REG : 33 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A10000
echo "=== REG : 32 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m A00000
echo "=== REG : 31 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 9F0000
echo "=== REG : 30 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 9E0000
echo "=== REG : 29 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 9D0000
echo "=== REG : 28 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 9C0000
echo "=== REG : 27 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 9B0000
echo "=== REG : 26 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 9A0000
echo "=== REG : 25 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 990000
echo "=== REG : 24 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 980000
echo "=== REG : 23 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 970000
echo "=== REG : 22 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 960000
echo "=== REG : 21 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 950000
echo "=== REG : 20 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 940000
echo "=== REG : 19 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 930000
echo "=== REG : 18 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 920000
echo "=== REG : 17 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 910000
echo "=== REG : 16 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 900000
echo "=== REG : 15 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 8F0000
echo "=== REG : 14 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 8E0000
echo "=== REG : 13 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 8D0000
echo "=== REG : 12 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 8C0000
echo "=== REG : 11 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 8B0000
echo "=== REG : 10 ==="
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 8A0000
echo "=== REG : 9 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 890000
echo "=== REG : 8 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 880000
echo "=== REG : 7 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 870000
echo "=== REG : 6 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 860000
echo "=== REG : 5 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 850000
echo "=== REG : 4 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 840000
echo "=== REG : 3 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 830000
echo "=== REG : 2 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 820000
echo "=== REG : 1 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 810000
echo "=== REG : 0 ==="
 ./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 800000

