# @file   pentek5950_tune_pll_4GHz.sh
# @brief  Tune pll
#
# @author Nicholas Molinski
# @date   06-01-2021

./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 000000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 000002
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 000000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 700000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 6F0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 6E0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 6D0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 6C0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 6B0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 6A0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 690021
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 680000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 670000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 663f80
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 650011
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 640000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 630000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 620200
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 610888
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 600000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 5F0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 5E0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 5D0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 5C0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 5B0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 5A0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 590000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 580000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 570000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 560000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 55d300
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 540001
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 530000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 521e00
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 510000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 506666
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4F0026
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4E00bf
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4D0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4C000c
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4B0800
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4A0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4906e4
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 48003e
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 470049
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 46c350
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 450000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4403e8
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 430000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 4201f4
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 410000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 401388
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 3F0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 3E0322
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 3D00a8
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 3C0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 3B0001
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 3A0001
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 390020
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 380000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 370000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 360000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 350000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 340820
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 330080
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 320000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 314180
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 300300
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 2F0300
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 2E07fe
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 2Dc0df
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 2C1f20
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 2B0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 2A0000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 290000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 280000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 27ffff
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 26ffff
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 250104
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 2407d0
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 230004
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 220000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 211e21
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 200393
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 1F43ec
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 1E318c
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 1D318c
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 1C0488
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 1B0002
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 1A0db0
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 190624
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 18071a
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 17007c
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 160001
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 150401
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 148848
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 1327b7
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 120064
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 110110
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 100080
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 0F064f
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 0E1e70
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 0D4000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 0C5002
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 0B0058
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 0A10d8
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 090604
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 082000
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 0740b2
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 06c802
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 0500c8
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 040a43
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 030642
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 020500
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 010808
./spi-test -d /dev/spidev1.0 -s 500000 -l 3 -m 00641c
