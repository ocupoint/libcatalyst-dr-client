# Catalyst Digital Receiver Client API

libcatalyst-dr-client provides client functionality for production control or evaulation control of Catalyst Digital receivers.

## Installation

TBD

## Usage

TBD

## List of supported receivers

|   Name   | Version |
|----------|---------|
| Osiris   | 0.0.1+  |
| Yosemite | 0.0.1+  |


