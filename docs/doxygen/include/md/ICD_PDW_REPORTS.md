# Component PDW Reports

## ICD

| Address | Width        | Name                     | LSB          |
|---------|--------------|--------------------------|--------------|
| 0x0000  | 31 downto 0  | TOA MSB                  | 1 ps         |
| 0x0004  | 31 downto 0  | TOA LSB                  | 1 ps         |
| 0x0008  | 31 downto 0  | Avg. Frequency           | 1 KHz        |
| 0x000C  | 31 downto 0  | Min. Frequency           | 1 KHz        |
| 0x0010  | 31 downto 0  | Max. Frequency           | 1 KHz        |
| 0x0014  | 31 downto 0  | Passband Center          | 1 KHz        |
| 0x0018  | 31 downto 0  | Pasband Width            | 1 KHz        |
| 0x001C  | 31 downto 0  | Pulse Width              | 1 ns         |
| 0x0020  | 31 downto 0  | SNR                      | 0.1dB        |
| 0x0020  | 31 downto 16 | Avg. Amplitude           | 0.1dB        |
| 0x0024  | 31 downto 0  | Min. Amplitude           | 0.1dB        |
| 0x0024  | 31 downto 0  | Max. Amplitude           | 0.1dB        |
| 0x0028  | 31 downto 0  | AOA                      | 0.1 degree   |
| 0x0028  | 31 downto 0  | Platform Heading         | 0.1 degree   |
| 0x002C  | 31 downto 0  | Platform Pitch           | 0.1 degree   |
| 0x002C  | 31 downto 0  | Platform Roll            | 0.1 degree   |
| 0x0030  | 31 downto 0  | Alt. Polarization Offset | 0.1 degree   |
| 0x0030  | 31 downto 0  | Parameters               | N/A          |
| 0x0034  | 31 downto 0  | Flag Bits                | N/A          |
| 0x0034  | 15 downto 8  | Angle Quality            | N/A          |
| 0x0034  |  7 downto 0  | PDW Source               | N/A          |


## Example Code

```c

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_types.h"
#include "xil_io.h"


#define BA_PDW_GENERATOR 0xA0000000
#define BA_PDW_MEMORY    0x40000000
#define BA_NUM_PDWS      1

int main()
{
    init_platform();

    print("Hello fake_pdw_generator_bd_wrapper\n\r");

    u32 drawing_number 		= Xil_In32(BA_PDW_GENERATOR);
    u32 version_revision    = Xil_In32(BA_PDW_GENERATOR + 4);
    u32 echo 				= Xil_In32(BA_PDW_GENERATOR + 8);

    printf("Fake PDW Generator Application:\n\tDrawing Number: %x\n\tVersion Revision: %x\n\tEcho: %x\n", drawing_number, version_revision, echo);

    Xil_Out32(BA_PDW_GENERATOR + 0x60, 0x1234); // Start Frequency
    Xil_Out32(BA_PDW_GENERATOR + 0x68, 0x4321); // Stop Frequency

    Xil_Out32(BA_PDW_GENERATOR + 0x78, 0x1234); // Pulse Width
    Xil_Out32(BA_PDW_GENERATOR + 0x7C, 0x4321); // PRIs


    Xil_Out32(BA_PDW_GENERATOR + 0x6C, 0x1234); // Start Amplitude
    Xil_Out32(BA_PDW_GENERATOR + 0x74, 0x4321); // Stop Amplitude


    u32 start_freq = Xil_In32(BA_PDW_GENERATOR + 0x60); // Start Frequency
    u32 stop_freq =  Xil_In32(BA_PDW_GENERATOR + 0x68); // Stop Frequency

    u32 pw  = Xil_In32(BA_PDW_GENERATOR + 0x78); // Pulse Width
    u32 pri = Xil_In32(BA_PDW_GENERATOR + 0x7C); // PRIs


    u32 start_amp = Xil_In32(BA_PDW_GENERATOR + 0x6C); // Start Amplitude
    u32 stop_amp =  Xil_In32(BA_PDW_GENERATOR + 0x74); // Stop Amplitude

    printf("\tPDW Parameters:\n\tStart Frequency: %x\n\tStop Frequency:  %x\n\tPulse Width: %x\n\tPRI: %x\n\tStart Amplitude: %x\n\tStop Amplitude: %x\n", start_freq, stop_freq, pw, pri, start_amp, stop_amp);

    for(uint32_t ii = 0; ii < BA_NUM_PDWS*30; ii+= 4)
    {
      printf("%5x: %x\n", ii, *((u32*)BA_PDW_MEMORY + ii));
    }

    cleanup_platform();

    return 0;
}

```