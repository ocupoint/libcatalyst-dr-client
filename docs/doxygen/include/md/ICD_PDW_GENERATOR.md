# Component PDW GENERATOR V0

## ICD

| Address | Width       | Name                    |
|---------|-------------|-------------------------|
| 0x0000  | 31 downto 0 | Drawing Number          |
| 0x0004  | 31 downto 0 | Version Revision        |
| 0x0008  | 31 downto 0 | Echo                    |
| 0x000C  | 31 downto 0 | Header Info             |
| 0x0010  | 31 downto 0 | Bus Addr Width          |
| 0x0014  | 31 downto 0 | Bus Data Width          |
| 0x0018  | 31 downto 0 | Component BAR           |
| 0x0060  | 31 downto 0 | F1 Start Frequency      |
| 0x0064  | 31 downto 0 | F1 Avg Frequency        |
| 0x0068  | 31 downto 0 | F1 Stop Frequency       |
| 0x006C  | 31 downto 0 | F1 Amplitude Start      |
| 0x0070  | 31 downto 0 | F1 Amplitude Avg        |
| 0x0074  | 31 downto 0 | F1 Amplitude Stop       |
| 0x0078  | 31 downto 0 | F1 Pulse Width          |
| 0x007C  | 31 downto 0 | F1 PRI                  |
| 0x0080  | 31 downto 0 | F1 SNR                  |
| 0x0084  | 31 downto 0 | F1 AOA                  |
| 0x0088  | 31 downto 0 | F1 Heading              |
| 0x008C  | 31 downto 0 | F1 Pitch                |
| 0x0090  | 31 downto 0 | F1 Roll                 |
| 0x0094  | 31 downto 0 | F1 Polarity             |
| 0x0098  | 31 downto 0 | F1 Angle Quality        |
| 0x009C  | 31 downto 0 | F1 Source               |
| 0x00A0  | 31 downto 0 | F1 Passband Center      |
| 0x00A4  | 31 downto 0 | F1 Passband Width       |

## Example Code

```c

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_types.h"
#include "xil_io.h"


#define BA_PDW_GENERATOR 0xA0000000
#define BA_PDW_MEMORY    0x40000000
#define BA_NUM_PDWS      1

int main()
{
    init_platform();

    print("Hello fake_pdw_generator_bd_wrapper\n\r");

    u32 drawing_number 		= Xil_In32(BA_PDW_GENERATOR);
    u32 version_revision    = Xil_In32(BA_PDW_GENERATOR + 4);
    u32 echo 				= Xil_In32(BA_PDW_GENERATOR + 8);

    printf("Fake PDW Generator Application:\n\tDrawing Number: %x\n\tVersion Revision: %x\n\tEcho: %x\n", drawing_number, version_revision, echo);

    Xil_Out32(BA_PDW_GENERATOR + 0x60, 0x1234); // Start Frequency
    Xil_Out32(BA_PDW_GENERATOR + 0x68, 0x4321); // Stop Frequency

    Xil_Out32(BA_PDW_GENERATOR + 0x78, 0x1234); // Pulse Width
    Xil_Out32(BA_PDW_GENERATOR + 0x7C, 0x4321); // PRIs


    Xil_Out32(BA_PDW_GENERATOR + 0x6C, 0x1234); // Start Amplitude
    Xil_Out32(BA_PDW_GENERATOR + 0x74, 0x4321); // Stop Amplitude


    u32 start_freq = Xil_In32(BA_PDW_GENERATOR + 0x60); // Start Frequency
    u32 stop_freq =  Xil_In32(BA_PDW_GENERATOR + 0x68); // Stop Frequency

    u32 pw  = Xil_In32(BA_PDW_GENERATOR + 0x78); // Pulse Width
    u32 pri = Xil_In32(BA_PDW_GENERATOR + 0x7C); // PRIs


    u32 start_amp = Xil_In32(BA_PDW_GENERATOR + 0x6C); // Start Amplitude
    u32 stop_amp =  Xil_In32(BA_PDW_GENERATOR + 0x74); // Stop Amplitude

    printf("\tPDW Parameters:\n\tStart Frequency: %x\n\tStop Frequency:  %x\n\tPulse Width: %x\n\tPRI: %x\n\tStart Amplitude: %x\n\tStop Amplitude: %x\n", start_freq, stop_freq, pw, pri, start_amp, stop_amp);

    for(uint32_t ii = 0; ii < BA_NUM_PDWS*30; ii+= 4)
    {
      printf("%5x: %x\n", ii, *((u32*)BA_PDW_MEMORY + ii));
    }

    cleanup_platform();

    return 0;
}

```