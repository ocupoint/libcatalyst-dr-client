# Creating a message

Using messages, we can allow the transport of information between a Client and a Server. Using a network backbone, Clients may send messages to many servers, and servers may send messages amongst themselves.

For example, a message can be sent from an HMI to a Digital Receiver to adjust signal thresholding levels in the Aquisition subsystem. Conversely, PDW messages can be sent from the Digital Receiver to a PDW subscriber. A Client can have many roles, therefore messages allow for flexibility. In certain situations, clients may want to send messages to each other through a server.

![Figure 1. Message Transport](message_arch.png)

@image latex message_arch.png

## Message Transport

To transmit a message a we send a #CatalystDRMessageHeader followed by a buffer containing the raw data down the UDP transport layer. See the #CatalystDRMessageHeader for more information on the data format.

![Figure 2. Message Flow](message_flow.png)

@image latex message_flow.png
