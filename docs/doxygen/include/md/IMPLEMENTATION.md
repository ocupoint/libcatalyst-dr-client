# Client Interface Implementation

The client has access to the digital receiver through an interface available in the firmware. Messages are routed though to the PS (Processor) and make their way to the PL (Programmable Logic) if requested.

![Figure 1. Message Interface](message_interface.png)

@image latex message_interface.png

## Message Bursting

TBD

## Message Queueing

TBD
