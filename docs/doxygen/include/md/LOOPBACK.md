# Loopback Example

In this example we will use the Catalyst Digital Receiver to capture a generated RF signal.

![Figure 1. Loopback](./loopback.png)

@image latex loopback.png

## Equipment Needed

2 MMCX to SMA Cables (or MMCX to similar)

1 SMA Bullet (or similar)

1 RFSOC / RFPGA platform with a 4GSPS capable ADC and DAC

## Instructions

1. Ensure that the latest bitstream is flashed to the FPGA.

2. Using the configuration specified in your hardware, connect an MMCX connector to an ADC of a particular band. Connect the other cable to the DAC that matches the same band. Connect these two cables with a bullet.

3. Using the messaging interface, set the PDW Configuration to use Live RF PDWs.

4. Observe the printouts onscreen. You should observe a 25us PRI being reported at 3.10GHz. The amplitude will be reported as -30dBm