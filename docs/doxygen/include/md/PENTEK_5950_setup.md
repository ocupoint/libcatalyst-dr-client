# Pentek 5950 Hardware setup

@brief Catalyst Digital Receiver setup on a Pentek 5950

The Catalyst Digital Receiver Platform is features a prebuilt binary compatible with the Pentek 5950 Hardware platform. To get started testing control from `libcatalyst-dr-client`, the hardware must be properly installed and firmware image flashed.

![Figure 2. JTAG Cable Block Diagram](./5950_setup.png)

@image latex 5950_setup.png

## Hardware

Equipment needed

| Name                         | Quantity |
|------------------------------|----------|
| Pentek 5950 3U VPX card      |        1 |
| Pentek 5901 RTM              |        1 |
| Diligent JTAG HS2 Programmer |        1 |
| Micro JTAG to JTAG adapter   |        1 |
| 3U VPX 1 Slot chassis        |        1 |
| USB Micro Cable              |        1 |
| Cat 5+ Ethernet Cable        |        1 |

Software needed

| Name                 | Description            |
|----------------------|------------------------|
| Petalinux SDK        | Xilinx Petalinux SDK   |
| Putty (or Similar)   | Serial Terminal        |


*Note, while the Diligent JTAG HS3 Programmer claims to support the xzcu27 on the Pentek 5950, Digilent and Pentek recommend the HS-2 programmer.

![Figure 1. Pentek 5950](./PENTEK5950.png)

@image latex PENTEK5950.png

![Figure 2. JTAG Cable Block Diagram](./JTAG_Conn.png)

@image latex JTAG_Conn.png

## 5950 Hardware Setup

The following steps detail how to prepare the Pentek 5950 for loading a Catalyst Digital Receiver.

### Pentek 5950 Configuration

![Figure 3. 5950 Solder Side](./5950_PCB_SW.jpeg)

@image latex 5950_PCB_SW.jpeg

The Pentek 5950 hosts a set of DIP switches to configure the Boot mode of the unit. This guide will provide instructions for configuring the system in QSPI32 boot mode, therefore, verify the switches are set to the proper configuration. `ON    OFF    ON    ON = QSPI32`

SW2 − FPGA Configuration 1

| Switch    | Function                           | ON                            | OFF                                            |
|-----------|------------------------------------|-------------------------------|------------------------------------------------|
| SW2−1     | SD Write Protect                   | Write Enabled*                | Write Protected                                |
| SW2−2     | Carrier EEPROM Write Protect       | Write Enabled*                | Write Protected                                |
| SW2-3     | VPX JTAG Enable (VPX_JTAG_EN_N)    | Use RTM JTAG                  | Use Front Panel JTAG*                          |
| SW2-4     | VPX SMB Enable (VPX_SMB_EN_N)      | SMB Connects Out to Backplane | SMB Does Not Connect to Backplane*             |
| SW2-5     | PS GTR0 Clock Select (USB3 Port)   | On Board 100MHz*              | PS_GTR_CLKIN                                   |
| SW2-6     | PCIe Clock Select                  | Onboard 100MHz*               | VPX_REFCLK (if distributed PCI CLK is present) |
| SW2−7     | Boot Mode 0                        | See FPGA Configuration Select below. ||
| SW2−8     | Boot Mode 1                        | See FPGA Configuration Select below. ||
| SW2−9     | Boot Mode 2                        | See FPGA Configuration Select below. ||
| SW2−10    | Boot Mode 3                        | See FPGA Configuration Select below. ||

`* Factory default settings`

FPGA Configuration Select − Switches SW2−7:10

| SW2−7     | SW2−8     | SW2−9     | SW2−10     | Configuration Select (FLASH) |
|-----------|-----------|-----------|------------|------------------------------|
| ON        | ON        | ON        | ON         | PS Dedicated JTAG            |
| OFF       | ON        | ON        | ON         | QSPI 24-bit Addressing**     |
| ON        | OFF       | ON        | ON         | QSPI 32-bit Addressing*      |
| OFF       | OFF       | ON        | ON         | SD 0 (Not Supported)         |
| ON        | ON        | OFF       | ON         | NAND (Not Supported)         |
| OFF       | ON        | OFF       | ON         | SD 1 (Not Supported)         |
| ON        | OFF       | OFF       | ON         | eMMC (Not Supported)         |
| ON        | ON        | ON        | OFF        | PJTAG (Not Supported)        |
| OFF       | ON        | ON        | OFF        | PJTAG (Not Supported)        |
| ON        | OFF       | ON        | OFF        | eMMC_18 (Not Supported)      |
| OFF       | OFF       | ON        | OFF        | USB                          |
| ON        | ON        | OFF       | OFF        | PJTAG (Not Supported)        |
| OFF       | ON        | OFF       | OFF        | PJTAG (Not Supported)        |
| ON        | OFF       | OFF       | OFF        | SD1-LS (Not Supported)       |

`* Factory default setting`

** Note that, for this configuration, Uboot reset and Linux reset commands will not reset the board.

####

### Pentek 5950 Installation

#### Step 1.

To install the Pentek RFSOC card, locate a free slot in any standard 3U VPX compatible chassis slot.

![Figure 4. Conduction Cooled 3U VPX 1 slot chassis](./elma.jpg)

@image latex elma.jpg

#### Step 2.

If your 3U VPX chassis is conduction cooled, like pictured above (Option 763 on the Pentek 5950), it is required to remove the 5950 front panel to install the unit into the slot. This panel can be removed by removing the central mounting screw on the 5950 and the two parallel screws located behind the front panel.

![Figure 5. Remove Front Panel Screw](./front_panel.jpg)

@image latex front_panel.jpg

This should allow disassembly of the front panel from the 5950.

![Figure 6. Remove Screws Behind Front Panel](./front_panel_side_screws.jpg)

@image latex front_panel_side_screws.jpg

#### Step 3.

With the Front Panel separated, slide the 5950 in the slot with the locking clips out. When the clips reach the mechanical locking slot, swing the clips closed. This may take some force.

![Figure 7. Pentek Mechanical Clips](./clip_instructions.jpg)

@image latex clip_instructions.jpg

### JTAG / Debug Connection

To be able to access the debugging capabilities of the 5950, we will need to connect the JTAG interface. The JTAG interface allows programming the FPGA firmware via QSPI. Additionally, the JTAG interface exposes hardware debug capabilities.

#### Step 1.

Plugin one end of the 002.20014 cable into the 5960 mini JTAG connector

![Figure 8. Mini JTAG Plugin](./jtag_plugin.jpg)

@image latex jtag_plugin.jpg

#### Step 2.

Connect the other end to the JTAG breakout compatible with the Xilinx HS2 programmer.

![Figure 9. HS-2 Connection](./jtag_adapter.jpg)

@image latex jtag_adapter.jpg

#### Step 3.

With the JTAG setup, connect the micro-usb cable from HS-2 programmer to the debugging computer.

### 5901 RTM

If available, it is advisable to install the 5901 RTM into the rear of the 3U VPX RTM chassis slot. The RTM allows for faster firmware image loads, UART and networking capabilities from the ethernet port in the back.

#### Step 1:

Slide the RTM in to the backplane. Ensure that the mechanical detent latches onto the chassis as pictured below. Screw in the top mounted mounting screw.

![Figure 10. HS-2 Connection](./rtm.jpg)

@image latex rtm.jpg

#### Step 2:

Connect the RTM to a a DCHP server capable modem via a Cat 5 ethernet cable.

## 5950 Firmware Setup

We will discuss two ways to load the firmware onto the Pentek 5950

1. QSPI (Slow but realiable)

2. TFTP boot (Faster but more involved)

First we will configure our serial port so that we can validate our programming capabilities.

### Serial Port Setup

#### Step 1:

Make sure that the JTAG port is connected via a micro USB cable to the firmware image "host" programming computer.

#### Step 2:

Power on the 5950. A yellow LED should be blinking on the 5950.

#### Step 3:

Find out the COM/USB/Serial port that the Pentek is mounted on. On Windows this can be done with the "Device Manager".

On Linux simply run:

`dmesg | grep tty`

This searches for all terminal devices attached to stdin (i.e. the Pentek COM port). Note down the most recent tty session as this is most likely the Pentek 5950 card due to the recent power on.

#### Step 4:

Open up a serial terminal application. In this example, we will use Putty, however, XTerm or similar will also work. For linux user's whose accounts dont have USB / serial port privileges, you will either need to enact `sudo` shen opening your serial terminal application or add a udev rule. **If you do not do this step, it will appear as if no output appears in the terminal session**.

Input the settings below. Replace the serial port to be the value found in step 3.

![Figure 11. Serial Terminal Session](./putty.png)

@image latex putty.png

If you are using Putty, make sure you check "Close Window on Exit" to "Never" so that when the system restarts, we can capture the UBoot log.

#### Step 5:

Connect to the device.

### QSPI Setup

#### Step 1:

Ensure that the JTAG cable is plugged into the Pentek 5950 connector and computer from step [JTAG / Debug Connection](#JTAG-/-Debug-Connection). Power on the system.

#### Step 2:

Open the Xilinx SDK

[Option 1]: **Xilinx SDK GUI**

Run the Xilinx SDK Program

[Option 2]: **Xilinx SDK CLI**

Navigate to the location of your Xilinx SDK in your terminal of choice.

In Linux this would be at: `$XILINX_PATH/SDK/2018.3/bin/xsdk`
In Windows this would be at: `$XILINX_PATH/SDK/2018.3/bin/xsdk.exe`

Now run:

```
# Linux
xsdk -batch
# Windows
xsdk.exe -batch
```

#### Step 3:

With the Xilinx SDK open: either run the `Program Flash` command in the Xilinx tab or in the CLI run the following command

**If using the GUI**

Using the menu bar, open the Program Flash Window in the `Xilinx->Program Flash` dropdown. Use the settings provided in the image below to flash the 5950 device. Depedending on whether your connection is LOCAL or REMOTE, you will need to specify the hardware server location in the Connection field.

For example:

For local connections the host field will be set to 127.0.0.1, while for remote connections, the host should be set to the IP of your host hardware server.

![Figure 13. Program Flash](./program_flash.png)

@image latex program_flash.png

Once your settings have been validated, hit the `Program` button to program the flash. This should take 10-20 minutes.

**If using the CLI**

Ensure you have a running hardware server connection. This can be done through the autoconnect option in Vivado or in the CLI run `connect` and then tun `targets` make sure you have a connection.

Now program the flash by running.

```
exec program_flash -f <path to>/BOOT.BIN \
-offset 0 \
-flash_type qspi-x8-dual_parallel \
-fsbl <path to>/zynqmp_fsbl.elf \
-cable type xilinx_tcf -url TCP:127.0.0.1:3121
```

This will program the flash of the fpga

### TFTP Setup

Booting from TFTP is the faster and preferred way of loading a firmware image.

#### Step 1:

Ensure that step [Serial Port Setup](#Serial-Port-Setup) has been completed. Now, toggle the power off and then back on to the Pentek 5950. Your serial session should now start displaying the UBoot loader.

![Figure 12. UBoot auto boot](./uboot.png)

@image latex uboot.png

Watch as UBoot finishes loading. You should see that a DHCP request will be sent out from the unit and an ip address will be assigned. Record this address. Watch as UBoot completes and a login prompt should appear.

![Figure 13. Discover](./discover.png)

@image latex discover.png

#### Step 2:

Now restart the 5950 again, this time stop autoboot by pressing any key before the 3 second timer is up.

#### Step 3:

Now with autoboot stopped, the terminal should be displaying the bootloader. In this session, we are going to run the following commands.

```
setenv ipaddr <5950 IP address assigned in step 1>
setenv serverip <The IP address of the TFTP server holding the boot image>
saveenv
```

#### Step 4:

With the environment variables configured, run:

`tftpboot 0x10000000 BOOT.BIN`

Where BOOT.BIN is the firmware image located at the top level of your tftp server's shared folder.

If this succeed, the Pentek has successfully loaded a new firmware image over TFTP.

To boot and run the firmware/software image, run:

`run install_boot`

To program the firmware image to memory.

#### Step 5:

Reboot the system and the new firmware image will load and execute on startup.

#### Step 6:

With the system powered on, connect thr MMCX connectors to the appropriate band shown below.

![Figure 14. 5950 Bands](./5950_bands.png)

@image latex 5950_bands.png

With the RF Cables plugged in the 5950 is now setup. The aquisition subsystem will be enabled by default and will be listening for RF signal. Feel free to run through the loopback example, or the live capture.

[Loopback]()