# Author: Nicholas Molinski
# Date: 10-18-2019
#
# C Makefile
#

# Include shared environment variables
include .env
export

# Makefile constraints
EXE       =libcatalyst-dr-client
# CC        =aarch64-linux-gnu-gcc#  # arm-linux-gnueabihf-gcc # aarch64-none-elf-gcc
CC        =aarch64-linux-gnu-g++# aarch64-linux-gnu-g++# arm-linux-gnueabihf-gcc # arm-linux-gnueabihf-gcc # aarch64-none-elf-gcc
LIB_DIR   =./lib
INC_DIR   =./include
LIC_DIR   =./docs/license
# LIB_PATHS =-L./lib/libnavigator_1_14
# LIBS      =" "
ENV_VARS  =LD_RUN_PATH='$$ORIGIN/lib'
CFLAGS    = -O2 -fPIC                                                            \
					 -D'ENV_VERSION_REVISION=$(VERSION_REVISION)'                          \
					 -D'ENV_BUILD_TIMESTAMP="$(shell date)"'                               \
					 -D'ENV_OCPCATALYST_DEBUG=$(OCPCATALYST_DEBUG)'                        \
					 -D'ENV_EXT_SAMPLE_CLOCK=$(EXT_SAMPLE_CLOCK)'                          \
					 -D'ENV_OCPCATALYST_LOG_LEVEL_TYPE=$(OCPCATALYST_LOG_LEVEL_TYPE)'      \
					 -D'_POSIX_C_SOURCE=199309L'                                           \
					 -std=c99 -Wall -I$(INC_DIR) -I$(LIB_DIR) -lm
# -l:libnav.so -l:libnav5950.so -l:libnavapi.so -l:libnavdev.so -l:libnavip.so -l:libnavutl.so
OFLAGS    = -fPIC -Wall -fvisibility=hidden
SRC_DIR   =src
EX_DIR    =examples
BUILD_DIR =./dist
OBJ_DIR   =$(BUILD_DIR)/obj
BIN_DIR   =$(BUILD_DIR)/bin
PUB_DIR   =$(BUILD_DIR)/include

# recursive file search functions
rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

# Files
C_FILES   =$(call rwildcard, $(SRC_DIR), *.c)
OBJ_FILES =$(patsubst %.c, $(OBJ_DIR)/%.o, $(notdir $(C_FILES)))
LIB_FILES =$(call rwildcard, $(LIB_DIR), *.so)
HEADERS   =$(call rwildcard, ./include/, *.h) $(call rwildcard, $(SRC_DIR), *.h)

PUB_HEADERS   =$(call rwildcard, ./include/public, *.h)

# Declarations
.PHONY: default all clean test docs
.PRECIOUS: $(EXE) $(OBJ_FILES)

default: $(EXE)
all: default

# Build the Executable
$(EXE): $(OBJ_FILES)
	@echo Building Executables
	@mkdir -p $(BUILD_DIR)
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(BIN_DIR)/lib
	@mkdir -p $(BIN_DIR)/license
	@mkdir -p $(PUB_DIR)
	@mkdir -p $(PUB_DIR)/public
	@# Create the shared lib
	@(              \
		$(ENV_VARS)   \
		$(CC)         \
		$(CFLAGS)     \
		$(LIB_PATHS)  \
		-dynamiclib   \
		-undefined    \
		-shared       \
		-o $(BIN_DIR)/$(EXE).so \
		$(OBJ_FILES)  \
		$(LIBS)       \
	)
	@rm -f $(OBJ_DIR)/main.o
	@ar -rcs $(BIN_DIR)/$(EXE).a $(filter-out %/main.o,$(OBJ_FILES))
	@# Delete main.o so we can redistribute object files
	@# Copy the public headers over
	@rm -rf $(PUB_DIR)/public
	@cp -r $(INC_DIR)/public $(PUB_DIR)/public
	@#@cp -r $(LIB_FILES) $(BIN_DIR)/lib
	@#cp -r $(LIC_DIR) $(BIN_DIR)/
	@#cp -r $(EX_DIR) $(BIN_DIR)/
	@#cp ./LICENSE $(BIN_DIR)/license
	@echo Executable Size: `wc -c < $(BIN_DIR)/$(EXE).so` Bytes
	@echo Done

# Build Object files
$(OBJ_FILES): $(C_FILES) $(HEADERS)
	@mkdir -p $(BUILD_DIR)
	@mkdir -p $(OBJ_DIR)
	@(              \
		$(ENV_VARS)   \
		$(CC)         \
		$(CFLAGS)     \
		$(OFLAGS)     \
		$(LIB_PATHS)  \
		-o $@ 			  \
		-c $(filter %$(patsubst %.o,%.c,$(notdir $@)),$(C_FILES))  \
		$(LIBS)       \
	)

# View exported funcitons in the Shared library
view_exported:
	nm -D $(BIN_DIR)/$(EXE).so

# Cleans the build folder
clean:
	@echo Cleaning
	@(rm -rf $(OBJ_DIR)/*)
	@(rm -rf $(BIN_DIR)/*)

# To run a unit test
# TARGET=/unit/foo_test.c make test
test:
	@echo Running Test
	@mkdir -p $(BUILD_DIR)
	@mkdir -p $(BUILD_DIR)/test
	@mkdir -p $(BUILD_DIR)/test/lib
	@# Compile all source to an out file
	@# filter out the main.c entry point
	@(             \
		$(ENV_VARS)   \
		$(CC)       \
		$(CFLAGS)     \
		$(LIB_PATHS)  \
		-g3         \
		-o $(BUILD_DIR)/test/test.out \
		./$(TARGET)   \
		$(filter-out %/main.c,$(C_FILES)) \
		$(LIBS)       \
	)
	@(./$(BUILD_DIR)/test/test.out)

docs:
	@(doxygen  ./docs/doxygen/Doxyfile)