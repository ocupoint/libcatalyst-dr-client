# Catalyst Digital Receiver Client

## Directory Structure

```
/src          <-- Source Files
/dist
  | /bin      <-- Binaries, Libraries, etc
  | /include  <-- Public Headers
/lib          <-- 3rd party libraries
/tests        <-- Unit tests
  /
```


## Building

To build the library run the `make` command. By default this will build using the linux arm compiler. To build using a different compiler, change the `CC` environment variable.

```bash
# For linux arm C++ compiler
make
# or
CC=aarch64-linux-gnu-g++ make
# or
CC=aarch64-linux-gnu-gcc make
```

Two libraries will be generated a static library `libcatalyst-dr-client.a` and dynamically linked library `libcatalyst-dr-client.so`.

These files can be found in the `/dist/bin` folder