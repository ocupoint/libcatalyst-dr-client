//                        *** UNCLASSIFIED ***
//------------------------------------------------------------
//
// $Source: DrInterface_Producer.h $
// $Revision: 1.0 $
// $Date: 2021/02/23 09:31:04PST $
// $Author: Trinh, Ray (US Person) (rtrinh) $
//
// Copyright 2021 Harris Corp. as an unpublished work. All rights reserved. This
// computer software (including documentation) is proprietary to and a trade
// secret of Harris Corp. and shall not be reproduced, disclosed or used without
// prior written permission of Harris Corp.
//
// DISTRIBUTION STATEMENT:
// THIS DOCUMENT CONTAINS HARRIS PROPRIETARY DATA THAT IS PRIVATE AND PRIVILEGED.
// THIS DOCUMENT SHALL BE MAINTAINED IN CONFIDENCE AND SHALL NOT BE USED FOR
// MANUFACTURE, DISCLOSED, OR REPRODUCED IN WHOLE OR IN PART WITHOUT THE
// EXPRESSED WRITTEN CONSENT OF HARRIS.
//
// WARNING:  THIS INFORMATION IS SUBJECT TO THE CONTROLS OF THE INTERNATIONAL
// TRAFFIC IN ARMS REGULATIONS (ITAR) AND SHALL NOT BE PROVIDED TO NON-U.S.
// PERSONS OR TRANSFERRED BY ANY MEANS TO ANY LOCATION OUTSIDE THE UNITED STATES
// WITHOUT ADVANCED WRITTEN PERMISSION FROM THE U.S. DEPARTMENT OF STATE.
//
// CONTAINS PROPRIETARY DATA.  U.S. GOVERNMENT USERS OF TECHNICAL DATA CONTAINED
// HEREIN RECEIVE NO GREATER THAN LIMITED/RESTRICTED RIGHTS PURSUANT TO
// FAR 52.227-14, DFARS 252.227-7013 AND/OR 252.227-7014.  ANY REPRODUCTION OF
// TECHNICAL DATA OR PORTIONS THEREOF MARKED WITH THIS LEGEND MUST ALSO REPRODUCE
// THE MARKINGS.  ANY PERSON, OTHER THAN THE U.S. GOVERNMENT, WHO HAS BEEN
// PROVIDED ACCESS TO SUCH DATA MUST PROMPTLY NOTIFY HARRIS ELECTRONIC SYSTEMS,
// 77 RIVER ROAD, CLIFTON, NEW JERSEY 07014.
//
//------------------------------------------------------------

#ifndef DRInterface_Producer_H
#define DRInterface_Producer_H
#ifdef __cplusplus
extern "C" {
#endif

extern void WritePDWs(void *msgBody);
extern void GetHardwareStatus(void *msgBody);

#ifdef __cplusplus
}
#endif

#endif

/********************************* < START OF FOOTER > *********************************
*
********************************* < END OF FOOTER > *********************************/
//                        *** UNCLASSIFIED ***

