/// @file   libcatalyst-dr-driver.h
/// @brief  Top level header for libcatalyst-dr-client definitions
///
/// @author Nicholas Molinski
/// @date   12-05-2020

#ifndef _OCPCDR_LIBCATALYS_DR_DRIVER_PUB_H_
#define _OCPCDR_LIBCATALYS_DR_DRIVER_PUB_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#include "public/ocpcatalystdrclient.h"


#include "public/handlers.h"


/**
 * @brief  Hello World function to test linking
 * @note
 * @retval None
 */
void HelloWorld_libcatalyst(void* msg);
void HelloWorld_Set_Narrowband_Tune(void* msg);
void HelloWorld_Set_Band_Conf(void* msg);
void HelloWorld_PDW_Rejects(void* msg);
void HelloWorld_Amp_Offset(void* msg);
void HelloWorld_SNR(void* msg);
void HelloWorld_Set_Narrowband_Step_Dwell(void* msg);
void HelloWorld_Set_Band_Status(void* msg);

#ifdef __cplusplus
}
#endif

#endif