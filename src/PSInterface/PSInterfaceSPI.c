/// @file   PSInterfaceSPI.c
/// @brief  SPI Interface on the PS Interface
///
/// @author Nicholas Molinski
/// @date   03-12-2021

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <dirent.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "public/errorcodes.h"
#include "private/helpers.h"
#include "private/debug.h"
#include "private/psinterface.h"
#include "private/event/eventhandleronerror.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

//@todo pass these as parameters
static uint32_t pll_mode = 0;
static uint8_t  pll_bits = 8;
// static uint16_t pll_delay = 1000;
static uint32_t pll_speed = 500000;

/**
 * @brief  Creates a SPI Interface struct
 * @retval PSInterfaceSPI *
 */
PSInterfaceSPI* PSInterfaceSPI_create()
{
  // Variables
  PSInterfaceSPI* psspi;

  psspi = (PSInterfaceSPI*)Heap_clear_allocate(sizeof(PSInterfaceSPI));

  if (psspi)
  {
    psspi->fd               = 0;
    psspi->sclk_speed_hz    = 500000; // 500K
    psspi->is_lsb_first     = 0;
    psspi->transaction_mode = 0;
    psspi->flags            = 0;
  }

  return psspi;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Opens a SPI interface that exists on the PS
 * @memberof  PSInterfaceSPI
 * @param     psspi: PSInterfaceSPI *
 * @param     filename[PSINTERFACE_SPI_FD_LENGTH]:
 * @retval    None
 */
void PSInterfaceSPI_open(PSInterfaceSPI* psspi, char filename[PSINTERFACE_SPI_FD_LENGTH])
{
  // Variabless
  uint32_t mode = 0;
  uint8_t bits = 8;
  char ioctl_erro[20];

  // Open ARM SPI descriptor
  // Good reference here: https://elixir.bootlin.com/linux/v3.9/source/Documentation/spi/spidev
  if ((psspi->fd = open(filename, O_RDWR)) < 0)
  {
    OCPCATALYST_LOG_ERRO("Failed to open SPI Interface: %s\r\n", filename);
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return ;
  }

  OCPCATALYST_LOG_DBUG("SPI Interface Opened: %d \r\n", psspi->fd );

  // SPI BIT
  // possible modes: mode |= SPI_LOOP; mode |= SPI_CPHA; mode |= SPI_CPOL; mode |= SPI_LSB_FIRST; mode |= SPI_CS_HIGH; mode |= SPI_3WIRE; mode |= SPI_NO_CS; mode |= SPI_READY;
  if (ioctl(psspi->fd, SPI_IOC_WR_MODE32, &pll_mode) < 0)
  {
    perror(ioctl_erro);
    OCPCATALYST_LOG_ERRO("Failed to intialize SPI Interface WRITE MODE: %s %s\r\n", filename, ioctl_erro);
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return;
  }

  if (ioctl(psspi->fd, SPI_IOC_RD_MODE32, &pll_mode) < 0)
  {
    perror(ioctl_erro);
    OCPCATALYST_LOG_ERRO("Failed to intialize SPI Interface READ MODE: %s %s\r\n", filename, ioctl_erro);
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return;
  }

  if (ioctl(psspi->fd, SPI_IOC_WR_BITS_PER_WORD, &pll_bits) < 0)
  {
    perror(ioctl_erro);
    OCPCATALYST_LOG_ERRO("Failed to intialize SPI Interface WR WORD SIZE: %s %s\r\n", filename, ioctl_erro);
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return;
  }

  if (ioctl(psspi->fd, SPI_IOC_RD_BITS_PER_WORD, &pll_bits) < 0)
  {
    perror(ioctl_erro);
    OCPCATALYST_LOG_ERRO("Failed to intialize SPI Interface RD WORD SIZE: %s %s\r\n", filename, ioctl_erro);
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return;
  }

  if (ioctl(psspi->fd, SPI_IOC_WR_MAX_SPEED_HZ, &pll_speed) < 0)
  {
    perror(ioctl_erro);
    OCPCATALYST_LOG_ERRO("Failed to intialize SPI Interface WR SCLK MAX SPEED: %s %s\r\n", filename, ioctl_erro);
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return;
  }

  if (ioctl(psspi->fd, SPI_IOC_RD_MAX_SPEED_HZ, &pll_speed) < 0)
  {
    perror(ioctl_erro);
    OCPCATALYST_LOG_ERRO("Failed to intialize SPI Interface RD SCLK MAX SPEED: %s\r\n", filename, ioctl_erro);
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return;
  }


  if (ioctl(psspi->fd, SPI_IOC_RD_LSB_FIRST, &psspi->is_lsb_first) < 0)
  {
    perror(ioctl_erro);
    OCPCATALYST_LOG_ERRO("Failed to intialize SPI Interface LSB MODE: %s %s\r\n", filename, ioctl_erro);
    return;
  }


  if (pll_mode & SPI_TX_QUAD)
  {
    OCPCATALYST_LOG_INFO("SPI_TX_QUAD\r\n");
  }
  else if (pll_mode & SPI_TX_DUAL)
  {
    OCPCATALYST_LOG_INFO("SPI_TX_DUAL\r\n");
  }
  if (pll_mode & SPI_RX_QUAD)
  {
    OCPCATALYST_LOG_INFO("SPI_RX_QUAD\r\n");
  }
  else if (pll_mode & SPI_RX_DUAL)
  {
    OCPCATALYST_LOG_INFO("SPI_RX_DUAL\r\n");
  }
  if (!(pll_mode & SPI_LOOP))
  {
    if (pll_mode & (SPI_TX_QUAD | SPI_TX_DUAL))
    {
      OCPCATALYST_LOG_INFO("SPI_LOOP DUAL\r\n");
    }
    else if (pll_mode & (SPI_RX_QUAD | SPI_RX_DUAL))
    {
      OCPCATALYST_LOG_INFO("SPI_LOOP QUAD\r\n");
    }
  }

  OCPCATALYST_LOG_INFO("PS Interface SPI initialized: %s [MODE:%d; BITS:%d; SPEED:%d]\r\n", filename, mode, bits, psspi->sclk_speed_hz);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Perform a SPI full-duplex transfer
 * @memberof  PSInterfaceSPI
 * @param     psspi: PSInterfaceSPI *
 * @param     buffer: uint8_t *
 * @param     bytes_per_word: uint8_t
 * @param     number_of_words: uint8_t
 * @param     verify_buffer: uint8_t *
 * @retval    None
 */
void PSInterfaceSPI_transfer(PSInterfaceSPI* psspi, uint8_t* buffer, uint8_t bytes_per_word, uint8_t number_of_words, uint8_t* verify_buffer)
{
  // Variables
  uint8_t* write_buffer;
  uint8_t* readback_buffer;
  size_t write_ptr;
  struct spi_ioc_transfer transaction;
  int ret;

  if (bytes_per_word == 0)
  {
    OCPCATALYST_LOG_ERRO("Zero byte transfer\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
  }

  OCPCATALYST_LOG_INFO("Performing SPI Transfer: %d bits, %d words\r\n", bytes_per_word, number_of_words);

  // Create Buffer for transactions
  write_buffer      = (uint8_t*)Heap_allocate(bytes_per_word*sizeof(uint8_t));
  readback_buffer   = (uint8_t*)Heap_allocate(bytes_per_word*sizeof(uint8_t));

  write_ptr = 0;
  memset(readback_buffer, 0, bytes_per_word);

  // Create the transaction
  transaction = (struct spi_ioc_transfer){
    .tx_buf          = (unsigned long)write_buffer,
    .rx_buf          = (unsigned long)readback_buffer,
    .len             = bytes_per_word,
    .speed_hz        = psspi->sclk_speed_hz,
    .delay_usecs     = 1
  };

  if (pll_mode & SPI_TX_QUAD)
  {
    transaction.tx_nbits = 4;
  }
  else if (pll_mode & SPI_TX_DUAL)
  {
    transaction.tx_nbits = 2;
  }
  if (pll_mode & SPI_RX_QUAD)
  {
    transaction.rx_nbits = 4;
  }
  else if (pll_mode & SPI_RX_DUAL)
  {
    transaction.rx_nbits =   2;
  }
  if (!(pll_mode & SPI_LOOP)) {
    if (pll_mode & (SPI_TX_QUAD | SPI_TX_DUAL))
    {
      transaction.rx_buf = 0;
    }
    else if (pll_mode & (SPI_RX_QUAD | SPI_RX_DUAL))
    {
      transaction.tx_buf = 0;
    }
  }

  // Go through buffer and write each transaction
  for (size_t ii = 0; ii < number_of_words; ii++)
  {
    OCPCATALYST_LOG_DBUG("Word %d\r\n", ii);

    // Load write buffer
    for (size_t jj = 0; jj < bytes_per_word; jj++)
    {
      OCPCATALYST_LOG_DBUG("Adding (%d) byte %2x\r\n", write_ptr, buffer[write_ptr]);

      // global to local buffer
      write_buffer[jj] = buffer[write_ptr];

      // Increment the write pointer
      write_ptr++;
    }

    OCPCATALYST_LOG_DBUG("Write %d: %2x%2x%2x\r\n", ii, write_buffer[0], write_buffer[1], write_buffer[2]);

    // Write the buffer
    ret = ioctl(psspi->fd, SPI_IOC_MESSAGE(1), &transaction);

    // Print readback
    OCPCATALYST_LOG_DBUG("Readback %d: %2x%2x%2x\r\n", ii, readback_buffer[0], readback_buffer[1], readback_buffer[2]);

    // Transaction
    if (ret < 1)
    {
      // char ioctl_erro[20];

      OCPCATALYST_LOG_ERRO("Failed to perform SPI Transaction: IOCTL CODE=%d\r\n", errno);
      EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    }

    // if (verify_buffer != NULL)
    // {
    //   OCPCATALYST_ASSERT(
    //     !((verify_buffer[write_ptr+1] == readback_buffer[1]) &&
    //     (verify_buffer[write_ptr+2] == readback_buffer[2])),
    //     "Buffer verification failed: Expected %2x%2x%2x, Got %2x%2x%2x; %d %d\r\n",
    //     verify_buffer[write_ptr+0],
    //     verify_buffer[write_ptr+1],
    //     verify_buffer[write_ptr+2],
    //     readback_buffer[0],
    //     readback_buffer[1],
    //     readback_buffer[2],
    //     (verify_buffer[write_ptr+1] == readback_buffer[1]),
    //     (verify_buffer[write_ptr+2] == readback_buffer[2])
    //   );

    //   OCPCATALYST_LOG_INFO("Verify Done: %d\r\n", ii);
    // }

    OCPCATALYST_LOG_DBUG("Transfer Done: %d\r\n", ii);
  }

  Heap_free(write_buffer);
  Heap_free(readback_buffer);

  OCPCATALYST_LOG_INFO("PS Interface SPI transaction complete\r\n");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Frees a SPI Interface
 * @memberof  PSInterfaceSPI
 * @param     psspi: PSInterfaceSPI *
 * @retval    None
 */
void PSInterfaceSPI_free(PSInterfaceSPI* psspi)
{
  close(psspi->fd);
  Heap_free(psspi);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
