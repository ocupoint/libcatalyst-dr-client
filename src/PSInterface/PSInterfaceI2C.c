/// @file   PSInterfaceI2C.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   03-11-2021


#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <dirent.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include "private/i2c.h"
#include "private/debug.h"
#include "private/psinterface.h"
#include "private/helpers.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a PS Interface to I2C
 * @memberof  PSInterfaceI2C
 * @retval    0=EXIT_SUCCESS; 1=EXIT_FAILURE
 */
PSInterfaceI2C* PSInterfaceI2C_create()
{
 // Variables
  PSInterfaceI2C* psi2c;

  psi2c = (PSInterfaceI2C*)Heap_allocate(sizeof(PSInterfaceI2C));

  if (psi2c)
  {
    psi2c->fd     = 0x0;
    psi2c->addr   = 0x0;
  }

  return psi2c;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Opens a I2C Device
 * @memberof  PSInterfaceI2C
 * @param     psi2c:
 * @param     i2c_bus:
 * @param     dev_addr:
 * @retval    uint8_t
 */
uint8_t PSInterfaceI2C_open(PSInterfaceI2C* psi2c, uint8_t i2c_bus, uint8_t dev_addr)
{
  // Variables
  char filename[20];

  snprintf(filename, 19, "/dev/i2c-%d", i2c_bus);

  psi2c->fd = open(filename, O_RDWR);

  if (psi2c->fd < 0)
  {
    OCPCATALYST_LOG_ERRO("Failed to open I2C file descriptor on bus %x\r\n", i2c_bus);
    PSInterfaceI2C_free(psi2c);

    return EXIT_FAILURE;
  }

  if (ioctl(psi2c->fd , I2C_SLAVE, dev_addr) < 0)
  {
    OCPCATALYST_LOG_ERRO("Failed to open I2C device 0x%x\r\n", dev_addr);
    PSInterfaceI2C_free(psi2c);

    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     I2C Write
 * @memberof  PSInterfaceI2C
 * @param     fd: I2C file descriptor
 * @param     command: i2c command
 * @param     buffer: Buffer to write
 * @param     num_bytes: transaction length
 * @retval    uint8_t
 */
uint8_t PSInterfaceI2C_write(PSInterfaceI2C* psi2c, uint8_t* buffer, uint8_t num_bytes)
{
  // Variables

  OCPCATALYST_LOG_INFO("I2C Write to 0x%x: %x ...\r\n", buffer[0], buffer[1]);

  if (write(psi2c->fd, buffer, num_bytes) != num_bytes) {
    /* ERROR HANDLING: I2C transaction failed */
    OCPCATALYST_LOG_ERRO("Failed to write I2C transaction\r\n");
    PSInterfaceI2C_free(psi2c);

    return EXIT_FAILURE;
  }

  OCPCATALYST_LOG_INFO("Write Success\r\n");

  // /* Using I2C Read, equivalent of i2c_smbus_read_byte(file) */
  // if (read(psi2c->fd, buffer, num_bytes) != num_bytes) {
  //   /* ERROR HANDLING: I2C transaction failed */
  //   OCPCATALYST_LOG_ERRO("Failed to read I2C transaction\r\n");
  //   PSInterfaceI2C_free(psi2c);

  //   return EXIT_FAILURE;
  // }

  // OCPCATALYST_LOG_INFO("I2C Readback to 0x%x: %x ...\r\n", buffer[0], buffer[1]);


  return EXIT_SUCCESS;
  // struct i2c_smbus_ioctl_data args;
  // unsigned char block[XIIC_BLOCK_MAX];

  // for (size_t ii = 1; ii <= length; ii++)
  // {
  //   block[ii] = buffer[ii-1];
  // }

  // block[0] = length;

  // args.read_write = I2C_SMBUS_WRITE;
  // args.command    = command;
  // args.size       = I2C_SMBUS_I2C_BLOCK;
  // args.data       = (union i2c_smbus_data *)&block;

  // ioctl(fd, I2C_SMBUS, &args);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Free an I2C Interface
 * @memberof  PSInterfaceI2C
 * @param     psi2c:
 * @retval    None
 */
void PSInterfaceI2C_free(PSInterfaceI2C* psi2c)
{
  close(psi2c->fd);
  Heap_free(psi2c);
}