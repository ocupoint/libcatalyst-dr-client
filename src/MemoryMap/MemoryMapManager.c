/// @file   MemoryMapManager.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include "private/ocp.h"
#include "private/helpers.h"
#include "private/debug.h"
#include "private/memorymap.h"
// POSIX libs
#if defined(__linux__) || defined(__APPLE__)
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Memory Map Manager allocates on the stack
 * @todo allow heap creation
 */
static MemoryMap* _static_manager_list = NULL;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Number of resource Managers that exist
 */
static uint32_t _static_resource_count = 0;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a new Memory Map Manager
 * @memberof  MemoryMapManager
 * @retval    MemoryMapManager*
 */
void MemoryMapManager_create()
{
  // Variables
  uintptr_t virtual_mem_ptr;
  int32_t devmem_fd;

  if ((MemoryMap*)_static_manager_list == NULL)
  {
    OCPCATALYST_LOG_DBUG("MemoryMapManager created.\r\n");

    _static_manager_list = (MemoryMap*)Heap_allocate(sizeof(MemoryMap)*__OCPCATALYST_MAX_MEMORY_MAP_COUNT);
    // Initialize List
    for (size_t ii = 0; ii < __OCPCATALYST_MAX_MEMORY_MAP_COUNT; ii++)
    {
      _static_manager_list[0].status = MEMORY_MAP_STATUS_FREE;
    }

    devmem_fd = open("/dev/mem", O_RDWR);

    // Will perform this operation to enable PL access
    // Enable I2C-0 and I2C-1 clocks to allow peripheral access
    virtual_mem_ptr = (uintptr_t)mmap((void*)0, 0x20, PROT_READ | PROT_WRITE, MAP_SHARED, devmem_fd, (off_t)0xF8000000);
    //*[19:18] = 2'b11, enable I2c0 and I2C1 clocks
    *((uint64_t *)virtual_mem_ptr + 0x12C/4) = *((uint64_t *)virtual_mem_ptr + 0x12C/4) | 0xC0000;
    // Cleanup
    munmap((void*)virtual_mem_ptr, 0x20);

    close(devmem_fd);
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Allocate a new memory map
 * @memberof  MemoryMapManager
 * @retval    MemoryMapManager*
 */
MemoryMap* MemoryMapManager_allocate(uint32_t* memory_offset, uint32_t size_bytes)
{
  // Variables
  MemoryMap* map;

  // If we have reached our maximum amount of resources, we will not allocate anymore
  if (_static_resource_count == __OCPCATALYST_MAX_MEMORY_MAP_COUNT)
  {
    OCPCATALYST_LOG_ERRO("Cannot allocate another Memory Map.")
    return NULL;
  }
  // Check if memory map already exists
  map = MemoryMapManager_find(memory_offset, size_bytes);

  if (map != (void*)0)
  {
    OCPCATALYST_LOG_DBUG("Found Existing Shared Memory Map\r\n");

    return map;
  }
  // Doesnt exist going to see if a free one exists

  for (uint32_t ii = 0; ii < __OCPCATALYST_MAX_MEMORY_MAP_COUNT; ii++)
  {
    if (_static_manager_list[ii].status == MEMORY_MAP_STATUS_FREE)
    {
      // Found a free list, allocate it
      OCPCATALYST_LOG_DBUG("Found Free Memory Map\r\n");

      return _static_manager_list + ii*sizeof(MemoryMap);
    }

  }

  // Could not allocate a new one
  return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Find an existing memory map
 * @memberof  MemoryMapManager
 * @retval    MemoryMapManager*
 */
MemoryMap* MemoryMapManager_find(uint32_t* memory_offset, uint32_t size_bytes)
{
  for (uint32_t ii = 0; ii < __OCPCATALYST_MAX_MEMORY_MAP_COUNT; ii++)
  {
    if (
      (_static_manager_list[ii].status != MEMORY_MAP_STATUS_FREE) &
      (_static_manager_list[ii].resource_physical_memory_addr == (uintptr_t)memory_offset) &
      (_static_manager_list[ii].resource_memory_size_bytes != size_bytes)
    )
    {
      OCPCATALYST_LOG_DBUG("Found Existing Memory Map\r\n");
      return _static_manager_list + ii*sizeof(MemoryMap);
    }

  }

//  OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_WARN, "Cannot find Memory Map. Make sure the offset and size is correct")

  // Does not exist
  return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
