/// @file   MemoryMap.c
/// @brief  Handles Memory Allocation and Consumption
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include "public/errorcodes.h"
#include "private/debug.h"
#include "private/ocp.h"
#include "private/helpers.h"
#include "private/debug.h"
#include "private/memorymap.h"
#include "private/event/event.h"
// POSIX libs
#if defined(__linux__) || defined(__APPLE__)
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a Memory Map Instance
 * @memberof  MemoryMap
 * @param     memory_offset: Offset in the Physical Memory Space
 * @param     size_bytes: Size of the Physical Memory
 * @retval    MemoryMap*
 */
MemoryMap* MemoryMap_create(uint32_t* memory_offset, uint32_t size_bytes)
{
  // Variables
  MemoryMap*   memory_map;
  uint32_t     devmem_fd;
  uintptr_t    virtual_mem_ptr;

  // allocate new memory map
  // memory_map = MemoryMapManager_allocate(memory_offset, size_bytes);
  memory_map = (MemoryMap*)malloc(sizeof(MemoryMap));

  if (memory_map == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to create new Memory Map\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

    return NULL;
  }

#ifdef XENV_STANDALONE_H
  // Standalone environment has direct access to physical memory
  memory_map->resource_virtual_memory_address = (void*)memory_offset;
  memory_map->resource_physical_memory_addr   = (void*)memory_offset;
  memory_map->resource_memory_size_bytes      = size_bytes;

#else

  OCPCATALYST_LOG_DBUG("Mapping Physical to Virtual Memory Map\r\n");

  devmem_fd       = open("/dev/mem", O_RDWR);

  if (memory_map->status == MEMORY_MAP_STATUS_ALLOCATED)
  {
    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_WARN,
      "This Memory Map %x has already been created, returning...\r\n",
      memory_map->resource_physical_memory_addr
    );
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return memory_map;
  }


  // Now map memory offset
  virtual_mem_ptr = (uintptr_t)mmap(0, size_bytes, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_SHARED, devmem_fd, (off_t)memory_offset); //I2C#0

  close(devmem_fd);

  memory_map->resource_virtual_memory_address = (uintptr_t)virtual_mem_ptr;
  memory_map->resource_physical_memory_addr   = (uintptr_t)memory_offset;
  memory_map->resource_memory_size_bytes      = size_bytes;


#endif

  memory_map->status |= MEMORY_MAP_STATUS_ALLOCATED;

  OCPCATALYST_LOG(
    OCPCATALYST_LOG_TYPE_DBUG,
    "New Memory Map at Phys=%p Virt=%p Size=%ld\r\n",
    memory_map->resource_physical_memory_addr,
    memory_map->resource_virtual_memory_address,
    memory_map->resource_memory_size_bytes
  );

  return memory_map;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Returns a MemoryMap in string form
 * @memberof  MemoryMap
 * @param     map: MemoryMap *
 * @retval    char*
 */
char* MemoryMap_to_string(MemoryMap* map)
{
  if (
    (map != NULL) &&
    (map->resource_virtual_memory_address != 0) &&
    (map->resource_physical_memory_addr   != 0) &&
    (map->resource_memory_size_bytes      != 0)
  )
  {
    return String_format(
      "[Memory Map]: resource_physical_memory_addr=%p, resource_virtual_memory_address=%p, resource_memory_size_bytes=%p",
      map->resource_physical_memory_addr,
      map->resource_virtual_memory_address,
      map->resource_memory_size_bytes
    );
  }

  if (map != NULL)
  {
    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_WARN,
      "Failed to convert Memory Map to string: Virt=%p;Phys=%p;Size=%ld\r\n",
      map->resource_virtual_memory_address,
      map->resource_physical_memory_addr,
      map->resource_memory_size_bytes
    );
  }
  else
  {
    OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_WARN, "Failed to convert Memory Map to string\r\n");
  }

  EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

  return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Validates a MemoryMap
 * @memberof  MemoryMap
 * @param     map: MemoryMap *
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t MemoryMap_validate(MemoryMap* map)
{
  if (
    (map != NULL) &&
    (map->resource_virtual_memory_address != 0) &&
    (map->resource_physical_memory_addr   != 0) &&
    (map->resource_memory_size_bytes      != 0)
  )
  {
    return EXIT_SUCCESS;
  }

  if (map != NULL)
  {
    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_WARN,
      "Failed to validate Memory Map: Virt=%p;Phys=%p;Size=%ld\r\n",
      map->resource_virtual_memory_address,
      map->resource_physical_memory_addr,
      map->resource_memory_size_bytes
    );
  }
  else
  {
    OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_WARN, "Passed invalid Memory Map\r\n");
  }

  return EXIT_FAILURE;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @brief     Frees MemoryMap, unmapping virtual and physical memory
 * @todo      Add warnings to prevent double free
 * @memberof  MemoryMap
 * @param     map:
 * @retval    None
 */
void MemoryMap_free(MemoryMap* map)
{
  OCPCATALYST_LOG_DBUG("Unmapping Virtual and Physical Memmory\r\n");
  // Unmap memory
  if (munmap((void*)map->resource_physical_memory_addr, map->resource_memory_size_bytes) < 0)
  {
    OCPCATALYST_LOG_ERRO("Failed Unmapping Memory Map\r\n");
  }

  map->resource_virtual_memory_address = 0;
  map->resource_physical_memory_addr = 0;
  map->resource_memory_size_bytes = 0;
  map->status = MEMORY_MAP_STATUS_FREE;
}
