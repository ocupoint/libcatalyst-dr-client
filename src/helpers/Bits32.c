/// @file   Bits32.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-26-2021

#include <stdint.h>
#include "private/helpers.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Read a value from a 32 bit word given a mask and offset
 * @memberof  Bits32
 * @param     word: 32 bit word
 * @param     mask: 32 bit mask
 * @param     offset: offset within 32 bit word
 * @retval    uint32_t
 */
uint32_t Bits32_read(uint32_t word, uint32_t mask, uint8_t offset)
{
  return (word & mask) >> offset;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Writes a value into a given word provided a mask and offset
 * @memberof  Bits32
 * @param     word: 32 bit word
 * @param     mask: 32 bit mask
 * @param     offset: offset within 32 bit word
 * @param     value: 32 bit value
 * @retval    uint32_t
 */
uint32_t Bits32_write(uint32_t word, uint32_t mask, uint8_t offset, uint32_t value)
{
  return (word & ~mask) | (value << offset);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
