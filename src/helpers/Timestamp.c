/// @file   Timestamp.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-28-2021

#include <stdint.h>
#include <time.h>
#if defined(__linux__) || defined(__APPLE__)
#include <sys/time.h>
#endif
#include "private/helpers.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// Used to hold a delta time
static uint64_t static_initialization_time = 0;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Get a time stamp in with microsecond resolution
 * @memberof  Timestamp
 * @retval    uint64_t
 */
uint64_t Timestamp_create()
{
  // Time value
  struct timeval tv;
  // Get the timestamp
  gettimeofday(&tv, NULL);
  // Concat time
  return ((uint64_t)tv.tv_sec)*1000*1000 + (uint64_t)tv.tv_usec;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Initialize a timestamp to be used as a reference point
 * @memberof  Timestamp
 * @retval    None
 */
void Timestamp_initialize()
{
  static_initialization_time = Timestamp_create();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Get the timedelta from the initialization time
 * @memberof  Timestamp
 * @retval    uint64_t
 */
uint64_t Timestamp_timedelta()
{
  return Timestamp_create() - static_initialization_time;
}