/// @file   HelloWorld.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-19-2021

#include <stdio.h>
#include "public/libcatalyst-dr-driver.h"
#include "private/ocp.h"
#include "private/debug.h"
#include "private/memorymap.h"
#include "private/message/messagealias.h"
#include "private/message/messagebody.h"


__export__ void HelloWorld_libcatalyst(void* msg)
{
  OCPCATALYST_LOG_DBUG("Hello libcatalyst\r\n\r");
}


__export__ void HelloWorld_SNR(void* msg)
{
  OCPCATALYST_LOG_DBUG("Hello libcatalyst SNR\r\n\r");
}


__export__ void HelloWorld_Amp_Offset(void* msg)
{
  OCPCATALYST_LOG_DBUG("Hello libcatalyst Amplitude Offset\r\n\r");
}



__export__ void HelloWorld_PDW_Rejects(void* msg)
{
  OCPCATALYST_LOG_DBUG("Hello libcatalyst PDW Rejects\r\n\r");
}


__export__ void HelloWorld_Set_Band_Conf(void* msg)
{
  OCPCATALYST_LOG_DBUG("Hello libcatalyst Band Configuration\r\n\r");
}

__export__ void HelloWorld_Set_Band_Status(void* msg)
{
  OCPCATALYST_LOG_DBUG("Hello libcatalyst Band Status\r\n\r");
}

__export__ void HelloWorld_Set_Narrowband_Step_Dwell(void* msg)
{
  OCPCATALYST_LOG_DBUG("Hello libcatalyst Band Configuration\r\n\r");
}


__export__ void HelloWorld_Set_Narrowband_Tune(void* msg)
{
  OCPCATALYST_LOG_DBUG("Hello libcatalyst Band Configuration\r\n\r");
}
