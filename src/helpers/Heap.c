/// @file   Heap.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-26-2021

#include <stdio.h>
#include <stdlib.h>
#include "public/errorcodes.h"
#include "private/debug.h"
#include "private/event/event.h"

/**
 * @brief  Standard Allocator to the Heap
 * @param  size_bytes: Size of memory pointer points to
 * @retval void *
 */
void* Heap_allocate(size_t size_bytes)
{
  // Variables
  void* ptr;

  // Allocate memory
  ptr = (void*)malloc(size_bytes);

  if (ptr == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to allocate memory on Heap\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
  }

  return ptr;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Standard Clear Memory Allocator to the Heap
 * @param  size_bytes: Size of memory pointer points to
 * @retval void *
 */
void* Heap_clear_allocate(size_t size_bytes)
{
  // Variables
  void* ptr;

  // Allocate memory
  ptr = (void*)malloc(size_bytes);

  if (ptr == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to allocate memory on Heap\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
  }
  else
  {
    // Cast pointer to bytes and se to 0
    for (size_t ii = 0; ii < size_bytes; ii++)
    {
      *(((unsigned char *)ptr) + ii*(sizeof(unsigned char))) = 0;
    }
  }

  return ptr;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Prints out to Heap data as Base16 Hex Data
 * @param  ptr: pointer to the heap
 * @param  size_bytes: size of the heap int bytes
 * @retval None
 */
void Heap_to_hex_string(void* ptr, size_t size_bytes)
{
  // Not a null pointer and has a size
  if ((ptr != NULL) && size_bytes)
  {
    for (size_t ii = 0; ii < size_bytes; ii++)
    {
      printf("%2x", ((uint8_t*)ptr)[ii]);
    }
    printf("\r\n");
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Frees Memory from the Heap
 * @param  ptr: Pointer to free
 * @retval None
 */
void Heap_free(void* ptr)
{

  if (ptr == NULL)
  {
    OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_WARN, "Free of a null pointer\r\n");
  }

  free(ptr);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
