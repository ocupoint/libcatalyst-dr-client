/// @file   String.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   09-06-2020

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include "private/ocp.h"
#ifdef __cplusplus
#include <iostream>
#endif

#ifdef __cplusplus
extern "C" {
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Concatenates two strings
 * @param  a: string A
 * @param  b: string B
 * @retval
 */
char* String_concat(const char* a, const char* b)
{
  // Variables
  char* buf;
  uint32_t len;

  len = strlen(a) + strlen(b) + 1;

  // Allocate space
  buf = (char*)malloc(sizeof(char)*(len));

  // Load buffer with both data
  strcpy(buf, a);
  strcat(buf, b);

  // Null Terminate
  // buf[len-1] = '\0';

  return buf;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Returns a formatted string
 * @param  format:
 * @retval char*
 */
char* String_format(const char* format, ...)
{
  // Variables
  char*   v_str;
  uint32_t  v_len;
  va_list v_args;
  va_list v_args_copy;

  va_start(v_args, format);
  // Get the length of the final string
  va_copy(v_args_copy, v_args);
  v_len = vsnprintf(NULL, 0, format, v_args_copy) + 1;
  // Allocate formatted string
  v_str = (char*)malloc(v_len);

  // String format to string pointer
  vsprintf(v_str, format, v_args);

  // Clean strings
  va_end(v_args);
  va_end(v_args_copy);

  return v_str;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  String free
 * @param  str:
 */
void String_destroy(char* str)
{
  free(str);
}

#ifdef __cplusplus
}
#endif
