/// @file   libcatalyst_dr_driver_main.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "public/libcatalyst-dr-driver.h"
#include "private/debug.h"
#include "private/drcomponent/drcomponent.h"
// #include "private/drcomponent/drcontroller.h"
#include "private/drcomponent/pdwreportcontroller.h"
#include "private/drcomponent/yosemitepdwgenerator.h"
#include "private/drcomponent/dmaaxixilinx.h"
#include "private/drcomponent/rfdcxilinx.h"
#include "private/drcomponent/pdwbuffer.h"
#include "private/drcomponent/rfdc.h"
#include "private/message/messagebody.h"
#include "private/message/messagepdwreportcontroller.h"
#include "private/helpers.h"
#include "private/fpgainterface.h"
#include "private/main.h"
#include "private/memorymap.h"
#include "private/ocp.h"
#include "private/platform/xilinx/xilinxrfdc.h"
#include "private/platform/pentek/platformpentek5950.h"

#include <linux/kernel.h>            /* Kernel... */
#include <linux/module.h>            /* ...Modul stuff*/



#define DMA_SIZE 1024
#define DMA_ADDR 0x70000000
#define NUM_PDWS 64

// static DRComponent* drc_dma;
// static DRComponent* drc_pdwgen;
// static DRComponent* drc_pdwbuffer;
// static DRComponent* drc_pdwreporter;
// static DRComponent* drc_rfdc;

// void on_dma_intc()
// {
//   PDWBuffer_print(drc_pdwbuffer, NUM_PDWS);
// }


/**
 * @brief  Main Function to Initialize digital receiver
 * @note   This should be called only once
 * @retval 0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
__export__ int libcatalyst_dr_driver_main()
{
  // Variables
  // void* dma_alloc = malloc(sizeof(uint32_t)*DMA_SIZE);//(void *)DMA_ADDR;
  // void* dma_alloc = (void *)DMA_ADDR;

  // DRComponent* drc_rfdc;

  OCPCATALYST_LOG_INFO("=== Catalyst Digital Receiver Driver Version: %s ===\r\n", OCPCDR_VERSION);

  OCPCATALYST_LOG_INFO("Initializing...\r\n");

  // Start Initialization Timer
  Timestamp_initialize();

  // Initialize all Memory Memory Space
  MemoryMapManager_create();

  // Initialize the Pentek
  PlatformPentek5950_setup();

  OCPCATALYST_LOG_INFO("Initialization Success!\r\n");

  return EXIT_SUCCESS;
}
