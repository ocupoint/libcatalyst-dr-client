/// @file   MesagePDWReportController.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   03-01-2021

#include <pthread.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#include "private/ocp.h"
#include "private/helpers.h"
#include "private/debug.h"
#include "private/drcomponent/drcomponent.h"
#include "private/drcomponent/dmaaxixilinx.h"
#include "private/drcomponent/pdwbuffer.h"
#include "private/drcomponent/rfdcxilinx.h"
#include "private/drcomponent/pdwreportcontroller.h"
#include "private/drcomponent/yosemitepdwgenerator.h"
#include "private/drcomponent/pdwreports.h"
#include "private/message/messagebody.h"
#include "private/message/messagepdwreportcontroller.h"
#include "private/message/messagealias.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

static pthread_t static_pthread_sw_pdw_generation_id = 0;
static pthread_t static_pthread_poll_buffer_id = 0;
static pthread_t static_pthread_poll_buffer_2_id = 0;

static pthread_mutex_t static_sw_generation_mutex;
static pthread_mutex_t static_virt_buf_mutex;

// static pthread_mutex_t static_pdw_buffer_mutex;
// static pthread_mutex_t static_poll_buffer_mutex;

static sem_t static_sem_mutex;
static sem_t static_sem_mutex_2;

static uint8_t is_generating_sw_pdws = 0;
static uint8_t is_threading = 0;
// static uint8_t is_pdws_transfered = 0;

static DRComponent* drc_pdwbuffer;
static DRComponent* drc_pdwbuffer_2;
static DRComponent* drc_dma;
static DRComponent* drc_dma_band_2;

static DRComponent* drc_pdwgen;
static DRComponent* drc_pdwgen_band_2;
static DRComponent* drc_rfdc;
static uintptr_t virt_buf_addr;
static size_t static_num_pdws;

static char* message_buffer;
static uint32_t* pdw_buffer;
static uint32_t* pdw_buffer_2;
static uint32_t* pdw_buffer_ptr;
static uint32_t* pdw_buffer_ptr_2;
static size_t pdw_count = 0;

static uint32_t static_amplitude_offset_decidb = 1500;
static uint32_t static_platform_heading_decideg = 0;

static struct MessagePDWRejectsBody static_pdw_rejects[20] = {0};
static uint8_t static_num_pdw_rejects = 0;

// Band Configuration Message
static uint32_t static_band_1_center_rf_khz = 0;
static uint32_t static_band_2_center_rf_khz = 0;
static uint32_t static_band_1_ibw_khz = 0;
static uint32_t static_band_2_ibw_khz = 0;
static uint16_t static_band_1_attention_decidb = 0;
static uint16_t static_band_2_attention_decidb = 0;

#define YOSE_NUM_PDWS 64

#define DMA_ADDR        0x70000000
#define DMA_ADDR_BAND2  0x70010000U

#define PDWBUFFER_WRAP 0x40
#define PDW_INTERNAL_BUF_COUNT 128


extern void WritePDWs(void *msgBody);


// static void* __sw_pdw_generation(void* args);
// static void* __fw_pdw_generation(void* args);
// static void* __fake_gen(void* args);
static void* __message_yosemite_pdw_conf(void* args);

#define AMP_FACTOR 0.000001863

void* __poll_buffer(void* msg)
{
  while (1)
  {
    OCPCATALYST_LOG_DBUG("Waiting for Semaphore\r\n");

    sem_wait(&static_sem_mutex);

    OCPCATALYST_LOG_DBUG("Signal Recieved for Semaphore\r\n");

    const float tsFPGA = 4e-9;

    struct MessagePDWReportsBody* body = (struct MessagePDWReportsBody*)message_buffer;

    pthread_mutex_lock(&static_virt_buf_mutex);

    OCPCATALYST_LOG_INFO("Sending Write PDW: NUM_PDW %d Virtual Addr %p\r\n", static_num_pdws, virt_buf_addr);

    uint16_t num_pdws_processed = PDW_INTERNAL_BUF_COUNT;
    uint8_t is_allowing_pdw = 1;

    uint32_t band_center_mhz = (static_band_1_center_rf_khz/1000);
    uint32_t band_ibw_mhz    = (static_band_1_ibw_khz/1000);

    uint32_t band_start_mhz  = band_center_mhz - (band_ibw_mhz/2);


    for (size_t ii = 0; ii < PDW_INTERNAL_BUF_COUNT; ii++)
    {
      uint32_t toa_msb = (pdw_buffer[0  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      uint32_t toa_lsb = (pdw_buffer[1  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      uint32_t freq_av = (pdw_buffer[2  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      uint32_t pw      = (pdw_buffer[7  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      int16_t amp      = (pdw_buffer[8  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]) & 0xFFFF;
      uint32_t aoa     = (pdw_buffer[10 + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      uint32_t cw      = (pdw_buffer[13 + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]) >> 16;

      is_allowing_pdw = 1;

      // printf(
      //   "RAW PDW\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n",
      //   (pdw_buffer[0  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[1  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[2  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[3  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[4  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[5  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[6  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[7  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[8  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[9  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[10  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[11  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[12  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[13  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT])
      // );

      // uint64_t pw_ns   = (pw);
      // Divide by
      uint32_t freq_calc_mhz = (freq_av/1000) + band_start_mhz;
      uint32_t freq_calc = (freq_calc_mhz*1000);

      OCPCATALYST_LOG_INFO(
        "Adding to WritePDWs PD Band 1: %d; TOA: %x%x; FREQ AVG: %d; PW: %d; AMP: %d; AOA: %d; CW: %x;\r\n",
        ii,
        toa_lsb,
        toa_msb,
        freq_calc,
        pw,
        amp,
        aoa,
        cw
      );

      // PDW Rejects
      for (size_t ii = 0; ii < static_num_pdw_rejects; ii++)
      {
        // Check if this reject is disabled
        if (
          (static_pdw_rejects[ii].frequency_low_mhz != 0) &&
          ((static_pdw_rejects[ii].frequency_low_mhz <= freq_calc_mhz) && (freq_calc_mhz <= static_pdw_rejects[ii].frequency_high_mhz))
        )
        {
          OCPCATALYST_LOG_WARN("PDW DROPPED PDW Reject\r\n");
          is_allowing_pdw = 0;
          break;
        }
        else if (static_pdw_rejects[ii].frequency_low_mhz != 0)
        {
          OCPCATALYST_LOG_WARN("PDW NOT REJECTED FROM %d to %d: %d\r\n", static_pdw_rejects[ii].frequency_low_mhz, static_pdw_rejects[ii].frequency_high_mhz, freq_calc_mhz);
        }
        else
        {
          OCPCATALYST_LOG_DBUG("PDW ALLOWED AT: %d\r\n", freq_calc_mhz);
        }
      }

      if ((freq_av == 0) || (pw < 40) || (amp == 0) || (is_allowing_pdw == 0))
      {
        OCPCATALYST_LOG_WARN(
          "PDW dropped from 0MHz RF or <40ns PW or Reject\r\nFREQ_AVG: %d\r\nPW: %d\r\nAMP: %d\r\nPDWS_EN: %d\r\n",
          freq_av, pw, amp, is_allowing_pdw
        );
        num_pdws_processed--;
        continue;
      }

      // Readback first dma value
      uint32_t* pdwbuf = (uint32_t*)virt_buf_addr;

      // Math
      long double scale_factor =1000*pow(2, -29);
      long double amp_scaled   = ((long double)amp)*((long double)scale_factor);
      long double logged       = log10(amp_scaled);
      int16_t rounded      = round(10*logged);
      uint16_t amp_calc   = (uint16_t)(((int16_t)rounded*10)+static_amplitude_offset_decidb);

      // Print out to see sensitivity
      // if (cw == 2)
      // {

      //   printf("FREQ: %d, PW: %d RAW AMP: %d, CONV AMP: %d, OFFSET: %d, \r\n", freq_calc, pw, amp, amp_calc-static_amplitude_offset_decidb, static_amplitude_offset_decidb);
      // }

      pdwbuf[ 0  + (ii*14)]  = toa_msb;
      pdwbuf[ 1  + (ii*14)]  = toa_lsb;
      pdwbuf[ 2  + (ii*14)]  = freq_calc;
      pdwbuf[ 3  + (ii*14)]  = freq_calc;
      pdwbuf[ 4  + (ii*14)]  = freq_calc;
      pdwbuf[ 5  + (ii*14)]  = static_band_1_center_rf_khz;
      pdwbuf[ 6  + (ii*14)]  = static_band_1_ibw_khz;
      pdwbuf[ 7  + (ii*14)]  = pw; //
      pdwbuf[ 8  + (ii*14)]  = ((int32_t)amp_calc << 16 | amp_calc);
      pdwbuf[ 9  + (ii*14)]  = 0x00270000 | amp_calc;
      pdwbuf[ 10 + (ii*14)]  = (static_platform_heading_decideg << 16) | (aoa & 0xFFFF); // heading | aoa
      pdwbuf[ 11 + (ii*14)]  = 0x0;
      pdwbuf[ 12 + (ii*14)]  = 0x0;
      pdwbuf[ 13 + (ii*14)]  = 0x1C000000 | (cw); // Source, Flags
    }

    body->num_pdws                = num_pdws_processed;
    body->virtual_buffer_address  = (uint64_t)virt_buf_addr;

    // pthread_mutex_lock(&static_poll_buffer_mutex);
    // is_pdws_transfered = 0;
    // pthread_mutex_unlock(&static_poll_buffer_mutex);

    pdw_count++;

    OCPCATALYST_LOG_INFO("Writing %d PDWs\r\n", num_pdws_processed);

    // printf("WritePDWs Complete\r\n");

    if (num_pdws_processed > 0)
    {
      WritePDWs(body);
      OCPCATALYST_LOG_INFO("WritePDWs complete. PDW Buffer: %d\r\n", pdw_count);
    }

    pthread_mutex_unlock(&static_virt_buf_mutex);

  }

  return NULL;
}


void* __poll_buffer_2(void* msg)
{
  while (1)
  {
    OCPCATALYST_LOG_DBUG("Waiting for Semaphore\r\n");

    sem_wait(&static_sem_mutex_2);

    OCPCATALYST_LOG_DBUG("Signal Recieved for Semaphore\r\n");

    const float tsFPGA = 4e-9;

    struct MessagePDWReportsBody* body = (struct MessagePDWReportsBody*)message_buffer;

    pthread_mutex_lock(&static_virt_buf_mutex);

    OCPCATALYST_LOG_INFO("Sending Write PDW: NUM_PDW %d Virtual Addr %p\r\n", static_num_pdws, virt_buf_addr);

    uint16_t num_pdws_processed = PDW_INTERNAL_BUF_COUNT;
    uint8_t is_allowing_pdw = 1;

    uint32_t band_center_mhz = (static_band_2_center_rf_khz/1000);
    uint32_t band_ibw_mhz    = (static_band_2_ibw_khz/1000);

    uint32_t band_start_mhz  = band_center_mhz - (band_ibw_mhz/2);

    for (size_t ii = 0; ii < PDW_INTERNAL_BUF_COUNT; ii++)
    {
      // Ensure no negative PDWS
      if (num_pdws_processed == 0)
      {
        break;
      }

      uint32_t toa_msb = (pdw_buffer_2[0  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      uint32_t toa_lsb = (pdw_buffer_2[1  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      uint32_t freq_av = (pdw_buffer_2[2  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      uint32_t pw      = (pdw_buffer_2[7  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      int16_t amp      = (pdw_buffer_2[8  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]) & 0xFFFF;
      uint32_t aoa     = (pdw_buffer_2[10  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]);
      uint32_t cw      = (pdw_buffer_2[13 + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]) >> 16;

      is_allowing_pdw = 1;

      // printf(
      //   "RAW PDW\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n",
      //   (pdw_buffer[0  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[1  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[2  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[3  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[4  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[5  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[6  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[7  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[8  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[9  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[10  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[11  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[12  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT]),
      //   (pdw_buffer[13  + ii*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT])
      // );

      // uint64_t pw_ns   = (pw);
      uint32_t freq_calc_mhz = (freq_av/1000) + band_start_mhz;
      uint32_t freq_calc = (freq_calc_mhz*1000);

      OCPCATALYST_LOG_INFO(
        "Adding to WritePDWs PDW: %d; TOA: %x%x; FREQ AVG: %x; PW: %x; AMP: %x; AOA: %d; CW: %x;\r\n",
        ii,
        toa_lsb,
        toa_msb,
        freq_calc,
        pw,
        amp,
        aoa,
        cw
      );

      // PDW Rejects
      for (size_t ii = 0; ii < static_num_pdw_rejects; ii++)
      {
        // Check if this reject is disabled
        if (
          (static_pdw_rejects[ii].frequency_low_mhz != 0) &&
          ((static_pdw_rejects[ii].frequency_low_mhz <= freq_calc_mhz) && (freq_calc_mhz <= static_pdw_rejects[ii].frequency_high_mhz))
        )
        {
          OCPCATALYST_LOG_WARN("PDW dropped from PDW Reject\r\n");
          is_allowing_pdw = 0;
          break;
        }
        else if (static_pdw_rejects[ii].frequency_low_mhz != 0)
        {
          OCPCATALYST_LOG_WARN("PDW NOT REJECTED FROM %d to %d: %d\r\n", static_pdw_rejects[ii].frequency_low_mhz, static_pdw_rejects[ii].frequency_high_mhz, freq_calc_mhz);
        }
        else
        {
          OCPCATALYST_LOG_DBUG("PDW ALLOWED AT: %d\r\n", freq_calc_mhz);
        }
      }

      if ((freq_av == 0) || (pw < 40) || (amp == 0) || (is_allowing_pdw == 0))
      {
        OCPCATALYST_LOG_WARN("PDW dropped from 0MHz RF or <40ns PW or Reject\r\n");
        num_pdws_processed--;
        continue;
      }

      // Readback first dma value
      uint32_t* pdwbuf = (uint32_t*)virt_buf_addr;

      // Math
      long double scale_factor =1000*pow(2, -29);
      long double amp_scaled   = ((long double)amp)*((long double)scale_factor);
      long double logged       = log10(amp_scaled);
      int16_t rounded      = round(10*logged);
      uint16_t amp_calc   = (uint16_t)(((int16_t)rounded*10)+static_amplitude_offset_decidb);


      // int16_t amp_calc = amp;
      // printf("AMP: %d\r\n", amp_calc);
      // printf("AOA: %d\r\n", aoa);
      // Dangerous cast - but numbers are guarenteed below 3000
      // amp_calc += (int32_t)static_amplitude_offset_decidb;

      // @todo: found issue that pdw processing does not like toa in msb
      // pdwbuf[ 0  + (ii*14)]  = toa_msb;
      // pdwbuf[ 1  + (ii*14)]  = toa_lsb;
      // Temp fix
      pdwbuf[ 0  + (ii*14)]  = toa_msb;
      pdwbuf[ 1  + (ii*14)]  = toa_lsb;
      pdwbuf[ 2  + (ii*14)]  = freq_calc;
      pdwbuf[ 3  + (ii*14)]  = freq_calc;
      pdwbuf[ 4  + (ii*14)]  = freq_calc;
      pdwbuf[ 5  + (ii*14)]  = static_band_2_center_rf_khz;
      pdwbuf[ 6  + (ii*14)]  = static_band_2_ibw_khz;
      pdwbuf[ 7  + (ii*14)]  = pw; //
      pdwbuf[ 8  + (ii*14)]  = ((int32_t)amp_calc << 16 | amp_calc);
      pdwbuf[ 9  + (ii*14)]  = 0x00270000 | amp_calc;
      pdwbuf[ 10 + (ii*14)]  = (static_platform_heading_decideg << 16) | (aoa & 0xFFFF); // heading | aoa
      pdwbuf[ 11 + (ii*14)]  = 0x0;
      pdwbuf[ 12 + (ii*14)]  = 0x0;
      pdwbuf[ 13 + (ii*14)]  = 0x1B000000 | (cw); // Source, Flags
    }

    OCPCATALYST_LOG_INFO("Writing %d PDWs\r\n", num_pdws_processed);

    if (num_pdws_processed > 0)
    {
      body->num_pdws                = num_pdws_processed;
      body->virtual_buffer_address  = (uint64_t)virt_buf_addr;

      pdw_count++;

      WritePDWs(body);
      OCPCATALYST_LOG_INFO("WritePDWs complete. PDW Buffer: %d\r\n", pdw_count);
    }

    pthread_mutex_unlock(&static_virt_buf_mutex);

  }

  return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the Amplitude Offset
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MesagePDWReportController_set_amplutude_offset(void* msg)
{
  // Variables
  struct MessageAmplitudeOffsetBody* payload;
  // DRComponent* drc;

  OCPCATALYST_LOG_DBUG("Message (%s) Received\r\n", __FUNCTION__);

  // @todo Add DRC Component FW Register write
  // if (drc_pdwgen != NULL)
  // {
  if (msg == (void*)0)
  {
    static_amplitude_offset_decidb = 1500;

    OCPCATALYST_LOG_DBUG("Amplitude Offset Default: %d\r\n");
  }
  else
  {
    payload = (struct MessageAmplitudeOffsetBody*)msg;

    OCPCATALYST_LOG_DBUG("Amplitude Offset Received: %d\r\n", payload->amplitude_offset_decidb);

    static_amplitude_offset_decidb = payload->amplitude_offset_decidb;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the Platform Heading
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MesagePDWReportController_set_platform_heading(void* msg)
{
  // Variables
  struct MessagePlatformHeadingBody* payload;

  OCPCATALYST_LOG_DBUG("Message (%s) Received\r\n", __FUNCTION__);

  if (msg == (void*)0)
  {
    static_platform_heading_decideg = 1;

    OCPCATALYST_LOG_DBUG("Platform Heading Default %d\r\n");
  }
  else
  {
    payload = (struct MessagePlatformHeadingBody*)msg;

    OCPCATALYST_LOG_DBUG("Platform Heading Received %d\r\n", payload->platform_heading_decideg);

    static_platform_heading_decideg = payload->platform_heading_decideg;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the Band Configuration
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MessagePDWReportController_set_band_configuration(void* msg)
{
  // Variables
  struct MessageBandConfigurationBody* payload;

  OCPCATALYST_LOG_DBUG("Message (%s) Received\r\n", __FUNCTION__);

  if (msg == (void*)0)
  {
    static_band_1_center_rf_khz     = 11000000;
    static_band_1_ibw_khz           = 2000000;
    static_band_1_attention_decidb  = 0;

    static_band_2_center_rf_khz     = 15000000;
    static_band_2_ibw_khz           = 2000000;
    static_band_2_attention_decidb  = 0;

  }
  else
  {
    payload = (struct MessageBandConfigurationBody*)msg;

//    printf(
//      "Received Band Configuration\r\n" \
//      "===========================\r\n" \
//      "Band:              %u\r\n" \
//      "Band Center RF:    %u\r\n" \
//      "Band IBW:          %u\r\n" \
//      "Band Attenuation:  %u\r\n",
//      payload->band_number,
//      payload->band_center_khz,
//      payload->band_ibw_khz,
//      payload->band_attenuation_decidb
//   );

    if (payload->band_number == 1)
    {
      static_band_1_center_rf_khz     = payload->band_center_khz;
      static_band_1_ibw_khz           = payload->band_ibw_khz;
      static_band_1_attention_decidb  = payload->band_attenuation_decidb;
    }
    else if (payload->band_number == 2)
    {
      static_band_2_center_rf_khz     = payload->band_center_khz;
      static_band_2_ibw_khz           = payload->band_ibw_khz;
      static_band_2_attention_decidb  = payload->band_attenuation_decidb;
    }
  }

  // Clear PDW rejects
  memset(&static_pdw_rejects, 0x00, sizeof(struct MessagePDWRejectsBody)*20);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the PDW Rejects
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MessagePDWReportController_set_pdw_rejects(void* msg)
{
  // Variables
  struct MessagePDWRejectsBody* payload;

  OCPCATALYST_LOG_DBUG("Message (%s) Received\r\n", __FUNCTION__);

  if (msg == (void*)0)
  {
    memset(&static_pdw_rejects, 0x00, sizeof(struct MessagePDWRejectsBody)*20);
  }
  else
  {
    payload = (struct MessagePDWRejectsBody*)msg;
    static_num_pdw_rejects = 0;

    // There will always be 20 PDW Rejects
    for (uint8_t ii = 0; ii < 20; ii++)
    {
      static_pdw_rejects[ii].frequency_low_mhz = payload[ii].frequency_low_mhz;
      static_pdw_rejects[ii].frequency_high_mhz = payload[ii].frequency_high_mhz;
      static_pdw_rejects[ii].bearing_low_decideg = payload[ii].bearing_low_decideg;
      static_pdw_rejects[ii].bearing_high_decideg = payload[ii].bearing_high_decideg;
      static_pdw_rejects[ii].pulse_width_low_ns = payload[ii].pulse_width_low_ns;
      static_pdw_rejects[ii].pulse_width_high_ns = payload[ii].pulse_width_high_ns;
      static_pdw_rejects[ii].amplitude_low_decidb = payload[ii].amplitude_low_decidb;
      static_pdw_rejects[ii].amplitude_high_decidb = payload[ii].amplitude_high_decidb;

      // Increment num reject
      if (payload[ii].frequency_low_mhz != 0)
      {
        static_num_pdw_rejects++;
      }

      // Check
      printf(
        "REJECT FILTER NUM: %d\r\n"                   \
        "/** Message Word 1 */              \r\n" \
        "uint16_t frequency_low_mhz;      %d\r\n" \
        "/** Message Word 2 */              \r\n" \
        "uint16_t frequency_high_mhz;     %d\r\n" \
        "  /** Message Word 3 */            \r\n" \
        "uint16_t bearing_low_decideg;    %d\r\n" \
        "/** Message Word 4 */              \r\n" \
        "uint16_t bearing_high_decideg;   %d\r\n" \
        "/** Message Word 5 */              \r\n" \
        "uint32_t pulse_width_low_ns;     %d\r\n" \
        "/** Message Word 6 */              \r\n" \
        "uint32_t pulse_width_high_ns;    %d\r\n" \
        "/** Message Word 7 */              \r\n" \
        "uint16_t amplitude_low_decidb;   %d\r\n" \
        "/** Message Word 8 */              \r\n" \
        "uint16_t amplitude_high_decidb;  %d\r\n",
        ii,
        static_pdw_rejects[ii].frequency_low_mhz,
        static_pdw_rejects[ii].frequency_high_mhz,
        static_pdw_rejects[ii].bearing_low_decideg,
        static_pdw_rejects[ii].bearing_high_decideg,
        static_pdw_rejects[ii].pulse_width_low_ns,
        static_pdw_rejects[ii].pulse_width_high_ns,
        static_pdw_rejects[ii].amplitude_low_decidb,
        static_pdw_rejects[ii].amplitude_high_decidb
      );
    }
  }
  // }
  // else
  // {
  //   OCPCATALYST_LOG_ERRO("(%s) Cannot find DRComponent\r\n", __FUNCTION__);
  // }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the PDW Amplitude Threshold
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MesagePDWReportController_set_pdw_amplitude_threshold(void* msg)
{
  // Variables
  struct MessagePDWReportControllerPDWAmplitudeThreshold payload;
  // DRComponent* drc;
  uint32_t value;

  OCPCATALYST_LOG_DBUG("Message (%s) Received\r\n", __FUNCTION__);

  if (drc_pdwgen != NULL)
  {

    if (msg == (void*)0)
    {
      // - 20dBm default
      YosemitePDWGenerator_set_fft_threshold(drc_pdwgen, -200);

      value = DRComponent_get_value(drc_pdwgen, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_FFT1_THREG_DATA);

      OCPCATALYST_LOG_INFO("Message (%s) Readback=%d\r\n", __FUNCTION__, value);
    }
    else
    {
      payload = *(struct MessagePDWReportControllerPDWAmplitudeThreshold*)msg;
      value   = payload.pdw_amplitude_threshold;

      OCPCATALYST_LOG_INFO("Message (%s) Write=%x\r\n", __FUNCTION__, value);

      YosemitePDWGenerator_set_fft_threshold(drc_pdwgen, payload.pdw_amplitude_threshold - static_amplitude_offset_decidb);
      YosemitePDWGenerator_set_fft_threshold(drc_pdwgen_band_2, payload.pdw_amplitude_threshold - static_amplitude_offset_decidb);
//      value = DRComponent_get_value(drc_pdwgen, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_FFT1_THREG_DATA);

      OCPCATALYST_LOG_INFO("Message (%s) Readback=%d\r\n", __FUNCTION__, value);
    }
  }
  else
  {
    OCPCATALYST_LOG_ERRO("(%s) Cannot find DRComponent\r\n", __FUNCTION__);
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @brief     Injests a message to set the PDW Generation Source
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MesagePDWReportController_set_pdw_generation_source(void* msg)
{
  // Variables
  struct MessagePDWReportControllerPDWFormatSelection payload;
  DRComponent* drc;
  uint32_t value;

  drc = DRComponent_get_by_name(DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR);

  OCPCATALYST_LOG_DBUG("Message (%s) Received\r\n", __FUNCTION__);

  if (drc != NULL)
  {

    if (msg == (void*)0)
    {
      DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_SCRATCH, 1);
      value = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_SCRATCH);
    }
    else
    {
      payload = *(struct MessagePDWReportControllerPDWFormatSelection*)msg;
      value   = payload.pdw_format_selection;

      DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_SCRATCH, value);
      value = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_SCRATCH);
    }

    OCPCATALYST_LOG_DBUG("Message (%s) Readback=%d\r\n", __FUNCTION__, value);
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Injests a message to set the PDW Report Configuration
 * @param     msg: A Message Body as defined
 * @retval    None
 */
void MessagePDWReportController_set_configuration(void* msg)
{
  // Variables
  struct MessagePDWReportControllerConfiguration payload;
  // DRComponent* drc;
  // uint32_t value1;
  // uint32_t value2;
  // uint32_t value3;
  // uint64_t virtual_addr;

  // uintptr_t* thread_buffer = (uintptr_t*)Heap_allocate(sizeof(uintptr_t));

  // drc     = DRComponent_get_by_name(DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR);

  OCPCATALYST_LOG_DBUG("Message (%s) Received\r\n", __FUNCTION__);

  // if (drc != NULL)
  // {

  //   // Heap_to_hex_string(msg, 14);
  //   // printf("%d\r\n", sizeof(struct MessagePDWReportControllerConfiguration));
  // OCPCATALYST_LOG_INFO("WritePDWs\r\n");

    payload = *(struct MessagePDWReportControllerConfiguration*)msg;
    // value1  = (uint32_t)payload.maximum_pdws_per_message;
    // value2  = payload.maximum_detection_duration_ms*1000*1000; // Convert to ns
    // value3  = payload.virtual_buffer_address;
    // *(thread_buffer) = (uintptr_t)value3;

    // OCPCATALYST_LOG_INFO("Message (%s) Readback=%d %d %p\r\n", __FUNCTION__, value1, value2, value3);
    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_INFO,
      "Message (%s) Readback=%d %d %p\r\n",
      __FUNCTION__,
      (uint32_t)payload.maximum_pdws_per_message,
      (uint32_t)payload.maximum_detection_duration_ms,
      payload.virtual_buffer_address
    );

    payload.maximum_pdws_per_message = 64;

    if (is_threading == 1)
    {
      OCPCATALYST_LOG(
        OCPCATALYST_LOG_TYPE_INFO,
        "Killing Existing PDW callback thread\r\n"
      );
      pthread_cancel(static_pthread_sw_pdw_generation_id);
      is_threading = 0;
    }

    // Kill any generators
    pthread_mutex_lock(&static_sw_generation_mutex);
    is_generating_sw_pdws = 0;
    pthread_mutex_unlock(&static_sw_generation_mutex);

    sleep(1); // cheap way to ensure all processes dead

    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_INFO,
      "PDWs Streaming thread creation.\r\n"
    );

    // pthread_mutex_lock(&static_sw_generation_mutex);
    // is_generating_sw_pdws = 1;
    // pthread_mutex_unlock(&static_sw_generation_mutex);

    // Intialize Semaphore Locked
    sem_init(&static_sem_mutex, 0, 1);
    sem_init(&static_sem_mutex_2, 0, 1);

    // Lock it
    sem_wait(&static_sem_mutex);
    sem_wait(&static_sem_mutex_2);

    pdw_count = 0;

    // Create Interrupt thread
    pthread_create(&static_pthread_sw_pdw_generation_id, NULL, &__message_yosemite_pdw_conf, msg);
    // Start Buffer polling
    pthread_create(&static_pthread_poll_buffer_id, NULL, &__poll_buffer, NULL);
    pthread_create(&static_pthread_poll_buffer_2_id, NULL, &__poll_buffer_2, NULL);

    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_INFO,
      "PDWs Streaming enabled.\r\n"
    );
}

static void on_dma_intc()
{
  __builtin___clear_cache((char*) drc_pdwbuffer->mmap->resource_virtual_memory_address, (((char*)drc_pdwbuffer->mmap->resource_virtual_memory_address) + 0xFFF ));
  __clear_cache((char*) drc_pdwbuffer->mmap->resource_virtual_memory_address, (((char*)drc_pdwbuffer->mmap->resource_virtual_memory_address) + 0xFFF));

  const uintptr_t delta_ptr = (uintptr_t)pdw_buffer_ptr - (uintptr_t)pdw_buffer;

  OCPCATALYST_LOG_DBUG("Received PDW Interrupt\r\n");

  // Perform the Raw read from memory
  volatile void* pdw_mem = (((volatile void*)drc_pdwbuffer->mmap->resource_virtual_memory_address));

  // Memcpy all pdws
  memcpy((void*)pdw_buffer_ptr, (void*)pdw_mem, sizeof(uint32_t)*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*static_num_pdws);

  // Filled the buffer condition
  if (delta_ptr >= PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*PDW_INTERNAL_BUF_COUNT)
  {
    OCPCATALYST_LOG_INFO("Buffer Full: %lu bytes\r\n", delta_ptr);

    // while (is_pdws_transfered == 1)
    // {
    //   OCPCATALYST_LOG_DBUG("PDW transfer hold\r\n");
    //   printf("PDW transfer hold\r\n");
    // }

    // Ensure we are ready to process pdws
    // if (is_pdws_transfered == 0)
    // {
    //   pthread_mutex_lock(&static_poll_buffer_mutex);
    //   is_pdws_transfered = 1;
    //   pthread_mutex_unlock(&static_poll_buffer_mutex);

    // Unlock thread
    sem_post(&static_sem_mutex);
    // Reset pointer
    pdw_buffer_ptr = pdw_buffer;
    // }
  }
  else if (delta_ptr < PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*PDW_INTERNAL_BUF_COUNT)
  {
    // Jump to next end of buffer
    pdw_buffer_ptr = (uint32_t*)((uint32_t*)pdw_buffer_ptr + (PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*static_num_pdws));

    OCPCATALYST_LOG_DBUG(
      "Adding %d PDWs to buffer\r\n",
      static_num_pdws
    );
  }
  else
  {
    // printf("INTC RECEIVED, doing NOTHING\r\n");
  }

  // __builtin___clear_cache((char*) drc_pdwbuffer->mmap->resource_virtual_memory_address, (((char*)drc_pdwbuffer->mmap->resource_virtual_memory_address) + 1024));
  // __clear_cache((char*) drc_pdwbuffer->mmap->resource_virtual_memory_address, (((char*)drc_pdwbuffer->mmap->resource_virtual_memory_address) + 1024));
  // __builtin___clear_cache((char*) drc_pdwbuffer->mmap->resource_virtual_memory_address, (((char*)drc_pdwbuffer->mmap->resource_virtual_memory_address) + 0xFFF ));
  // __clear_cache((char*) drc_pdwbuffer->mmap->resource_virtual_memory_address, (((char*)drc_pdwbuffer->mmap->resource_virtual_memory_address) + 0xFFF));
}


static void on_dma_intc_band2()
{
  __builtin___clear_cache((char*) drc_pdwbuffer_2->mmap->resource_virtual_memory_address, (((char*)drc_pdwbuffer_2->mmap->resource_virtual_memory_address) + 0xFFF ));
  __clear_cache((char*) drc_pdwbuffer_2->mmap->resource_virtual_memory_address, (((char*)drc_pdwbuffer_2->mmap->resource_virtual_memory_address) + 0xFFF));

  const uintptr_t delta_ptr = (uintptr_t)pdw_buffer_ptr_2 - (uintptr_t)pdw_buffer_2;

  OCPCATALYST_LOG_DBUG("Received PDW Interrupt\r\n");

  // Perform the Raw read from memory
  volatile void* pdw_mem = (((volatile void*)drc_pdwbuffer_2->mmap->resource_virtual_memory_address));

  // Memcpy all pdws
  memcpy((void*)pdw_buffer_ptr_2, (void*)pdw_mem, sizeof(uint32_t)*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*static_num_pdws);

  // Filled the buffer condition
  if (delta_ptr >= PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*PDW_INTERNAL_BUF_COUNT)
  {

    OCPCATALYST_LOG_INFO("Buffer Full: %lu bytes\r\n", delta_ptr);
    // Unlock thread
    sem_post(&static_sem_mutex_2);
    // Reset pointer
    pdw_buffer_ptr_2 = pdw_buffer_2;
  }
  else if (delta_ptr < PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*PDW_INTERNAL_BUF_COUNT)
  {
    // Jump to next end of buffer
    pdw_buffer_ptr_2 = (uint32_t*)((uint32_t*)pdw_buffer_ptr_2 + (PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*static_num_pdws));

    OCPCATALYST_LOG_DBUG(
      "Adding %d PDWs to buffer\r\n",
      static_num_pdws
    );
  }
}
// static void* pdw_buffer;
// static uint8_t

void* __message_yosemite_pdw_conf(void* args)
{
  // Variables
  struct MessagePDWReportControllerConfiguration payload;
  payload = *(struct MessagePDWReportControllerConfiguration*)args;
  message_buffer =  (char*)Heap_allocate(sizeof(char)*12);
  pdw_buffer     = (uint32_t*)Heap_allocate(sizeof(uint32_t)*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*PDW_INTERNAL_BUF_COUNT);
  pdw_buffer_ptr = pdw_buffer;
  pdw_buffer_2     = (uint32_t*)Heap_allocate(sizeof(uint32_t)*PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*PDW_INTERNAL_BUF_COUNT);
  pdw_buffer_ptr_2 = pdw_buffer;
  // num pdws * 64 bytes
  // const uint32_t pdw_transfer_bytes = payload.maximum_pdws_per_message*((PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*32)/8);
  const uint32_t pdw_transfer_bytes = YOSE_NUM_PDWS*((PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT*32)/8);

  OCPCATALYST_LOG(
    OCPCATALYST_LOG_TYPE_INFO,
    "Message Received: %d %d %p\r\n",
    payload.maximum_pdws_per_message,
    payload.maximum_detection_duration_ms,
    payload.virtual_buffer_address
  );

  uintptr_t virt_addr = (uintptr_t)payload.virtual_buffer_address;
  void* dma_alloc = (void *)DMA_ADDR;

  virt_buf_addr = virt_addr;
  // static_num_pdws = payload.maximum_pdws_per_message;
  static_num_pdws = YOSE_NUM_PDWS;

  OCPCATALYST_LOG_INFO("Sleeping for 5 seconds....\r\n");
  sleep(5);

  // Create all Components

  //
  // RFDC Xilinx
  //
  drc_rfdc  = RFDCXilinx_create();

  OCPCATALYST_ASSERT(!(drc_rfdc != NULL), "Failed to create DRComponent\r\n");

  OCPCATALYST_ASSERT(RFDCXilinx_setup(drc_rfdc), "Failed to setup RFDC");

  sleep(1);

  OCPCATALYST_LOG_INFO("Real PDW Generation to address: %p\r\n", virt_addr);

  //
  // PDWBuffer
  //
  drc_pdwbuffer = PDWBuffer_create();

  drc_pdwbuffer_2 = DRComponent_create_noncatalyst_component(
    DRCOMPONENT_NAME_PDW_BUFFER,
    (uint32_t*)DMA_ADDR_BAND2,
    DRCOMPONENT_RESERVED_ADDR_SPACE_PDW_BUFFER
  );



  PDWBuffer_setup(drc_pdwbuffer);
  // PDWBuffer_print(drc_pdwbuffer, payload.maximum_pdws_per_message);

  sleep(1);

  //
  // YosemitePDWGenerator
  //
  drc_pdwgen = YosemitePDWGenerator_create();

  drc_pdwgen_band_2 = DRComponent_create_noncatalyst_component(
    DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR,
    (uint32_t*)0xA0030000,
    DRCOMPONENT_RESERVED_ADDR_SPACE_YOSEMITE_PDW_GENERATOR
  );

  OCPCATALYST_ASSERT(!(drc_pdwgen != NULL), "Failed to create DRComponent\r\n");
  OCPCATALYST_ASSERT(!(drc_pdwgen_band_2 != NULL), "Failed to create DRComponent\r\n");

  // Initialize and run YosemitePDWGenerator
  OCPCATALYST_ASSERT(YosemitePDWGenerator_setup(drc_pdwgen, static_num_pdws), "Failed to setup YosemitePDWGenerator\r\n");
  OCPCATALYST_ASSERT(YosemitePDWGenerator_setup(drc_pdwgen_band_2, static_num_pdws), "Failed to setup YosemitePDWGenerator\r\n");

  // DRComponent_free(drc_pdwgen);


  //
  // DMAAXIXilinx
  //
  OCPCATALYST_LOG_INFO("Allocating DMA Channel at %p\r\n", DMA_ADDR);

  drc_dma = DMAAXIXilinx_create();
  drc_dma_band_2 =  DRComponent_create_noncatalyst_component(
    DRCOMPONENT_NAME_DMA_AXI_XILINX,
    (uint32_t*)0xA0020000,
    DRCOMPONENT_RESERVED_ADDR_SPACE_DMA_AXI_XILINX
  );

  OCPCATALYST_ASSERT(DMAAXIXilinx_setup(drc_dma, (uintptr_t)dma_alloc), "Failed to setup DMA Channel\r\n");
  OCPCATALYST_ASSERT(DMAAXIXilinx_setup(drc_dma_band_2, (uintptr_t)DMA_ADDR_BAND2), "Failed to setup DMA Channel\r\n");

  sleep(1);

  OCPCATALYST_LOG_INFO("Starting DMA polling on %p\r\n", DMA_ADDR);

  OCPCATALYST_ASSERT(DMAAXIXilinx_s2mm_transfer(drc_dma, pdw_transfer_bytes), "Failed to setup DMA Channel\r\n");
  OCPCATALYST_ASSERT(DMAAXIXilinx_s2mm_transfer(drc_dma_band_2, pdw_transfer_bytes), "Failed to setup DMA Channel\r\n");

  OCPCATALYST_ASSERT(YosemitePDWGenerator_capture(drc_pdwgen), "Failed to capture from YosemitePDWGenerator\r\n");
  OCPCATALYST_ASSERT(YosemitePDWGenerator_capture(drc_pdwgen_band_2), "Failed to capture from YosemitePDWGenerator\r\n");

  // Variables
  uint32_t status;
  DMAAXIXilinxStatusRegister* flags;

  if (drc_dma != (void*)0)
  {
    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_INFO,
      "%s Polling values\r\n",
      DRCOMPONENT_NAME_DMA_AXI_XILINX
    );
    struct timespec sleep_time;
    sleep_time.tv_sec  = 0;
    sleep_time.tv_nsec = 2500;
    // sleep_time.tv_nsec = 1000000;

    while (1)
    {
      // Get Status reg
      status = DMAAXIXilinx_get_s2mm_status(drc_dma);
      OCPCATALYST_LOG_DBUG("DMA 1 status %x\r\n", status);
      flags = (DMAAXIXilinxStatusRegister*)&status;

      if ((flags->halted == 1) || (flags->IOC_Irq == 1))
      {
        // Callback current batch
        OCPCATALYST_LOG_DBUG("poll intc callback\r\n");
        on_dma_intc();
        // Clear interrupt
        OCPCATALYST_LOG_DBUG("poll intc handle\r\n");
        // DMAAXIXilinx_handle_interrupt(drc, dma_address);
        DMAAXIXilinx_clear_interrupt(drc_dma, (uintptr_t)dma_alloc);
        // Ask for more
        OCPCATALYST_LOG_DBUG("poll intc transfer\r\n");
        DMAAXIXilinx_s2mm_transfer(drc_dma, pdw_transfer_bytes);
        YosemitePDWGenerator_capture(drc_pdwgen);
      }
      else
      {
        OCPCATALYST_LOG_DBUG("no pending transfers on band 1...\r\n");
      }

      // Get Status reg
      status = DMAAXIXilinx_get_s2mm_status(drc_dma_band_2);
      OCPCATALYST_LOG_DBUG("DMA 2 status %x\r\n", status);
      flags = (DMAAXIXilinxStatusRegister*)&status;

      if ((flags->halted == 1) || (flags->IOC_Irq == 1))
      {
        // Callback current batch
        OCPCATALYST_LOG_DBUG("poll intc callback\r\n");
        on_dma_intc_band2();
        // Clear interrupt
        OCPCATALYST_LOG_DBUG("poll intc handle\r\n");
        // DMAAXIXilinx_handle_interrupt(drc, dma_address);
        DMAAXIXilinx_clear_interrupt(drc_dma_band_2, (uintptr_t)DMA_ADDR_BAND2);
        // Ask for more
        OCPCATALYST_LOG_DBUG("poll intc transfer\r\n");
        DMAAXIXilinx_s2mm_transfer(drc_dma_band_2, pdw_transfer_bytes);
        YosemitePDWGenerator_capture(drc_pdwgen_band_2);
      }
      else
      {
        OCPCATALYST_LOG_DBUG("no pending transfers on band 2...\r\n");
      }

      nanosleep(&sleep_time, NULL);
    }
  }

  return NULL;
}
