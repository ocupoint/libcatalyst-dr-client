/// @file   Message.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-25-2021

#include <stdlib.h>
#include "private/ocp.h"
#include "private/debug.h"
#include "private/event/eventhandleronerror.h"
#include "private/message/message.h"
#include "public/errorcodes.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates a Message
 * @memberof  Message
 * @retval    Message *
 */
Message* Message_create()
{
  Message* msg;

  msg = (Message*)malloc(sizeof(Message));

  if (msg == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to allocate memory for message");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
  }

  return msg;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates a Message Payload
 * @memberof  Message
 * @param     payload_size_bytes: The size of the message payload in bytes
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t Message_create_payload(Message* msg, size_t payload_size_bytes)
{
  msg->payload = (Message*)malloc(payload_size_bytes);

  if (msg->payload == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to allocate memory for message->payload");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Frees a Message
 * @param  msg: Message
 * @retval None
 */
void Message_free(Message* msg)
{
  free(msg->payload);
  free(msg);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
