/// @file   MessageDigitalReceiver.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-25-2021


#include <stdlib.h>
#include "private/debug.h"
#include "private/ocp.h"
#include "private/main.h"
#include "private/message/messagedigitalreceiver.h"
#include "private/message/messagealias.h"


/**
 * @brief  Setup Digital Receiver
 * Initializes Memory and sets appropriate registers to put the Digital Receiver in the
 * start state.
 * @retval None
 */
void MessageDigitalReceiver_setup()
{
  if (libcatalyst_dr_driver_main() != EXIT_SUCCESS)
  {
    OCPCATALYST_LOG_ERRO("Failed to Initialize Catalyst Digital Receiver Client");
  }
}
