/// @file   OCPCatalystDebugLogger.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-20-2021


#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <pthread.h>
#include <string.h>
#include "private/helpers.h"
#include "private/debug.h"

static pthread_mutex_t printf_mutex;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Debug Logger Function
 * @param  log_type: Log type enum
 * @param  format: String Format
 * @param  args: String Arguments
 * @retval None
 */
void OCPCatalystDebugLogger_log(enum LOG_TYPE log_type, const char *format, ...)
{
#ifndef _OCPCDR_PRINTS_
  // Variables
  va_list args;
  char dbg_fmt[51];

  // validate we want to see the log
  if (log_type >= ENV_OCPCATALYST_LOG_LEVEL_TYPE)
  {
    // Get the variable args
    va_start(args, format);

    // Add log message type
    switch (log_type)
    {
    case OCPCATALYST_LOG_TYPE_INTR:
      strcpy(dbg_fmt,"\x1b[35m[Catalyst Digital Receiver Client] INTR\x1b[0m: ");
      break;
    case OCPCATALYST_LOG_TYPE_DBUG:
      strcpy(dbg_fmt,"\x1b[90m[Catalyst Digital Receiver Client] DBUG\x1b[0m: ");
      break;
    case OCPCATALYST_LOG_TYPE_INFO:
    default:
      strcpy(dbg_fmt,"[Catalyst Digital Receiver Client] INFO: ");
      break;
    case OCPCATALYST_LOG_TYPE_WARN:
      strcpy(dbg_fmt,"\x1b[33m[Catalyst Digital Receiver Client] WARN\x1b[0m: ");
      break;
    case OCPCATALYST_LOG_TYPE_ERRO:
      strcpy(dbg_fmt,"\x1b[31m[Catalyst Digital Receiver Client] ERRO\x1b[0m: ");
      break;
    }

    // Concatted the debug message
    char* msg = String_concat((char*)&dbg_fmt, format);

    pthread_mutex_lock(&printf_mutex);
    // Print the rendered format string - this produces a nonsense output
    // if argp was aleady passed to another function earlier
    vprintf(msg, args);
    pthread_mutex_unlock(&printf_mutex);
    // Free the variable argument pointer
    va_end(args);
    String_destroy(msg);
  }
#endif
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
