/// @file   FPGAInterface.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   01-20-2021

#include "stdint.h"
#include "private/helpers.h"
#include "private/ocp.h"
#include "private/debug.h"
#include "private/memorymap.h"


// POSIX libs
#if defined(__linux__) || defined(__APPLE__)
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#else
#include "xil_io.h"
#include "xil_cache.h"
#endif


#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief     FPGA Interface for hardware writing
 * @param     mem: MemoryMap*
 * @param     addr: Address in the component to access
 * @retval    None
 */
void FPGAInterface_write(MemoryMap* mem, uint32_t addr, uint32_t value)
{
  // Variables
  char* mmap_str;

  OCPCATALYST_LOG_DBUG("Performing FPGA Write to register: %x\r\n", addr);
  mmap_str = MemoryMap_to_string(mem);
  OCPCATALYST_LOG_DBUG("%s\r\n", mmap_str);

  Heap_free(mmap_str);

  __builtin___clear_cache((char*)mem->resource_virtual_memory_address, (((char*)mem->resource_physical_memory_addr) + mem->resource_memory_size_bytes));
  __clear_cache((char*)mem->resource_virtual_memory_address, (((char*)mem->resource_physical_memory_addr) + mem->resource_memory_size_bytes));

  if (
    (!MemoryMap_validate(mem)) &&
    (addr <= mem->resource_memory_size_bytes)
  )
  {
#ifdef XENV_STANDALONE_H
    OCPCATALYST_LOG_DBUG("Performing Physical Hardware Write: Phys=%p Virt=%p Value=%p\r\n", (uint32_t*)mem->resource_physical_memory_addr + addr, (uint32_t*)mem->resource_virtual_memory_address + addr, value);
#ifdef ENABLE_SAFETY
    XStl_RegUpdate(mem->resource_physical_memory_addr + addr, value);
#else
    Xil_Out32(mem->resource_physical_memory_addr + addr, value);
    Xil_ICacheInvalidateRange((INTPTR)mem->resource_virtual_memory_address, mem->resource_memory_size_bytes);
    Xil_DCacheInvalidateRange((INTPTR)mem->resource_virtual_memory_address, mem->resource_memory_size_bytes);

#endif
#else
    // Perform the Raw write
    OCPCATALYST_LOG_DBUG("Performing Virtual Hardware Write: Phys=%p Virt=%p Value=%p\r\n", (uint32_t*)mem->resource_physical_memory_addr + addr, (uint32_t*)mem->resource_virtual_memory_address + addr, value);
    *((volatile uint32_t*)((volatile uint32_t*)mem->resource_virtual_memory_address + (addr/4))) = value;
#endif
  __builtin___clear_cache((char*)mem->resource_virtual_memory_address, (((char*)mem->resource_physical_memory_addr) + mem->resource_memory_size_bytes));
  __clear_cache((char*)mem->resource_virtual_memory_address, (((char*)mem->resource_physical_memory_addr) + mem->resource_memory_size_bytes));

  }
  else
  {
    OCPCATALYST_LOG_ERRO("Specified hardware write out of bounds: Addr %d >= %d Allocation \r\n", addr, mem->resource_memory_size_bytes);
  }
}


/**
 * @brief     FPGA Interface for hardware reading
 * @param     mem: MemoryMap*
 * @param     addr: Address in the component to access
 * @retval    uint32_t
 */
uint32_t FPGAInterface_read(MemoryMap* mem, uint32_t addr)
{
  // Variables
  uint32_t readback;
  char* mmap_str;

  OCPCATALYST_LOG_DBUG("Performing FPGA Read to register: %x\r\n", addr);

  mmap_str = MemoryMap_to_string(mem);

  OCPCATALYST_LOG_DBUG("%s\r\n", mmap_str);

  Heap_free(mmap_str);

  __builtin___clear_cache((char*)mem->resource_virtual_memory_address, (((char*)mem->resource_physical_memory_addr) + mem->resource_memory_size_bytes));
  __clear_cache((char*)mem->resource_virtual_memory_address, (((char*)mem->resource_physical_memory_addr) + mem->resource_memory_size_bytes));

  if (
    (!MemoryMap_validate(mem)) &&
    ((uint32_t)addr <= (uint32_t)mem->resource_memory_size_bytes)
  )
  {
#ifdef XENV_STANDALONE_H
  OCPCATALYST_LOG_DBUG("Performing Physical Hardware Read: Addr=0x%x\r\n", mem->resource_virtual_memory_address + addr);
  readback = Xil_In32(mem->resource_physical_memory_addr + addr, value);
  Xil_ICacheInvalidateRange((INTPTR)mem->resource_virtual_memory_address, mem->resource_memory_size_bytes);
  Xil_DCacheInvalidateRange((INTPTR)mem->resource_virtual_memory_address, mem->resource_memory_size_bytes);
  OCPCATALYST_LOG_DBUG("Received: Value=%x\r\n", readback);
#else

  // Perform the Raw read
  OCPCATALYST_LOG_DBUG("Performing Virtual Hardware Read: Phys=%p Virt=%p\r\n", (char*)mem->resource_physical_memory_addr + addr, (uint32_t*)mem->resource_virtual_memory_address + (addr/4));
  readback = *((volatile uint32_t*)((volatile uint32_t*)mem->resource_virtual_memory_address + (addr/4)));
  OCPCATALYST_LOG_DBUG("Received: Value=%x\r\n", readback);
#endif

  __builtin___clear_cache((char*)mem->resource_virtual_memory_address, (((char*)mem->resource_physical_memory_addr) + mem->resource_memory_size_bytes));
  __clear_cache((char*)mem->resource_virtual_memory_address, (((char*)mem->resource_physical_memory_addr) + mem->resource_memory_size_bytes));

  return readback;
  }
  else
  {
    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_ERRO,
      "Failed to read from Hardware Address 0x%x/%ld<=>0x%x+0x%x\r\n",
      mem->resource_physical_memory_addr,
      mem->resource_memory_size_bytes,
      mem->resource_virtual_memory_address,
      addr
    );
    return 0;
  }
}

#ifdef __cplusplus
}
#endif
