/// @file   PlatformPentek5950.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   05-30-2021

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "public/libcatalyst-dr-driver.h"
#include "private/debug.h"
#include "private/psinterface.h"
#include "private/platform/pentek/platformpentek5950.h"

// static uint8_t __PlatformPentek5950_print_lmx2594();
static uint8_t __PlatformPentek5950_setup_lmx2594();
static uint8_t __PlatformPentek5950_setup_lattice(bool use_external_ref);
static uint8_t __PlatformPentek5950_toggle_lattice_sysref();


/**
 * @brief     Initialize the Pentek Platform
 * @memberof  PlatformPentek5950
 * @retval    0=EXIT_SUCCESS; 1=EXIT_FAILURE
 */
uint8_t PlatformPentek5950_setup()
{
  OCPCATALYST_LOG_INFO("Initializing the Pentek 5950\r\n");
#if (ENV_EXT_SAMPLE_CLOCK == 1)
  OCPCATALYST_ASSERT(__PlatformPentek5950_setup_lattice(true), "Failed to setup Latice\r\n");
#else
  OCPCATALYST_ASSERT(__PlatformPentek5950_setup_lattice(false), "Failed to setup Latice\r\n");
  OCPCATALYST_ASSERT(__PlatformPentek5950_setup_lmx2594(), "Failed to setup PLL\r\n");
  OCPCATALYST_ASSERT(__PlatformPentek5950_toggle_lattice_sysref(), "Failed to toggle SysRef\r\n");
#endif
  return EXIT_SUCCESS;
}

uint8_t __PlatformPentek5950_setup_lattice(bool use_external_ref)
{
  // Variables
  PSInterfaceI2C* psi2c;

  psi2c = PSInterfaceI2C_create();

  OCPCATALYST_ASSERT(PSInterfaceI2C_open(psi2c, PENTEK5950_LATTICE_I2C_BUS, PENTEK5950_LATTICE_I2C_ADDR), "Failed to open Lattice\r\n");

  if (use_external_ref)
  {
    OCPCATALYST_LOG_INFO("Configuring External Sample Clock\r\n");
    // Provide external sample clock and reference
    OCPCATALYST_ASSERT(PSInterfaceI2C_write(psi2c, (uint8_t*)PENTEK5950_LATTICE_REG_0_EXT_SAMPLE_CLK, PENTEK5950_LATTICE_I2C_BUFFER_SIZE), "Failed to write to Lattice\r\n");
  }
  else
  {
    OCPCATALYST_LOG_INFO("Configuring Internal Sample Clock\r\n");
    // Use internal PLL
    OCPCATALYST_ASSERT(PSInterfaceI2C_write(psi2c, (uint8_t*)PENTEK5950_LATTICE_REG_0_PLL_SAMPLE_CLK, PENTEK5950_LATTICE_I2C_BUFFER_SIZE), "Failed to write to Lattice\r\n");
  }

  PSInterfaceI2C_free(psi2c);

  OCPCATALYST_LOG_INFO("Lattice Setup Complete.\r\n");

  return EXIT_SUCCESS;
}

uint8_t __PlatformPentek5950_toggle_lattice_sysref()
{
  // Variables
  PSInterfaceI2C* psi2c;

  psi2c = PSInterfaceI2C_create();

  OCPCATALYST_ASSERT(PSInterfaceI2C_open(psi2c, PENTEK5950_LATTICE_I2C_BUS, PENTEK5950_LATTICE_I2C_ADDR), "Failed to open Lattice\r\n");

  OCPCATALYST_LOG_INFO("Toggle Sysref\r\n");
  // Provide external sample clock and reference
  OCPCATALYST_ASSERT(PSInterfaceI2C_write(psi2c, (uint8_t*)PENTEK5950_LATTICE_REG_1_EXT_SYSREF, PENTEK5950_LATTICE_I2C_BUFFER_SIZE), "Failed to write to Lattice\r\n");

  PSInterfaceI2C_free(psi2c);

  OCPCATALYST_LOG_INFO("Sysref Toggle Complete.\r\n");

  return EXIT_SUCCESS;
}

/**
 * @brief     Setup the LMX2594 PLL
 * @memberof  PlatformPentek5950
 * @retval    0=EXIT_SUCCESS; 1=EXIT_FAILURE
 */
uint8_t __PlatformPentek5950_setup_lmx2594()
{
  // Variables
  PSInterfaceSPI* psspi;
  char fd_name[15] = "/dev/spidev1.0";

  OCPCATALYST_LOG_INFO("LMX2595 Power On: %s.\r\n", fd_name);

  psspi = PSInterfaceSPI_create();

  PSInterfaceSPI_open(psspi, fd_name);

  // Power on and reset the LMX2594
  PSInterfaceSPI_transfer(
    psspi,
    (uint8_t*)PENTEK5950_LMX2594_REG_SEQ_POWERON,
    PENTEK5950_LMX2594_SPI_BYTES_PER_WORD,
    PENTEK5950_LMX2594_REG_SEQ_POWERON_NUM_WORDS,
    NULL
  );
  sleep(1);

  OCPCATALYST_LOG_INFO("LMX2595 Tune.\r\n");

  // Tune the LMX2594
  PSInterfaceSPI_transfer(
    psspi,
    (uint8_t*)PENTEK5950_LMX2594_REG_SEQ_TUNE,
    PENTEK5950_LMX2594_SPI_BYTES_PER_WORD,
    PENTEK5950_LMX2594_REG_SEQ_TUNE_NUM_WORDS,
    NULL
  );

  sleep(1);

  // Readback and verify all registers
  PSInterfaceSPI_transfer(
    psspi,
    (uint8_t*)PENTEK5950_LMX2594_REG_SEQ_READBACK,
    PENTEK5950_LMX2594_SPI_BYTES_PER_WORD,
    PENTEK5950_LMX2594_REG_SEQ_TUNE_NUM_WORDS,
    (uint8_t*)PENTEK5950_LMX2594_REG_SEQ_VERIFY
  );

  // exit(1);
  sleep(1);

  PSInterfaceSPI_free(psspi);

  OCPCATALYST_LOG_INFO("LMX2594 Setup Complete.\r\n");

  return EXIT_SUCCESS;
}


