// /// @file   XilinxRFDC.c
// /// @brief  File Description
// ///
// /// @author Nicholas Molinski
// /// @date   03-11-2021

// #include <string.h>
// #include <stdio.h>
// #include <stdlib.h>
// #include <unistd.h>
// #include <time.h>
// #include <limits.h>
// #include <dirent.h>
// #include <fcntl.h>
// #include <errno.h>
// #include <stdio.h>
// #include <sys/types.h>
// #include <sys/stat.h>
// #include <sys/param.h>
// #include <sys/ioctl.h>
// #include <linux/i2c-dev.h>

// #include "private/debug.h"
// #include "private/helpers.h"
// #include "private/drcomponent/drcomponent.h"
// #include "private/drcomponent/rfdc.h"
// #include "private/i2c.h"
// #include "private/psinterface.h"
// #include "private/platform/xilinx/xilinxrfdc.h"
// #include "private/platform/pentek/pentekdevice.h"


// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /** LMK04208 Configuration */
// static uint32_t XILINXRFDC_LMK04208_CONFIG[XILINXRFDC_LMK04208_CONFIG_LENGTH] = {
//   0x00160040, 0x80140320, 0x80140321, 0x80140322,
//   0xC0140023, 0x40140024, 0x80141E05, 0x03300006,
//   0x01300007, 0x06010008, 0x55555549, 0x9102410A,
//   0x0401100B, 0x1B0C006C, 0x2302886D, 0x0200000E,
//   0x8000800F, 0xC1550410, 0x00000058, 0x02C9C419,
//   0x8FA8001A, 0x10001E1B, 0x0021201C, 0x0180033D,
//   0x0200033E, 0x003F001F
// };

// #define MAX_FREQ  27
// #define LMK04208_count 26
// #define LMX2594_A_count 113

// typedef struct {
//   uint32_t XFrequency;
//   uint32_t LMX2594_A[LMX2594_A_count];
// } XClockingLmx;

// uint32_t lmx2594DefaultTable[LMX2594_A_count] = {
//   0x700000,
//   0x6F0000,
//   0x6E0000,
//   0x6D0000,
//   0x6C0000,
//   0x6B0000,
//   0x6A0000,
//   0x690021,
//   0x680000,
//   0x670000,
//   0x663F80,
//   0x650011,
//   0x640000,
//   0x630000,
//   0x620200,
//   0x610888,
//   0x600000,
//   0x5F0000,
//   0x5E0000,
//   0x5D0000,
//   0x5C0000,
//   0x5B0000,
//   0x5A0000,
//   0x590000,
//   0x580000,
//   0x570000,
//   0x560000,
//   0x55D300,
//   0x540001,
//   0x530000,
//   0x521E00,
//   0x510000,
//   0x506666,
//   0x4F0026,
//   0x4E0003,
//   0x4D0000,
//   0x4C000C,
//   0x4B0800, // 0840
//   0x4A0000,
//   0x4906E4,
//   0x48001E,
//   0x470049, //0x47008D,
//   0x46C350,
//   0x450000,
//   0x4403E8,
//   0x430000,
//   0x4201F4,
//   0x410000,
//   0x401388,
//   0x3F0000,
//   0x3E0322,
//   0x3D00A8,
//   0x3C0000,
//   0x3B0001,
//   0x3A0001, //0x3A8001,
//   0x390020,
//   0x380000,
//   0x370000,
//   0x360000,
//   0x350000,
//   0x340820,
//   0x330080,
//   0x320000,
//   0x314180,
//   0x300300,
//   0x2F0300,
//   0x2E07FE, //*
//   0x2DC0DF,
//   0x2C1F23, //*
//   0x2B0000,
//   0x2A0000,
//   0x290000,
//   0x280000,
//   0x2703E8,
//   0x260000,
//   0x250304,
//   0x240064,
//   0x230004,
//   0x220000,
//   0x211E21,
//   0x200393,
//   0x1F43EC, // chdiv_div2 = 1
//   0x1E318C,
//   0x1D318C,
//   0x1C0488,
//   0x1B0002,
//   0x1A0DB0,
//   0x190624,
//   0x18071A,
//   0x17007C,
//   0x160001,
//   0x150401,
//   0x14B048, // VCO sel = 6
//   0x1327B7,
//   0x120064,
//   0x11012C,
//   0x100080,
//   0x0F064F,
//   0x0E1E70,
//   0x0D4000,
//   0x0C5001, // PLL pre = 1
//   0x0B0018, // R = 1
//   0x0A10D8,//MULT=10 -//0x0A10D8, // mult = 1
//   0x091604, // osc = 2x
//   0x082000,
//   0x0740B2,
//   0x06C802,
//   0x0500C8,
//   0x040A43,
//   0x030642,
//   0x020500,
//   0x010808,
//   0x00641C
// };

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// static uint8_t __LMK04208_tune(XilinxRFDC* xilrfdc);
// static uint8_t __LMX2594_tune(XilinxRFDC* xilrfdc);

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// static void __update_i2c(int fd, uint8_t r[27]);

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief  Xilinx RFDC Create
//  * @retval XilinxRFDC *
//  */
// XilinxRFDC* XilinxRFDC_create()
// {
//   // Variables
//   XilinxRFDC* xilrfdc;

//   xilrfdc = (XilinxRFDC*)Heap_allocate(sizeof(XilinxRFDC));

//   if (xilrfdc != NULL)
//   {
//     xilrfdc->pll_reference_frequency_khz  = 0;
//     xilrfdc->tile                         = XILRFDC_TILE_NONE;
//     xilrfdc->mixer_frequency_khz          = 0;
//     xilrfdc->i2c_bus                      = 0;
//   }

//   return xilrfdc;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief  Xilinx Power up all RFDC Tiles
//  * @retval 0=EXIT_SUCCESS, 1=EXIT_FAILURE
//  */
// uint8_t XilinxRFDC_power_up_all(XilinxRFDC* xilrfdc)
// {
//   // Variables
//   uint8_t major;
//   uint8_t minor;
//   uint8_t revision;
//   uint64_t timestamp;
//   DRComponent* drc;

//   drc = DRComponent_get_by_name(DRCOMPONENT_NAME_RFDC);

//   if (drc == NULL)
//   {
//     return EXIT_FAILURE;
//   }

//   OCPCATALYST_LOG_INFO("Powering up RF Data Converter\r\n");

//   major     = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_IP_VERSION_MAJOR);
//   minor     = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_IP_VERSION_MINOR);
//   revision  = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_IP_VERSION_REVISION);

//   if ((major | minor | revision) == 0)
//   {
//     return EXIT_FAILURE;
//   }

//   OCPCATALYST_LOG_INFO("=== RF Data Converter Version: %d.%d.%d ===\r\n", major, minor, revision);
//   OCPCATALYST_LOG(
//     OCPCATALYST_LOG_TYPE_INFO,
//     "End States: ADC 0=%d; \r\n",
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_RESTART_END_STATE)
//   );


//   DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_MASTER_RESET, 0x1);
//   sleep(1);

//   XilinxRFDC_print_tile_status(drc);

//   OCPCATALYST_LOG_DBUG("Waiting...\r\n");

//   timestamp = Timestamp_create();

//   while (DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_CURRENT_STATE) < 3)
//   {
//     sleep(1);
//   }

//   OCPCATALYSTete: 17us

// while (1)
// {
//   /* code */
// }

//   while (DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_CURRENT_STATE) < 10)
//   {
//     sleep(1);
//   }

//   OCPCATALYST_LOG_INFO("Clock Detection Complete\r\n");
//   XilinxRFDC_print_tile_status(drc);

//   return EXIT_SUCCESS;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief  Logs tile status in a Debug (Level=INFO) Message
//  * @param  drc: DRComponent*
//  * @retval None
//  */
// void XilinxRFDC_print_tile_status(DRComponent* drc)
// {
//   OCPCATALYST_LOG_INFO("Current Tile status: DAC0=%d; DAC1=%d; DAC2=%d; DAC3=%d; ADC0=%d; ADC1=%d; ADC2=%d; ADC3=%d;\r\n",
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_DAC0_TILE_CURRENT_STATE),
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_DAC1_TILE_CURRENT_STATE),
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_DAC2_TILE_CURRENT_STATE),
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_DAC3_TILE_CURRENT_STATE),
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_ADC0_TILE_CURRENT_STATE),
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_ADC1_TILE_CURRENT_STATE),
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_ADC2_TILE_CURRENT_STATE),
//     DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_ADC3_TILE_CURRENT_STATE)
//   );
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief  Tunes the onboard PLL to the provided reference
//  * @param  xilrfdc: XilinxRFDC
//  * @param  pll_reference_freq_mhz: uint32_t
//  * @retval 0=EXIT_SUCCESS; 1=EXIT_FAILURE
//  */
// uint8_t XilinxRFDC_tune_pll_reference_frequency(XilinxRFDC* xilrfdc, uint32_t pll_reference_frequency_khz)
// {
//   // Variables
//   OCPCATALYST_LOG_INFO("Setting Oscillator frequency to: %d\r\n", pll_reference_frequency_khz);

//   xilrfdc->pll_reference_frequency_khz = pll_reference_frequency_khz;

//   // if (__LMK04208_tune(xilrfdc) == EXIT_FAILURE)
//   // {
//   //   return EXIT_FAILURE;
//   // }


//   PentekDevice_init();

//   if (__LMX2594_tune(xilrfdc) == EXIT_FAILURE)
//   {
//     return EXIT_FAILURE;
//   }

//   OCPCATALYST_LOG_INFO("PLL Tuned: %d\r\n", pll_reference_frequency_khz);
//   return EXIT_SUCCESS;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief  Tune an LMK04208
//  * @retval None
//  */
// uint8_t __LMK04208_tune(XilinxRFDC* xilrfdc)
// {
//   // Variables
//   int fd_i2c;
//   char fd_i2c_name[20];
//   unsigned char tx_array[4];

//    // Generate the path
//   sprintf(fd_i2c_name, "/dev/i2c-%d", xilrfdc->i2c_bus);

//   fd_i2c = open(fd_i2c_name, O_RDWR);

//   if (ioctl(fd_i2c, I2C_SLAVE_FORCE, 0x2f) < 0)
//   {
//     OCPCATALYST_LOG_ERRO("Failed to set address\r\n");

//     return EXIT_FAILURE;
//   }

//   close(fd_i2c);

//   for (size_t ii = 0; ii < XILINXRFDC_LMK04208_CONFIG_LENGTH; ii++)
//   {
//     tx_array[3] = (unsigned char) (XILINXRFDC_LMK04208_CONFIG[ii]) & (0xFF);
//     tx_array[2] = (unsigned char) (XILINXRFDC_LMK04208_CONFIG[ii] >> 8) & (0xFF);
//     tx_array[1] = (unsigned char) (XILINXRFDC_LMK04208_CONFIG[ii] >> 16) & (0xFF);
//     tx_array[0] = (unsigned char) (XILINXRFDC_LMK04208_CONFIG[ii] >> 24) & (0xFF);

//     I2C_write(fd_i2c, 2, 4, tx_array);

//     usleep(1000);
//     sleep(1000);
//   }

//   return EXIT_SUCCESS;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// uint8_t __LMX2594_tune(XilinxRFDC* xilrfdc)
// {
//   // Variables
//   char dev[20] = "/dev/spidev1.0";
//   uint8_t buf[3];
//   PSInterfaceSPI* psspi;

//   psspi = PSInterfaceSPI_create();

//   PSInterfaceSPI_open(psspi, dev);

//   for (size_t jj = 0; jj < LMX2594_A_count; jj++)
//   {
//     uint32_t val = lmx2594DefaultTable[jj];

//     buf[0] = (uint8_t)((0xFF0000 & val) >> 16);
//     buf[1] = (uint8_t)((0x00FF00 & val) >>  8);
//     buf[2] = (uint8_t)((0x0000FF & val) >>  0);

//     PSInterfaceSPI_transfer(psspi, buf, 8, 3);

//     usleep(1000000);

//   }

//   PSInterfaceSPI_free(psspi);

//   // int fd_i2c;
//   // char fd_i2c_name[20];

//   // sprintf(fd_i2c_name, "/dev/i2c-%d", xilrfdc->i2c_bus);
//   // fd_i2c = open(fd_i2c_name, O_RDWR);

//   // if (ioctl(fd_i2c, I2C_SLAVE_FORCE, 0x2f) < 0) {
//   //   OCPCATALYST_LOG_ERRO("Failed to set address\r\n");
//   //   return EXIT_FAILURE;
//   // }

//   // unsigned char tx_array[3];

//   // if (xilrfdc->pll_reference_frequency_khz == 0)
//   // {
//   //   tx_array[2] = 0x3;
//   //   tx_array[1] = 0;
//   //   tx_array[0] = 0;

//   //   I2C_write(fd_i2c, 0xd, 3, tx_array);
//   //   return EXIT_SUCCESS;
//   // }

//   // for(size_t ii=0 ; ii < MAX_FREQ; ii++)
//   // {
//   //   if (ClockingLmx[ii].XFrequency == xilrfdc->pll_reference_frequency_khz)
//   //   {
//   //     __update_i2c(fd_i2c, (uint8_t*)ClockingLmx[ii].LMX2594_A);
//   //   }
//   // }
//   // __update_i2c( fd_i2c, (uint8_t*)ClockingLmx[0].LMX2594_A);

//   OCPCATALYST_LOG_INFO("LMX2594 Tuned\r\n");
//   return EXIT_SUCCESS;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// void __update_i2c(int fd, uint8_t r[27])
// {
//   int Index=0;
//   int val;
//   unsigned char tx_array[3];
//   /*
//   * 1. Apply power to device.
//   * 2. Program RESET = 1 to reset registers.
//   * 3. Program RESET = 0 to remove reset.
//   * 4. Program registers as shown in the register map in REVERSE order from highest to lowest.
//   * 5. Program register R0 one additional time with FCAL_EN = 1 to ensure that the VCO calibration runs from a
//   * stable state.
//   */
//   tx_array[2] = 0x2;
//   tx_array[1] = 0;
//   tx_array[0] = 0;

//   val = tx_array[0] | (tx_array[1] << 8) | (tx_array[2] << 16 );

//   I2C_write(fd, 0xd, 3, tx_array);
//   usleep(100000);

//   tx_array[2] = 0;
//   tx_array[1] = 0;
//   tx_array[0] = 0;
//   val = tx_array[0] | (tx_array[1] << 8) | (tx_array[2] << 16 ) ;

//   I2C_write(fd, 0xd, 3, tx_array);
//   usleep(100000);

//   for (Index = 0; Index < LMX2594_A_count; Index++) {
//     tx_array[2] = (unsigned char) (r[Index]) & (0xFF);
//     tx_array[1] = (unsigned char) (r[Index] >> 8) & (0xFF);
//     tx_array[0] = (unsigned char) (r[Index] >> 16) & (0xFF);
//     val = tx_array[0] | (tx_array[1] << 8) | (tx_array[2] << 16 ) ;

//     I2C_write(fd, 0xd, 3, tx_array);
//     usleep(100000);
//   }

//   /* FCAL_EN = 1 */
//   tx_array[2] = (unsigned char) (r[112]) & (0xFF);
//   tx_array[1] = (unsigned char) (r[112] >> 8) & (0xFF);
//   tx_array[0] = (unsigned char) (r[112] >> 16) & (0xFF);

//   val = tx_array[0] | (tx_array[1] << 8) | (tx_array[2] << 16 ) ;

//   printf("LMX configured \n");
//   I2C_write(fd, 0xd, 3, tx_array);
// }