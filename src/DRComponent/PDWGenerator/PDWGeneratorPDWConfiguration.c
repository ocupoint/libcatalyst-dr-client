/// @file   PDWGeneratorPDWConfiguration.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-04-2021


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "private/fpgainterface.h"
#include "private/memorymap.h"

// /**
//  * @brief  Create a PDW Configuration to write to component
//  * @todo   Make static
//  * @struct PDWGeneratorPDWConfiguration
//  * @retval PDWGeneratorPDWConfiguration*
//  */
// PDWGeneratorPDWConfiguration* PDWGeneratorPDWConfiguration_create()
// {
//   return (PDWGeneratorPDWConfiguration*)malloc(sizeof(PDWGeneratorPDWConfiguration));
// }


// /**
//  * @brief  Frees a configuration
//  * @note   Beta
//  * @retval None
//  */
// void PDWGeneratorPDWConfiguration_free(PDWGeneratorPDWConfiguration* conf)
// {
//   free(conf);
// }