/// @file   DMAAXIXilinx.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   05-24-2021

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include "public/libcatalyst-dr-driver.h"
#include "private/debug.h"
#include "private/drcomponent/drcomponent.h"
#include "private/drcomponent/pdwreports.h"
#include "private/drcomponent/dmaaxixilinx.h"
#include "private/helpers.h"
#include "private/fpgainterface.h"
#include "private/main.h"
#include "private/memorymap.h"
#include "private/ocp.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates the DMA AXI Xilinx component
 * @memberof  DMAAXIXilinx
 * @retval    DRComponent *
 */
DRComponent* DMAAXIXilinx_create()
{
  return DRComponent_create_noncatalyst_component(
    DRCOMPONENT_NAME_DMA_AXI_XILINX,
    DRCOMPONENT_BASE_ADDR_DMA_AXI_XILINX,
    DRCOMPONENT_RESERVED_ADDR_SPACE_DMA_AXI_XILINX
  );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Setup routine for the DMA AXI Xilinx component
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @param     dma_address:
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t DMAAXIXilinx_setup(DRComponent* drc, uintptr_t dma_address)
{
  // Variables
  uint32_t control_register;
  uint32_t dma_msb;
  uint32_t dma_lsb;


  if (drc != (void*)0)
  {
    // Reset
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR, 0x4);

    // Check for reset to be deasserted
    while((DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR) & 0x4))
    {
      OCPCATALYST_LOG_DBUG("Awaiting Reset Completion\r\n");
    }

    // Halt the DMA
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR, 0x0);

    dma_msb = (((uint64_t)dma_address) & (0xFFFFFFFF00000000)) >> 32;
    dma_lsb = (((uint64_t)dma_address) & (0x00000000FFFFFFFF)) >> 0;

    OCPCATALYST_LOG_DBUG(
      "%s Setting DMA Target 0x%x%x.\r\n",
      DRCOMPONENT_NAME_DMA_AXI_XILINX,
      dma_msb,
      dma_lsb
    );

    // Destination address
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DA_MSB, (uint32_t)dma_msb);
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DA,     (uint32_t)dma_lsb);

    // Get current status
    control_register = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR);

    // Enable the DMA
    // Bit 0  = AXIDMA Enable
    // Bit 12 = IRQ Enable
    control_register = control_register  | DRCOMPONENT_DMA_AXI_XILINX_CR_MODE;

    OCPCATALYST_LOG_DBUG(
      "%s Setting Control Register=0x%x\r\n",
      DRCOMPONENT_NAME_DMA_AXI_XILINX,
      control_register
    );

    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR, control_register);

    // Check Initialization success on write
    if ((DRCOMPONENT_DMA_AXI_XILINX_CR_MODE & DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR)) == DRCOMPONENT_DMA_AXI_XILINX_CR_MODE)
    {
      OCPCATALYST_LOG_DBUG(
        "%s Checking DMA Status\r\n",
        DRCOMPONENT_NAME_DMA_AXI_XILINX
      );

      // Keep checking until halted bit is desasserted
      while ((0x1 & DMAAXIXilinx_get_s2mm_status(drc)) != 0x0)
      {
        OCPCATALYST_LOG_DBUG("DMA Intializing...\r\n");
      }

      OCPCATALYST_LOG(
        OCPCATALYST_LOG_TYPE_INFO,
        "%s Initialized DMA! %x\r\n",
        DRCOMPONENT_NAME_DMA_AXI_XILINX,
        DMAAXIXilinx_get_s2mm_status(drc)
      );

      return EXIT_SUCCESS;
    }
  }

  OCPCATALYST_LOG(
    OCPCATALYST_LOG_TYPE_ERRO,
    "%s Failed to Setup! DMACR State 0x%x\r\n",
    DRCOMPONENT_NAME_DMA_AXI_XILINX,
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR)
  );

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Perform a manual DMA transfer
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @param     num_bytes:
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t DMAAXIXilinx_s2mm_transfer(DRComponent* drc, size_t num_bytes)
{

  // Valid parameters
  if ((drc != (void*)0) && (num_bytes != 0))
  {

    // Check DMA available for transfer

    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_DBUG,
      "%s Performing DMA transfer on 0x%x%x.\r\n",
      DRCOMPONENT_NAME_DMA_AXI_XILINX,
      DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DA_MSB),
      DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DA)
    );

    // Write Length (Trigger)
    // Write Length (this is the trigger)
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_LENGTH, num_bytes);


    // DMA bytes buffer
    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_DBUG,
      "%s Reading number of bytes %d.\r\n",
      DRCOMPONENT_NAME_DMA_AXI_XILINX,
      DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_LENGTH)
    );

    return EXIT_SUCCESS;
  }

  OCPCATALYST_LOG_ERRO("Failed to transfer DMA\r\n");

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Get the s2mm status from the DMA Engine
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @retval    uint32_t
 */
uint32_t DMAAXIXilinx_get_s2mm_status(DRComponent* drc)
{
  // Variables
  uint32_t status;
  DMAAXIXilinxStatusRegister* flags;

  if (drc != (void*)0)
  {
    status = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMASR);

    flags = (DMAAXIXilinxStatusRegister*)&status;

    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_DBUG,
      "%s Status: %x\r\n     \
      halted           : %d \r\n\
      idle             : %d \r\n\
      SGIncld          : %d \r\n\
      DMAIntErr        : %d \r\n\
      DMASlvErr        : %d \r\n\
      DMADecErr        : %d \r\n\
      SGIntErr         : %d \r\n\
      SGSlvErr         : %d \r\n\
      SGDeErr          : %d \r\n\
      IOC_Irq          : %d \r\n\
      Dly_Irq          : %d \r\n\
      Err_Irq          : %d \r\n\
      IRQThresholdSts  : %d \r\n\
      IRQDelaySts      : %d \r\n",
      DRCOMPONENT_NAME_DMA_AXI_XILINX,
      status,
      flags->halted,
      flags->idle,
      flags->SGIncld,
      flags->DMAIntErr,
      flags->DMASlvErr,
      flags->DMADecErr,
      flags->SGIntErr,
      flags->SGSlvErr,
      flags->SGDeErr,
      flags->IOC_Irq,
      flags->Dly_Irq,
      flags->Err_Irq,
      flags->IRQThresholdSts,
      flags->IRQDelaySts
    );

    return status;
  }

  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Handle DMA interrupts
//  * @memberof  DMAAXIXilinx
//  * @param     drc:
//  * @param     dma_address:
//  * @retval    None
//  */
// void DMAAXIXilinx_handle_interrupt(DRComponent* drc, void* dma_address)
// {
//   // Variables
//   uint32_t conf;

//   if ((drc != (void*)0) && (dma_address != (void*)0))
//   {
//     OCPCATALYST_LOG(
//       OCPCATALYST_LOG_TYPE_INFO,
//       "%s Interrupt Triggered.\r\n",
//       DRCOMPONENT_NAME_DMA_AXI_XILINX
//     );

//     DMAAXIXilinx_clear_interrupt(drc);
//     // DMAAXIXilinx_s2mm_transfer(drc, dma_address);

//     // Get the control configuration
//     // conf =  DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMACR);
//   }
// }

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Manually poll the DMA for transfers
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @param     dma_address:
 * @retval    None
 */
void DMAAXIXilinx_poll(DRComponent* drc, void* dma_address, void (*intc_callback)(), size_t num_bytes)
{
  // Variables
  uint32_t status;

  DMAAXIXilinxStatusRegister* flags;
  DRComponent* drc_dma = drc;

  if ((drc != (void*)0) && (dma_address != (void*)0))
  {
    OCPCATALYST_LOG_DBUG(
      "%s Polling values\r\n",
      DRCOMPONENT_NAME_DMA_AXI_XILINX
    );

    struct timespec sleep_time;
    sleep_time.tv_sec  = 0;
    sleep_time.tv_nsec = 10000;

    while (1)
    {
      // Get Status reg
      status = DMAAXIXilinx_get_s2mm_status(drc_dma);
      flags = (DMAAXIXilinxStatusRegister*)&status;

      if (flags->IOC_Irq != 0)
      {
        // Callback current batch
        OCPCATALYST_LOG_DBUG("poll intc callback\r\n");
        intc_callback();
        // Clear interrupt
        OCPCATALYST_LOG_DBUG("poll intc handle\r\n");
        DMAAXIXilinx_clear_interrupt(drc_dma, (uintptr_t)dma_address);
        // // Ask for more
        OCPCATALYST_LOG_DBUG("poll intc transfer\r\n");
        DMAAXIXilinx_s2mm_transfer(drc_dma, num_bytes);
      }

      nanosleep(&sleep_time, NULL);
    }

  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Clears the DMA Interrupt
 * @memberof  DMAAXIXilinx
 * @param     drc:
 * @param     dma_alloc:
 * @retval    uint8_t
 */
uint8_t DMAAXIXilinx_clear_interrupt(DRComponent* drc, uintptr_t dma_alloc)
{
  // Variables
  uint32_t status;
  // uint32_t conf;
  DMAAXIXilinxStatusRegister* flags;

  if (drc != NULL)
  {
    status = DMAAXIXilinx_get_s2mm_status(drc);
    OCPCATALYST_LOG_DBUG("Status needing clearing %x\r\n", status);
    flags = (DMAAXIXilinxStatusRegister*)&status;

    if (flags->IOC_Irq)
    {
      // Clear IRQ status
      status = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMASR);
      status = status | 0x00001000;
      DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMASR, status);
      status = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DMASR);

      OCPCATALYST_LOG_DBUG(
        "%s Cleared interrupt: Status %x\r\n",
        DRCOMPONENT_NAME_DMA_AXI_XILINX,
        status
      );
    }

    // Transfers done
    if (flags->halted)
    {
      // Reset/reenale
      OCPCATALYST_LOG_WARN("DMA Halted\r\n");

      DMAAXIXilinx_setup(drc, dma_alloc);

      OCPCATALYST_LOG_DBUG("DMA Reset Gracefully\r\n");

    }

    // If we have an error we have to reset DMA manually
    if (flags->Err_Irq)
    {
      OCPCATALYST_LOG(
        OCPCATALYST_LOG_TYPE_ERRO,
        "%s IRQ Transfer Error\r\n",
        DRCOMPONENT_NAME_DMA_AXI_XILINX
      );
    }

    // IDLE DMA is not a good one
    if (flags->idle)
    {
      OCPCATALYST_LOG(
        OCPCATALYST_LOG_TYPE_WARN,
        "%s No pending transactions\r\n",
        DRCOMPONENT_NAME_DMA_AXI_XILINX
      );
    }

    // DMA Internal Error reset
    if (flags->DMAIntErr)
    {
      OCPCATALYST_LOG(
        OCPCATALYST_LOG_TYPE_ERRO,
        "%s DMA Internal Error\r\n",
        DRCOMPONENT_NAME_DMA_AXI_XILINX
      );

      DMAAXIXilinx_setup(drc, DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_DMA_AXI_XILINX_S2MM_DA));
    }

    status = DMAAXIXilinx_get_s2mm_status(drc);
    OCPCATALYST_LOG_DBUG("Cleared Status %x\r\n", status);
    OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_WARN, "Status cleared\r\n");

    return EXIT_SUCCESS;
  }

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
