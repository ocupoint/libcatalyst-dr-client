/// @file   DRComponent.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-26-2021

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "public/errorcodes.h"
#include "private/debug.h"
#include "private/event/event.h"
#include "private/drcomponent/drcomponent.h"
#include "private/helpers.h"
#include "private/main.h"
#include "private/memorymap.h"
#include "private/fpgainterface.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/** Lazy implementation of static resources */

// Holds global array to all Digital Receiver Components
static DRComponent* static_dr_component_list = NULL;
// Holds the current number of alslocated Digital Receiver Components
static uint8_t static_num_dr_components = 0;


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a Digital Receiver Component
 * @memberof  DRComponent *
 * @param     name[16]:
 * @param     base_address: Base address to the component
 * @param     reserved_address_space: Reserved Address Space component takes
 * @retval    DRComponent *
 */
DRComponent* DRComponent_create(const char name[DRCOMPONENT_NAME_LENGTH], uint32_t* base_address, uint32_t reserved_address_space)
{
  // Variables
  DRComponent* drc;
  uint32_t     timeval;
  uint32_t     readback;

  // if (static_dr_component_list == NULL)
  // {
  //   static_dr_component_list = (DRComponent*)Heap_allocate(DRCOMPONENT_MAX_NUM_OF_COMPONENTS*sizeof(DRComponent));
  // }

  // if (static_num_dr_components == DRCOMPONENT_MAX_NUM_OF_COMPONENTS)
  // {
  //   OCPCATALYST_LOG_ERRO("Failed to get memory for Digital Receiver Component.\r\n");
  //   EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

  //   return NULL;
  // }

  drc = (DRComponent*)Heap_allocate(DRCOMPONENT_MAX_NUM_OF_COMPONENTS*sizeof(DRComponent)); // &static_dr_component_list[static_num_dr_components];

  OCPCATALYST_LOG_INFO("Intializing Digital Receiver Component: %s...\r\n", name);
  // Will create a memory map
  drc->mmap  = MemoryMap_create(base_address, reserved_address_space);

  if (drc->mmap == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to get memory map for Digital Receiver Component.\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

    return NULL;
  }

  static_num_dr_components += 1;


  // Copy Component Name
  memcpy(drc->name, name, DRCOMPONENT_NAME_LENGTH);

  if (drc->name == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to get name of Digital Receiver Component\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

    return NULL;
  }

  // Initialize Component
  drc->drawing_number = 0;
  drc->version        = 0;
  drc->revision       = 0;
  drc->date           = 0;
  drc->shadow_memory  = (void*)0;

  // Currently Writeable FPGA Components
  if ((base_address >= (uint32_t*)0xA0000000) && (base_address < (uint32_t*)0xC0000000))
  {

    // First Address of any component is always the drawing number
    drc->drawing_number = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_HEADER_DRAWING_NUMBER);

    drc->version = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_HEADER_VERSION);

    drc->revision = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_HEADER_REVISION);

    drc->date = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_HEADER_DATE);

    // All memory is readback

    // Time to verify write access
    // Write to echo register current time
    timeval =  (uint32_t)(Timestamp_timedelta() & 0xFFFFFFFF);
    // bits32modify
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_HEADER_ECHO, timeval);

    readback = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_HEADER_ECHO);

    if (timeval != readback)
    {
      OCPCATALYST_LOG_ERRO("Failed to communicate with Digital Receiver Component\r\n");
      EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
    }
  }

  if (drc->drawing_number != 0)
  {
    OCPCATALYST_LOG_INFO("Component: <%s>; Drawing Number=%x; Version=%x; Revision=%x\r\n",
      name,
      drc->drawing_number,
      drc->version,
      drc->revision
    );
  }
  else if (drc->mmap != NULL)
  {
    OCPCATALYST_LOG_INFO("Component: <%s>;\r\n",
      name
    );
  }
  else
  {
    OCPCATALYST_LOG_ERRO("Abstract Component: <%s>;\r\n",
      name
    );
  }

  return drc;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Create a Digital Receiver Component that does conform to the Catalyst Component Framework
 * @memberof  DRComponent
 * @param     name[DRCOMPONENT_NAME_LENGTH]:
 * @param     base_address:
 * @param     reserved_address_space:
 * @retval    DRComponent *
 */
DRComponent* DRComponent_create_noncatalyst_component(const char name[DRCOMPONENT_NAME_LENGTH], uint32_t* base_address, uint32_t reserved_address_space)
{
   // Variables
  DRComponent* drc;

  if (static_dr_component_list == NULL)
  {
    static_dr_component_list = (DRComponent*)Heap_allocate(DRCOMPONENT_MAX_NUM_OF_COMPONENTS*sizeof(DRComponent));
  }

  if (static_num_dr_components == DRCOMPONENT_MAX_NUM_OF_COMPONENTS)
  {
    OCPCATALYST_LOG_ERRO("Failed to get memory for Digital Receiver Component.\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

    return NULL;
  }

  drc = &static_dr_component_list[static_num_dr_components];

  OCPCATALYST_LOG_INFO("Intializing Digital Receiver Component: %s...\r\n", name);
  // Will create a memory map
  drc->mmap  = MemoryMap_create(base_address, reserved_address_space);

  if (drc->mmap == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to get memory map for Digital Receiver Component.\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

    return NULL;
  }

  static_num_dr_components += 1;


  // Copy Component Name
  memcpy(drc->name, name, DRCOMPONENT_NAME_LENGTH);

  if (drc->name == NULL)
  {
    OCPCATALYST_LOG_ERRO("Failed to get name of Digital Receiver Component\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

    return NULL;
  }

  // Initialize Component
  drc->drawing_number = 0;
  drc->version        = 0;
  drc->revision       = 0;
  drc->date           = 0;
  drc->shadow_memory  = (void*)0;

  if (drc->mmap != NULL)
  {
    OCPCATALYST_LOG_INFO("Component: <%s:Virt=%p:Phys=%p>;\r\n",
      name,
      drc->mmap->resource_virtual_memory_address,
      drc->mmap->resource_physical_memory_addr
    );

    OCPCATALYST_LOG_INFO("Component: <%s:Virt=%p:Phys=%p>;\r\n",
      name,
      drc->mmap->resource_virtual_memory_address,
      drc->mmap->resource_physical_memory_addr
    );
  }
  else
  {
    OCPCATALYST_LOG_ERRO("Abstract Component: <%s>;\r\n",
      name
    );
  }

  return drc;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Gets a DRComponent by name
 * @memberof  DRComponent
 * @param     name[DRCOMPONENT_NAME_LENGTH]: Component Name
 * @retval    DRComponent *
 */
DRComponent* DRComponent_get_by_name(const char name[DRCOMPONENT_NAME_LENGTH])
{
  for (size_t ii = 0; ii < static_num_dr_components; ii++)
  {
    if (strcmp(static_dr_component_list[ii].name, name) == 0)
    {
      return &static_dr_component_list[ii];
    }
  }

  OCPCATALYST_LOG_ERRO("Failed to find Component\r\n");
  EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);

  return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Get value of Digital Receiver Component Register
 * @memberof  DRComponent
 * @param     drc: DRComponent *
 * @param     vinfo: DRComponentValueInfo
 * @retval    uint32_t
 */
uint32_t DRComponent_get_value(DRComponent* drc, DRComponentValueInfo vinfo)
{
  if (drc == NULL)
  {
    OCPCATALYST_LOG_ERRO("Null component passed\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
  }

  return Bits32_read(
    FPGAInterface_read(drc->mmap, (void*)(vinfo.addr)),
    vinfo.mask,
    vinfo.offset
  );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Set value of Digital Receiver Component Register
 * @memberof  DRComponent
 * @param     drc: DRComponent *
 * @param     vinfo: DRComponentValueInfo
 * @param     value: uint32_t
 * @retval    None
 */
void DRComponent_set_value(DRComponent* drc, DRComponentValueInfo vinfo, uint32_t value)
{
  uint32_t readback;
  uint32_t new_word;

  if (drc == NULL)
  {
    OCPCATALYST_LOG_ERRO("Null component passed\r\n");
    EventHandlerOnError_emit(MEMORY_ERROR, __FUNCTION__);
  }

  // Get the whole 32 bit data word
  readback = DRComponent_get_value(drc, (DRComponentValueInfo){ .addr=vinfo.addr, .mask=0xFFFFFFFF, .offset=0 });

  // Form the new word
  new_word = Bits32_write(
    readback,
    vinfo.mask,
    vinfo.offset,
    value
  );

  // Write the new word
  FPGAInterface_write(
    drc->mmap,
    (void*)(vinfo.addr),
    new_word
  );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Free a Digital Receiver Component from memory
 * @memberof  DRComponent
 * @param     drc:
 * @retval    None
 */
void DRComponent_free(DRComponent* drc)
{
  MemoryMap_free(drc->mmap);

  free(drc->shadow_memory);
  // free(drc);
}