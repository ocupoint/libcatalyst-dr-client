/// @file   RFDCXilinx.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   05-26-2021

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "public/libcatalyst-dr-driver.h"
#include "private/debug.h"
#include "private/drcomponent/drcomponent.h"
#include "private/drcomponent/rfdcxilinx.h"
#include "private/helpers.h"
#include "private/fpgainterface.h"
#include "private/main.h"
#include "private/memorymap.h"
#include "private/ocp.h"
#include "private/psinterface.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates the RFDC Xilinx component
 * @memberof  RFDCXilinx
 * @retval    DRComponent *
 */
DRComponent* RFDCXilinx_create()
{
  return DRComponent_create_noncatalyst_component(
    DRCOMPONENT_NAME_RFDC_XILINX,
    DRCOMPONENT_BASE_ADDR_RFDC_XILINX,
    DRCOMPONENT_RESERVED_ADDR_SPACE_RFDC_XILINX
  );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Setup RFDC Xilinx component
 * @memberof  RFDCXilinx
 * @param     drc:
 * @retval    DRComponent *
 */
uint8_t RFDCXilinx_setup(DRComponent* drc)
{
  // Variables
  uint8_t major;
  uint8_t minor;
  uint8_t revision;
  uint64_t timestamp;

  if (drc != (void*)0)
  {
    OCPCATALYST_LOG_INFO("%s Setup\r\n", DRCOMPONENT_NAME_RFDC_XILINX);

    major     = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_IP_VERSION_MAJOR);
    minor     = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_IP_VERSION_MINOR);
    revision  = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_IP_VERSION_REVISION);

    OCPCATALYST_LOG_INFO("=== RF Data Converter Version: %d.%d.%d ===\r\n", major, minor, revision);
    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_INFO,
      "Start States: ADC 0=%d; End States: ADC 0=%d; \r\n",
      DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_RESTART_START_STATE),
      DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_RESTART_END_STATE)
    );


    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_MASTER_RESET, 0x1);
    sleep(1);

    RFDCXilinx_print_tile_status(drc);

     OCPCATALYST_LOG_DBUG("Waiting...\r\n");

    timestamp = Timestamp_create();

    while (DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_CURRENT_STATE) < 3)
    {
      sleep(1);
    }

    OCPCATALYST_LOG_INFO("Power Supply Adjustment Phase Complete: %luus\r\n", Timestamp_create() - timestamp);
    RFDCXilinx_print_tile_status(drc);

    while (DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_CURRENT_STATE) < 10)
    {
      sleep(1);
      RFDCXilinx_print_tile_status(drc);

    }

    OCPCATALYST_LOG_INFO("Clock Detection Complete\r\n");
    RFDCXilinx_print_tile_status(drc);

    return EXIT_SUCCESS;
  }

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  Logs tile status in a Debug (Level=INFO) Message
 * @param  drc: DRComponent*
 * @retval None
 */
void RFDCXilinx_print_tile_status(DRComponent* drc)
{
  OCPCATALYST_LOG_INFO("Current Tile state: DAC0=%d; DAC1=%d; DAC2=%d; DAC3=%d; ADC0=%d; ADC1=%d; ADC2=%d; ADC3=%d;\r\n",
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_CURRENT_STATE),
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_CURRENT_STATE),
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_CURRENT_STATE),
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_CURRENT_STATE),
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_CURRENT_STATE),
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_CURRENT_STATE),
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_CURRENT_STATE),
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_CURRENT_STATE)
  );
    OCPCATALYST_LOG_INFO("PLL Lock status: DAC0=%d; DAC1=%d; DAC2=%d; DAC3=%d; ADC0=%d; ADC1=%d; ADC2=%d; ADC3=%d;\r\n",
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC0_TILE_COMMON_STATUS) & 0x8,
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC1_TILE_COMMON_STATUS) & 0x8,
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC2_TILE_COMMON_STATUS) & 0x8,
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_DAC3_TILE_COMMON_STATUS) & 0x8,
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC0_TILE_COMMON_STATUS) & 0x8,
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC1_TILE_COMMON_STATUS) & 0x8,
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC2_TILE_COMMON_STATUS) & 0x8,
    DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_RFDC_XILINX_ADC3_TILE_COMMON_STATUS) & 0x8
  );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////