/// @file   YosemitePDWGenerator.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   05-23-2021


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include "public/libcatalyst-dr-driver.h"
#include "private/debug.h"
#include "private/drcomponent/drcomponent.h"
#include "private/drcomponent/yosemitepdwgenerator.h"
#include "private/helpers.h"
#include "private/fpgainterface.h"
#include "private/main.h"
#include "private/memorymap.h"
#include "private/ocp.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates the YosemitePDWGenerator IP Core DrCompoent
 * @memberof  YosemitePDWGenerator
 * @retval    DRComponent *
 */
DRComponent* YosemitePDWGenerator_create()
{
  // Currently non-conforming to Catalyst Standard
  return DRComponent_create_noncatalyst_component(
    DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR,
    DRCOMPONENT_BASE_ADDR_YOSEMITE_PDW_GENERATOR,
    DRCOMPONENT_RESERVED_ADDR_SPACE_YOSEMITE_PDW_GENERATOR
  );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Setups routine for the Yosemite PDW Generator
 * @memberof  YosemitePDWGenerator
 * @param     drcomponent:
 * @param     num_pdws:
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t YosemitePDWGenerator_setup(DRComponent* drc, size_t num_pdws)
{
  if (drc != (void*)0)
  {
    // Reset Core
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_RESET, 0x1);
    // Do I have to clear the reset?
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_RESET, 0x0);
    // Enable the component
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_ENABLE, 0x1);
    // DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CONTINUOS_STREAM, 0x1);
    // Set the Capture length and Packet size to 10 PDWS
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_PACKETSIZE_AXI4_STREAM_DMA_MASTER, num_pdws*4);
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURELENGTH_REG_DATA, num_pdws*4);
    // > Adc_stream should be “0” for pdw processing
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_ADCSTREAMREG_DATA, 0);
    // > Set the thresholds “0001”
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_FFT1_THREG_DATA, 0xFFFF);
    // DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_FFT1_THREG_DATA, 0x20C5);
    // DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_FFT1_THREG_DATA, 0x20C5);
    // Set the Capture Length (CW Declaration time)
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CWREPORTCYCLESREG_DATA, 0xF424);

    // Ensure readback works
    if (DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_ENABLE) == 0x1)
    {
      // Now validate setup
      OCPCATALYST_LOG(
        OCPCATALYST_LOG_TYPE_INFO,
        "%s Setup Success: Capture Length=%d; ADC Stream Mode=0x%x; FFT Threshold=0x%x\r\n",
        DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR,
        DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURELENGTH_REG_DATA),
        DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_ADCSTREAMREG_DATA),
        DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_FFT1_THREG_DATA)
      );

      OCPCATALYST_LOG(
        OCPCATALYST_LOG_TYPE_INFO,
        "%s Timestamp=%d\r\n",
        DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR,
        DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_IPCORE_TIMESTAMP)
      );

      return EXIT_SUCCESS;
    }
  }

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Enables the PDW capture trigger
 * @memberof  YosemitePDWGenerator
 * @param     drc:
 * @retval    0=EXIT_SUCCESS, 1=EXIT_FAILURE
 */
uint8_t YosemitePDWGenerator_capture(DRComponent* drc)
{
  // Variables
  uint32_t capture_trigger;

  if (drc != (void*)0)
  {
    capture_trigger = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURETRIGGER_REG_DATA);

    // Initialize state to 0
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURETRIGGER_REG_DATA, 0x0);

    OCPCATALYST_LOG_DBUG(
      "%s Capture State: 0x%x\r\n",
      DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR,
      capture_trigger
    );

    // Toggle on and off the capture
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURETRIGGER_REG_DATA, 0x1);
    // Readback Toggle on
    capture_trigger = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURETRIGGER_REG_DATA);

    OCPCATALYST_LOG_DBUG(
      "%s Capture State: 0x%x\r\n",
      DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR,
      capture_trigger
    );

    // DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURETRIGGER_REG_DATA, 0x0);
    // // Readback Toggle off
    // capture_trigger = DRComponent_get_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_CAPTURETRIGGER_REG_DATA);
    // OCPCATALYST_LOG(
    //   OCPCATALYST_LOG_TYPE_INFO,
    //   "%s Capture State: 0x%x\r\n",
    //   DRCOMPONENT_NAME_YOSEMITE_PDW_GENERATOR,
    //   capture_trigger
    // );

    OCPCATALYST_LOG_INFO("Capture Triggered Successfully Toggled\r\n");

    return EXIT_SUCCESS;
  }

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

#define THRSH_POW 536870


/**
 * @brief     YosemitePDWGenerator
 * @memberof  YosemitePDWGenerator
 * @param     drc:
 * @param     amplitude_dbm:
 * @todo      provide arbitrary scale value
 * @retval    0=EXIT_SUCCESS; 1=EXIT_FAILURE
 */
uint8_t YosemitePDWGenerator_set_fft_threshold(DRComponent* drc, int16_t amplitude_decidbm)
{
  // Variables
  uint32_t scale_factor;
  long double  scaled_down;
  long double  anti_log;
  long double  denominator;

  if (drc != NULL)
  {
    OCPCATALYST_LOG_INFO("Setting FFT Threshold: %d decidbm.\r\n", amplitude_decidbm);

    // Clip Amplitude scale
//    if (amplitude_decidbm >= 1700)
//    {
//      amplitude_decidbm = 1700;
//    }
//    else if (amplitude_decidbm <= 0)
//    {
//      amplitude_decidbm = 0;
//   }

    scaled_down 	= ((long double)amplitude_decidbm)/(long double)100;

    anti_log     	= (long double)pow(10, scaled_down);

    scale_factor  = (uint32_t)round((long double)((long double)anti_log*(long double)THRSH_POW));
//    printf("Input %d\r\n", amplitude_decidbm);
//    printf("AMPLITUDE CALC: (10^(decidbm/100))/(2^(-23)) =  %LF,%LF = %x \r\n", scaled_down, anti_log, scale_factor);
    // scale_factor = 0xFFFF;
    DRComponent_set_value(drc, DRCOMPONENT_VALUE_INFO_YOSEMITE_PDW_GENERATOR_FFT1_THREG_DATA, scale_factor);

    return EXIT_SUCCESS;

  }

  OCPCATALYST_LOG_ERRO("Failed to set FFT Threshold.\r\n");

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
