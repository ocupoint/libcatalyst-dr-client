/// @file   PDWBuffer.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-04-2021

#include <stdio.h>
#include <stdlib.h>
#include "public/libcatalyst-dr-driver.h"
#include "private/debug.h"
#include "private/drcomponent/drcomponent.h"
#include "private/drcomponent/pdwbuffer.h"
#include "private/helpers.h"
#include "private/fpgainterface.h"
#include "private/main.h"
#include "private/memorymap.h"
#include "private/ocp.h"

#define PDWBUFFER_WRAP 0x40

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Creates a PDW Buffer
 * @memberof  PDWBuffer
 * @retval    DRComponent *
 */
DRComponent* PDWBuffer_create()
{
  return DRComponent_create_noncatalyst_component(
    DRCOMPONENT_NAME_PDW_BUFFER,
    DRCOMPONENT_BASE_ADDR_PDW_BUFFER,
    DRCOMPONENT_RESERVED_ADDR_SPACE_PDW_BUFFER
  );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Setup PDW Buffer
 * @memberof  PDWBuffer
 * @param     drc:
 * @retval    None
 */
void PDWBuffer_setup(DRComponent* drc)
{
  if (drc != NULL)
  {
    // Zero-ize buffer
    for (size_t ii = 0; ii < DRCOMPONENT_RESERVED_ADDR_SPACE_PDW_BUFFER; ii+=4)
    {
      DRComponent_set_value(drc, ((DRComponentValueInfo){ .addr=(ii), .mask=0xFFFFFFFF, .offset=0 }), 0x0 );
    }
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Prints out a PDWBuffer
 * @memberof  PDWBuffer
 * @param     drc:
 * @param     num_pdws:
 * @retval    None
 */
void PDWBuffer_print(DRComponent* drc, size_t num_pdws)
{
  if (drc != (void*)0)
  {
    __builtin___clear_cache((char*) drc->mmap->resource_virtual_memory_address, (((char*)drc->mmap->resource_virtual_memory_address) + 1024));
    __clear_cache((char*) drc->mmap->resource_virtual_memory_address, (((char*)drc->mmap->resource_virtual_memory_address) + 1024));

    OCPCATALYST_LOG(
      OCPCATALYST_LOG_TYPE_INFO,
      "Printing %s at %p => Virt=%p;Phys=%p\r\n",
      DRCOMPONENT_NAME_PDW_BUFFER,
      DRCOMPONENT_BASE_ADDR_PDW_BUFFER,
      drc->mmap->resource_virtual_memory_address,
      drc->mmap->resource_physical_memory_addr
    );

    uint32_t pdw_buffer[PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT];

    for (size_t ii = 0; ii < num_pdws; ii++)
    {

      for (size_t jj = 0; jj < PDWBUFFER_CURRENT_PDW_FORMAT_WORD_COUNT; jj++)
      {
        pdw_buffer[jj] = DRComponent_get_value(drc, ((DRComponentValueInfo){ .addr=(jj*4 + (ii*PDWBUFFER_WRAP)), .mask=0xFFFFFFFF, .offset=0  }));
      }

      // Readback first dma value
      OCPCATALYST_LOG(
        OCPCATALYST_LOG_TYPE_INFO,
        "RAW BUFFER:\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n%x\r\n",
        pdw_buffer[0],
        pdw_buffer[1],
        pdw_buffer[2],
        pdw_buffer[3],
        pdw_buffer[4],
        pdw_buffer[5],
        pdw_buffer[6],
        pdw_buffer[7],
        pdw_buffer[8],
        pdw_buffer[9],
        pdw_buffer[10],
        pdw_buffer[11],
        pdw_buffer[12],
        pdw_buffer[13],
        pdw_buffer[14],
        pdw_buffer[15]
      );
    }

    __builtin___clear_cache((char*) drc->mmap->resource_virtual_memory_address, (((char*)drc->mmap->resource_virtual_memory_address) + 1024));
    __clear_cache((char*) drc->mmap->resource_virtual_memory_address, (((char*)drc->mmap->resource_virtual_memory_address) + 1024));

  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
