/// @file   PDWBufferPDW.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-04-2021


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "private/debug.h"
#include "private/fpgainterface.h"
#include "private/memorymap.h"
// #include "public/components.h"

// /**
//  * @brief  Creates a PDW Buffer PDW struct
//  * @note
//  * @retval PDWBufferPDW *s
//  */
// PDWBufferPulseReport* PDWBufferPulseReport_create()
// {
//   // Report
//   PDWBufferPulseReport* report = (PDWBufferPulseReport*)malloc(sizeof(PDWBufferPulseReport));

//   if (report == (void*)0)
//   {
//     OCPCATALYST_LOG_ERRO("Failed to allocate space for PDW Buffer");
//   }

//   return report;
// }


// /**
//  * @brief  Frees a pulse report
//  * @note
//  * @retval
//  */
// void PDWBufferPulseReport_free(PDWBufferPulseReport* pulse_report)
// {
//   free(pulse_report);
// }