/// @file   EventHandlerOnError.c
/// @brief  File Description
///
/// @author Nicholas Molinski
/// @date   02-25-2021

#include <stddef.h>
#include <stdint.h>
#include "public/errorcodes.h"
#include "private/event/event.h"
#include "private/event/eventhandleronerror.h"


//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Event Handler creation
 * @memberof  EventHandlerOnError
 * @retval    0=SUCCESS, 1=FAILURE
 */
uint8_t EventHandlerOnError_create()
{
  return EventHander_create(EVENT_ON_ERROR);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Subscribe to an OnError event
 * @memberof  EventHandlerOnError
 * @param     handler: EventHandler
 * @param     callback: The callback when this event is emitted
 * @retval    0=SUCCESS, 1=FAILURE
 */
uint8_t EventHandlerOnError_subscribe(on_error_listener callback)
{
  return EventHander_subscribe(EVENT_ON_ERROR, (void (*)(EventHandlerType, ...))callback);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Emits an event to all event subscribers created for this type
 * @memberof  EventHandlerOnError
 * @param     erro_code: The error code emitted
 * @param     erro_msg: The error message
 * @retval    None
 */
void EventHandlerOnError_emit(enum OCPCDR_ErrorCodes erro_code, const char erro_msg[16])
{
  EventHander_emit(__NUM_ARGS_EVENT_ON_ERROR, EVENT_ON_ERROR, erro_code, erro_msg);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
