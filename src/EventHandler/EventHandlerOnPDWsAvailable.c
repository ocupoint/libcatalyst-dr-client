// /// @file   EventHandlerOnPDWsAvailable.c
// /// @brief  File Description
// ///
// /// @author Nicholas Molinski
// /// @date   02-05-2021

// #include <stddef.h>
// #include <stdint.h>
// #include "private/event/event.h"
// #include "private/event/eventhandleronpdwsavailable.h"

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Event Handler creation
//  * @memberof  EventHandlerOnPDWsAvailable
//  * @param     event_type: Event subscribing to
//  * @param     subscriber_handler: callback to run when event is occuring
//  * @retval    0=SUCCESS, 1=FAILURE
//  */
// uint8_t EventHandlerOnPDWsAvailable_create()
// {
//   return EventHander_create(EVENT_ON_PDWS_AVAILABLE);
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Subscribe to an OnPDWsAvailable event
//  * @memberof  EventHandlerOnPDWsAvailable
//  * @param     handler: EventHandler
//  * @param     subscriber_handler:
//  * @todo      Add more error codes
//  * @retval    0=SUCCESS, 1=FAILURE
//  */
// uint8_t EventHandlerOnPDWsAvailable_subscribe(on_pdws_available_listener callback)
// {
//   return EventHander_subscribe(EVENT_ON_PDWS_AVAILABLE, (void (*)(EventHandlerType, ...))callback);
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Emits an event to all event subscribers created for this type
//  * @memberof  EventHandlerOnPDWsAvailable
//  * @param     num_pdws: Number of pdws to be emited
//  * @param     pdw_buffer: Buffer to pass to all subscribers
//  * @retval    None
//  */
// void EventHandlerOnPDWsAvailable_emit(uint16_t num_pdws, PDWBufferPulseReport* pdw_buffer)
// {
//   EventHander_emit(__NUM_ARGS_EVENT_ON_PDWS_AVAILABLE, EVENT_ON_PDWS_AVAILABLE, num_pdws, pdw_buffer);
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////
