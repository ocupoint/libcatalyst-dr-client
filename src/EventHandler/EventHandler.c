/// @file   EventHandler.c
/// @brief  Event Handler interface
///
/// @author Nicholas Molinski
/// @date   02-05-2021

#include <stdlib.h>
#include <stdarg.h>
#include "private/debug.h"
#include "private/event/event.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/** Holds static list of event handlers on the stack */
static EventHandler _static_event_handler_list[__MAX_EVENT_HANDLERS];

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/** Counts number of event handlers */
static size_t _static_num_event_handlers = 0;

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Event Handler creation
 * @memberof  EventHandler
 * @param     event_type: Event subscribing to
 * @param     subscriber_handler: callback to run when event is occuring
 * @retval    0=SUCCESS, 1=FAILURE
 */
uint8_t EventHander_create(enum EventHandlerType event_type)
{
  // Variables
  EventHandler *handler;

  // Go through all event handlers to see if there are any already made for this event type
  for (size_t ii = 0; ii < __MAX_EVENT_HANDLERS; ii++)
  {
    // Found our event
    if (_static_event_handler_list[ii].event_type == event_type)
    {
      return EXIT_SUCCESS;
    }
  }

  // We could not find an event, lets clear and create one
  if (_static_num_event_handlers < __MAX_EVENT_HANDLERS)
  {

    handler = &_static_event_handler_list[_static_num_event_handlers];

    // Clear allocation
    memset(handler->subscriber_handler, 0, __MAX_EVENT_SUBSCRIBERS * sizeof(EventHandler));
    handler->num_subscribers = 0;
    handler->event_type = event_type;

    _static_num_event_handlers += 1;

    return EXIT_SUCCESS;
  }
  else
  {
    OCPCATALYST_LOG_ERRO("Failed to allocate EventHander\r\n");
  }

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Subscribe to an event
 * @memberof  EventHandler
 * @param     handler: EventHandler
 * @param     subscriber_handler:
 * @todo      Add more error codes
 * @retval    0=SUCCESS, 1=FAILURE
 */
uint8_t EventHander_subscribe(enum EventHandlerType event_type, void (*subscriber_handler)(enum EventHandlerType event_type, ...))
{
  // Variables
  EventHandler *handler;

  // Event must exist
  if ((_static_num_event_handlers > 0) && (event_type != NO_EVENT) && (subscriber_handler != (void *)0))
  {
    // Go through all events to find a the correct event
    for (size_t ii = 0; ii < __MAX_EVENT_HANDLERS; ii++)
    {
      // local static
      handler = &_static_event_handler_list[ii];

      // Found the allocation to add a new event subscriber
      if ((handler->event_type == event_type) && (handler->num_subscribers < __MAX_EVENT_SUBSCRIBERS))
      {
        // Add new subscriber and increment
        handler->subscriber_handler[handler->num_subscribers] = subscriber_handler;
        handler->num_subscribers = handler->num_subscribers + 1;

        return EXIT_SUCCESS;
      }
    }
  }

  OCPCATALYST_LOG_ERRO("Failed to subscribe to event. Does this event exist or does the maximum number of subscribers need to be adjusted?\r\n");

  return EXIT_FAILURE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief     Emits an event to all event handlers created for that type
 * @todo      Needs a refactor
 * @memberof  EventHandler
 * @param     event_type:
 * @retval    None
 */
void EventHander_emit(uint8_t num_args, ...)
{
  // Variables
  EventHandler *handler;
  va_list args;
  enum EventHandlerType event_type;
  void *arg_1;
  void *arg_2;
  void *arg_3;

  va_start(args, num_args);

  // extract the event type
  event_type = (EventHandlerType)va_arg(args, int);

  // Find the event handler
  for (size_t ii = 0; ii < __MAX_EVENT_SUBSCRIBERS; ii++)
  {
    handler = &_static_event_handler_list[ii];

    if (event_type == handler->event_type)
    {
      switch (num_args)
      {
      case 3:
        arg_1 = va_arg(args, void *);
        arg_2 = va_arg(args, void *);
        arg_3 = va_arg(args, void *);
        // Go through all the subscribers
        for (size_t jj = 0; jj < handler->num_subscribers; jj++)
        {
          // According to the event pass the appropriate amount of args
          handler->subscriber_handler[jj](event_type, arg_1, arg_2, arg_3);
        }
        break;
      case 2:
        arg_1 = va_arg(args, void *);
        arg_2 = va_arg(args, void *);
        // Go through all the subscribers
        for (size_t jj = 0; jj < handler->num_subscribers; jj++)
        {
          // According to the event pass the appropriate amount of args
          handler->subscriber_handler[jj](event_type, arg_1, arg_2);
        }
        break;
      case 1:
        arg_1 = va_arg(args, void *);
        arg_2 = va_arg(args, void *);
        for (size_t jj = 0; jj < handler->num_subscribers; jj++)
        {
          // According to the event pass the appropriate amount of args
          handler->subscriber_handler[jj](event_type, arg_1);
        }
        break;
      default:
        for (size_t jj = 0; jj < handler->num_subscribers; jj++)
        {
          // According to the event pass the appropriate amount of args
          handler->subscriber_handler[jj](event_type);
        }
        break;
      }

      break;
    }
  }

  va_end(args);
}
