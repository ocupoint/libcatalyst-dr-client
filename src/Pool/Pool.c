// /// @file   Pool.c
// /// @brief  File Description
// ///
// /// @author Nicholas Molinski
// /// @date   02-28-2021

// #include <stdlib.h>
// #include <stdbool.h>
// #include "private/debug.h"
// #include "private/helpers.h"
// #include "private/pool.h"

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Create a Object Pool
//  * @param     object_size: size of the object in the pool
//  * @param     num_objects: number of object to allocate space for
//  * @memberof  Pool
//  * @retval    Pool *
//  */
// Pool* Pool_create(size_t object_size, size_t num_objects)
// {
//   // Variables
//   Pool*   pool;

//   pool = (Pool*)Heap_allocate(sizeof(Pool));

//   // Check for failed pool allocation
//   if (pool != NULL)
//   {
//     // Save Pool information
//     pool->object_size          = object_size;
//     pool->num_objects          = num_objects;
//     pool->num_volatile_objects = 1;

//     // Allocate pool
//     pool->pool_memory          = Heap_clear_allocate(num_objects*object_size);
//     pool->pool_header          = Heap_clear_allocate(num_objects*sizeof(struct PoolObjectHeader));
//     pool->pool_volatile_memory = Heap_allocate(sizeof(void**));

//     if ((pool->pool_memory != NULL) && (pool->pool_header != NULL))
//     {
//       // Clear pool
//       *(pool->pool_volatile_memory) = NULL;

//       pool->num_objects_allocated = 0;

//       return pool;
//     }
//   }

//   OCPCATALYST_LOG_ERRO("Failed to allocate Pool\r\n");

//   return NULL;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Returns the remaining amount of objects that can be allocated to the pool
//  * @param     pool: Pool *
//  * @memberof  Pool
//  * @retval    size_t
//  */
// size_t Pool_get_num_objects_free(Pool* pool)
// {
//   // Assert pool exists
//   if (pool == NULL)
//   {
//     return 0;
//   }

//   return pool->num_objects - pool->num_objects_allocated;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Returns the amount of objects allocated
//  * @param     pool: Pool *
//  * @memberof  Pool
//  * @retval    size_t
//  */
// size_t Pool_get_num_objects_allocated(Pool* pool)
// {
//   // Assert pool exists
//   if (pool == NULL)
//   {
//     return 0;
//   }

//   return pool->num_objects_allocated;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Returns Allocation to first unused_pool object.
//  * @param     pool: Pool *
//  * @memberof  Pool
//  * @retval    void *
//  */
// void* Pool_get_free_object(Pool* pool)
// {
//   // Variables
//   void* alloc;

//   // ensure there are available objects
//   if (Pool_get_num_objects_free(pool) > 0)
//   {
//     alloc = pool->pool_memory;

//     // Loop through the objects
//     for (size_t ii = 0; ii < pool->num_objects; ii++)
//     {
//       PoolObjectHeader* block_header = pool->pool_header + (ii*sizeof(PoolObjectHeader));
//       // Check if object is free
//       if (block_header->is_used == false)
//       {
//         // Set the object to be occupied
//         block_header->is_used = true;

//         pool->num_objects_allocated += 1;

//         // Return the object ptr
//         return pool->pool_memory + (pool->object_size*ii);
//       }
//     }
//   }
//   // Expensive volatile pool allocation
//   else if (pool != NULL)
//   {
//     // Check if there exists any extra pointer locations
//     for (size_t ii = 0; ii < pool->num_volatile_objects; ii++)
//     {
//       void* volatile_alloc = pool->pool_volatile_memory + (ii*sizeof(void*));

//       // pointer points to null
//       if (volatile_alloc == NULL)
//       {
//         OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_WARN, "Using Voltaile Pool\r\n");

//         // point to new pointer
//         volatile_alloc = malloc(pool->object_size);

//         return volatile_alloc;
//       }
//     }

//     // No free pointers found, time to realloc
//     // Increase pool size
//     pool->num_volatile_objects += 1;

//     void** temp = pool->pool_volatile_memory;

//     pool->pool_volatile_memory = realloc(pool->pool_volatile_memory, pool->num_volatile_objects);

//     free(temp);

//     // Now allocate newest pointer
//     pool->pool_volatile_memory[pool->num_volatile_objects - 1] = malloc(pool->object_size);

//     return pool->pool_volatile_memory[pool->num_volatile_objects - 1];
//   }

//   return NULL;
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Free the pool item
//  * @param     pool: Pool *
//  * @param     object: pointer to an object
//  * @memberof  Pool
//  * @retval    None
//  */
// void Pool_free_object(Pool* pool, void* object)
// {
//   // Variables
//   void* alloc;

//   if (pool != NULL)
//   {
//     alloc = pool->pool_memory;

//     // Loop through the objects
//     for (size_t ii = 0; ii < pool->num_objects; ii++)
//     {
//       // Check if exists in our static memory
//       if (object == (pool->pool_memory + (pool->object_size*ii)))
//       {
//         // Allow object for re-use
//         pool->pool_header[ii].is_used = false;
//         pool->num_objects_allocated -= 1;

//         return;
//       }
//     }

//     // Check if object in volatile memory
//     // Have not found the
//     for (size_t ii = 0; ii < pool->num_volatile_objects; ii++)
//     {
//       // Found volatile object
//       if (object == pool->pool_volatile_memory[ii])
//       {
//         free(object);
//         // Point to NULL
//         pool->pool_volatile_memory[ii] = NULL;
//         // Free pointer

//         return;
//       }
//     }

//     OCPCATALYST_LOG(OCPCATALYST_LOG_TYPE_WARN, "Failed to free object from managed pool.\r\n");
//   }
//   else
//   {
//     // Normal object
//     free(object);
//   }
// }

// //////////////////////////////////////////////////////////////////////////////////////////////////////////

// /**
//  * @brief     Destroy the pool
//  * @param     pool: Pool *
//  * @memberof  Pool
//  * @retval    None
//  */
// void Pool_destroy(Pool* pool)
// {
//   if (pool != NULL)
//   {
//     // Free two allocations of static memory
//     free(pool->pool_header);
//     free(pool->pool_memory);

//     // Free all objects in volatile pool
//     for (size_t ii = 0; ii < pool->num_volatile_objects; ii++)
//     {
//       free(pool->pool_volatile_memory[ii]);
//     }

//     // Free pointer pointing to volatile objects
//     free(pool->pool_volatile_memory);

//     // free the pool
//     free(pool);
//   }
// }